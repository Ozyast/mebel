<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!--[if (gte mso 9)|(IE)]>
<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td>
<![endif]-->
<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
    <tr>
        <td align="center" style="padding: 20px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold;">
            <img src="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl("img/logo/logo_white.png") ?>" alt="Express-mebel"  style="display:block;" />
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#45a7b9" style="padding: 25px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 26px;">
            <b><?= strlen($this->title) ? Html::encode($this->title) : "Новый заказ" ?></b>
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 20px 20px 20px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px;">
            <b>Данные заказа</b>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" style="padding: 0 20px 40px 20px; color: #777777; font-family: Arial, sans-serif; font-size: 18px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
          <table cellpadding="0" cellspacing="0" border="0">
           <tr>
            <td width="50%" style="border-bottom: 1px solid #f6f6f6;">ID</td>
            <td width="50%" style="border-bottom: 1px solid #f6f6f6;">
              <a href="<?= Yii::$app->urlManagerBackend->createAbsoluteUrl("cart/index?s=".$cart['id_cart']."&row=id_cart&t=i") ?>"><?= $cart['id_cart'] ?></a>
            </td>
           </tr>
           <tr>
            <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Статус</td>
            <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['status'] ?></td>
           </tr>
           <tr>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Имя покупателя</td>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['user_name'] ?></td>
           </tr>
           <tr>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Телефон покупателя</td>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['user_phone'] ?></td>
           </tr>
           <?php if ($cart['delivery']): ?>
             <tr>
               <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Доставка</td>
               <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Нужна</td>
             </tr>
             <tr>
               <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Адрес покупателя</td>
               <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['user_address'] ?></td>
             </tr>
           <?php endif; ?>
           <tr>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Комментарий к заказу</td>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['comment'] ?></td>
           </tr>
           <tr>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Цена без скидки</td>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['price'] ?></td>
           </tr>
           <tr>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Скидка</td>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['sale'] ?></td>
           </tr>
           <tr>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;">Итого</td>
             <td width="50%" style="border-bottom: 1px solid #f6f6f6;"><?= $cart['price_total'] ?></td>
           </tr>
          </table>
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 20px 20px 20px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px;">
            <b>Товар в заказе</b>
        </td>
    </tr>
    <?php foreach ($cartItem['cartItem'] as $key => $row):
      $row = (array) $row; ?>
      <tr>
        <td bgcolor="#ffffff" style="padding: 20px 20px 0 20px; border-bottom: 1px solid #f6f6f6;">
          <table width="128" align="left" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="128" style="padding: 0 20px 20px 0;">
                <img src="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl("img/items/$row[image]") ?>" alt="Товар #<?= $key ?>" width="128" height="128" style="display: block;" />
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
          <table width="387" align="left" cellpadding="0" cellspacing="0" border="0">
          <tr>
          <td>
          <![endif]-->
          <table class="col387" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 387px;">
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;">
                      <strong>
                        <a href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl("items/view?id=".$row['id_items']) ?>" style="color: #555555;">
                          <?= $row['title'] ?>
                        </a>
                      </strong><br/>
                      <ul style="list-style:none;">
                        <li>Цвет: <?= $row['color'] ?></li>
                        <li>Кол-во: <?= $row['count'] ?></li>
                        <?php if ($row['sale']): ?>
                          <li>Цена: <sub><del style="color:#cccccc"><?= $row['price'] ?></del></sub> <?= ($row['price']-$row['sale']) ?></li>
                        <?php else: ?>
                          <li>Цена: <?= $row['price'] ?></li>
                        <?php endif; ?>
                        <li>Итого: <?= $row['sale'] ? (($row['price']-$row['sale']) * $row['count']) : ($row['price'] * $row['count']) ?></li>
                      </ul>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
            </table>
          <![endif]-->
        </td>
      </tr>
    <?php endforeach; ?>

    <tr>
      <td align="center" bgcolor="#f9f9f9" style="padding: 30px 20px 30px 20px; font-family: Arial, sans-serif;">
        <table bgcolor="#45a7b9" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
          <tr>
            <td align="center" height="55" style=" padding: 0 35px 0 35px; font-family: Arial, sans-serif; font-size: 22px;" class="button">
              <a href="<?= Yii::$app->urlManagerBackend->createAbsoluteUrl("cart/index?s=".$cart['id_cart']."&row=id_cart&t=i") ?>" style="color: #ffffff; text-align: center; text-decoration: none;">Просмотр заказа</a>
            </td>
          </tr>
        </table>
      </td>
    </tr>

</table>
<!--[if (gte mso 9)|(IE)]>
        </td>
    </tr>
</table>
<![endif]-->
