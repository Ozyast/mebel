<?php
return [
    // Почта администратора, пока не используется
    'adminEmail' => 'admin@express-mebel23.ru',

    // Почта поддержки, используется в:
    // - При отправке почты на emailNotification о новом заказе
    // 'supportEmail' => 'support@express-mebel23.ru',
    'supportEmail' => 'ozy@nxt.ru',

    // Системная настройка
    'user.passwordResetTokenExpire' => 3600,

    /**
      * Настройки для сайта (свои)
      */
    // Использовать подкатегорию
    'siteSubCategory' => 0,
    // Использовать группы категорий (группа 1: деревянные, железные.
    // группа 2: 1-2 года, 2-3 года)
    'siteCategoryGroup' => 1,
    // Разрешить добавлять в слайдере текст
    'siteSliderText' => 0,

    // Email для оповещения (Новый зака, )
    // 'emailNotification' => "express-mebel23@mail.ru",
    'emailNotification' => "ozycast@gmail.com",

];
