<?php

namespace frontend\controllers;

use Yii;
use app\models\Items;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * itemsController implements the CRUD actions for items model.
 */
class ItemsController extends Controller
{
  // Массив с меню содержит в себе два массива, массив с категориями (category)
  // и комнатами (subcategroy)
  public $page;
  // Мета тег description
  public $description;
  // Шаблон
  public $layout = 'wrapper';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all items models.
     * @return mixed
     */
    public function actionIndex($page = 1, $ps = 20, $so = 0)
    {
      // Кол-во записей на странице
      $ps_array = Items::getCountSortBar();
      $ps = in_array($ps, $ps_array) ? $ps : 20;

      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      // Получим данные для фильтра
      $filter = Items::getFilter();

      // Обрабатываем дополнительные условия выборки
      $criteria = Items::setFilter(Items::criteria());

      // Получаем данные пагинации
      $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => $ps]);
      // Запросим данные согласно пагинации
      $items = $criteria->offset($pagination->offset)
          ->limit($pagination->limit)
          ->all();

      // Получим данные по кол-ву старниц и т.д.
      $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

      if (!Yii::$app->request->isPost) {
        return $this->render('index', [
            'data_table' => $items,
            "filter_min_max" => $filter["min_max"],
            "filter_color" => $filter["color"],
            "filter_manufacturer" => $filter["manufacturer"],
            'pages' => $pages,
        ]);
      } else {
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/items/_index.php', [
            "data_table" => $items,
            'pages' => $pages,
        ]);

        return json_encode($data);
      }
    }


    /**
     * Lists all items models.
     * @return mixed
     */
    public function actionCategory($cat, $page = 1, $ps = 20, $so = 0)
    {
      // Кол-во записей на странице
      $ps_array = Items::getCountSortBar();
      $ps = in_array($ps, $ps_array) ? $ps : 20;

      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();
      $id = $this->page['category'][$cat]['id_category'];
      if (!$id)
        throw new NotFoundHttpException('Категория не найдена');

      // Получим данные для фильтра
      $filter = Items::getFilter();

      // Добавим условие
      $criteria = Items::criteria()
                ->rightJoin("items_category_relationship fcar", "items.id_items = fcar.id_items AND fcar.id_category = :id_category", [":id_category" => $id]);

      // Обрабатываем дополнительные условия выборки
      $criteria = Items::setFilter($criteria);

      // Получаем данные пагинации
      $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => $ps]);
      // Запросим данные согласно пагинации
      $items = $criteria->offset($pagination->offset)
          ->limit($pagination->limit)
          ->all();
      // Получим данные по кол-ву старниц и т.д.
      $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

      if (!Yii::$app->request->isPost) {
        $filter_active_color = isset($_GET["cl"]) ? explode(",", $_GET["cl"]) : [];

        return $this->render('category', [
            'data_table' => $items,
            "filter_min_max" => $filter["min_max"],
            "filter_color" => $filter["color"],
            "filter_active_color" => $filter_active_color,
            "filter_manufacturer" => $filter["manufacturer"],
            "pages" => $pages,
        ]);
      } else {
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/items/_index.php', [
            "data_table" => $items,
            "pages" => $pages,
        ]);

        return json_encode($data);
      }
    }

    /**
     * Lists all items models.
     * @return mixed
     */
    // public function actionInterior($id, $page = 1, $ps = 20, $so = 0)
    // {
    //   // Кол-во записей на странице
    //   $ps_array = Items::getCountSortBar();
    //   $ps = in_array($ps, $ps_array) ? $ps : 20;
    //
    //   $this->page = @\app\models\Site::page();
    //   if (!isset($this->page['interior'][$id]))
    //     throw new NotFoundHttpException('Интерьер не найдена');
    //
    //   // Добавим заголовок
    //   $this->view->title = $this->page['interior'][$id]['name_ru']." - Экспресс Мебель";
    //
    //   // Получим данные для фильтра
    //   $filter = Items::getFilter();
    //
    //   // Добавим условие
    //   $criteria = Items::criteria()
    //             ->rightJoin("items_interior_relationship fir", "items.id_items = fir.id_items AND fir.id_interior = :id_interior", [":id_interior" => $id]);
    //   // Обрабатываем дополнительные условия выборки
    //   $criteria = Items::setFilter($criteria);
    //   // Получаем данные пагинации
    //   $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => $ps]);
    //   // Запросим данные согласно пагинации
    //   $items = $criteria->offset($pagination->offset)
    //       ->limit($pagination->limit)
    //       ->all();
    //
    //   // Получим данные по кол-ву старниц и т.д.
    //   $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);
    //
    //   if (!Yii::$app->request->isPost) {
    //     $filter_active_color = isset($_GET["cl"]) ? explode(",", $_GET["cl"]) : [];
    //
    //     return $this->render('index', [
    //       "data_table" => $items,
    //       "interior" => $this->page["interior"][$id],
    //       "pageHeadImage" => "/img/interior/".$this->page["interior"][$id]["image"],
    //       "pageHeadTitle" => $this->page["interior"][$id]["name_ru"],
    //       "pageHeadMenu" => ["Главная"=>["/"], $this->page["interior"][$id]["name_ru"]],
    //       "filter_min_max" => $filter["min_max"],
    //       "filter_color" => $filter["color"],
    //       "filter_manufacturer" => $filter["manufacturer"],
    //       "filter_active_color" => $filter_active_color,
    //       "pages" => $pages,
    //     ]);
    //   } else {
    //     $data['status'] = "success";
    //     $data['html'] = $this->renderFile('@app/views/items/_index.php', [
    //         "data_table" => $items,
    //         "pages" => $pages,
    //     ]);
    //
    //     return json_encode($data);
    //   }
    // }

    /**
     * Displays a single items model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      $items = (new \yii\db\Query())
            ->select([
                'f.*',
                "( SELECT count(*) FROM cart_item ci WHERE ci.id_items = :id ) as total_sold",
                "( SELECT GROUP_CONCAT(id_category) FROM items_category_relationship fcr WHERE fcr.id_items = :id ) as category",
                // "( SELECT GROUP_CONCAT(id_interior) FROM items_interior_relationship fcr WHERE fcr.id_items = :id ) as interior",
                "( SELECT GROUP_CONCAT(id_colors) FROM items_colors_relationship fcr WHERE fcr.id_items = :id ) as colors",
                "( SELECT GROUP_CONCAT(id_modules) FROM items_modules_relationship fcr WHERE fcr.id_items = :id ) as modules",
            ])
            ->from("items f")
            ->where(["f.id_items" => $id, "f.visible" => "1"])
            ->params([":id"=>$id])
            ->one();

      if (!$items)
        throw new NotFoundHttpException('Товар не найдена');

      // Похожие товары
      $items_similar = Items::getSimilar($id, $items['category']);

      // ФОТО
      $items_image = (new \yii\db\Query())
              ->from("items_image")
              ->where(["id_items" => $id, "visible" => 1])
              ->All();

      // ОПИСАНИЕ
      $items_description = (new \yii\db\Query())
              ->from("items_description")
              ->where(["id_items" => $id])
              ->One();

      // ОТЗЫВЫ
      $items_review = (new \yii\db\Query())
              ->from("items_review")
              ->where(["id_items" => $id])
              ->All();
      // ВОПРОСЫ
      $items_question = (new \yii\db\Query())
              ->from("items_question")
              ->where(["id_items" => $id])
              ->All();

      $items["colors"] = explode(",", $items["colors"]);
      $items["modules"] = explode(",", $items["modules"]);
      $items_description["description"] = json_decode($items_description["description"]);

      // ЦВЕТА
      $items_color = (new \yii\db\Query())
              ->select("icr.visible, icr.instock, c.image, c.name, c.id_colors")
              ->from("items_colors_relationship icr")
              ->leftJoin("colors c", "c.id_colors = icr.id_colors")
              ->where(["id_items" => $id])
              ->orderBy('visible DESC')
              ->All();

      // Проверяем какое кол-во товара осталось в наличии по цветам
      $reserve = Items::checkAllItemsInstock($items, $items_color);
      $items_color = $reserve['colors'];

      //  МОДУЛИ
      if (count($items["modules"])){
        $modules = (new \yii\db\Query())
          ->from("modules")
          ->where(["id_modules" => $items["modules"]])
          ->All();

        $modules = Helper::indexByKey($modules, 'id_modules');
        if (count($modules)) {
          foreach ($items["modules"] as $key) {
            $items_modules[] = $modules[$key];
          }
        }
      }

      return $this->render('view', [
          'items' => $items,
          "items_image" => $items_image,
          "items_description" => $items_description,
          "items_color" => $items_color,
          "items_question" => $items_question,
          "items_review" => $items_review,
          "items_modules" => $items_modules,
          "items_similar" => $items_similar,
          "reserve" => $reserve,
      ]);
    }

    /**
     * Finds the items model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Items the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Items::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
