<?php

namespace frontend\controllers;

use Yii;
use app\models\Contact;
use app\models\Site;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Cookie;

/**
 * CartController implements the CRUD actions for Cart model.
 */
class ContactController extends Controller
{
  // Массив с меню содержит в себе два массива, массив с категориями (category)
  // и комнатами (subcategroy)
  public $page;
  // Мета тег description
  public $description;
  // Шаблон
  public $layout = 'wrapper';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['POST'],
                ],
            ],
        ];
    }

    /**
      * Добавление нового вопроса к товару
      */
    public function actionCreate()
    {
        $model = new Contact();

        $model->attributes = Yii::$app->request->post();
        $model->ip = Yii::$app->request->userIP;

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);

          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['text'] = "Ваше сообщение успешно отправлено. Скоро мы Вам ответим.";
        return json_encode($data);
    }

    /**
     * Finds the Cart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
