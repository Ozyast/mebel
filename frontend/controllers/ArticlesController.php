<?php

namespace frontend\controllers;

use Yii;
use app\models\Articles;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * itemsController implements the CRUD actions for items model.
 */
class ArticlesController extends Controller
{
  // Массив с меню содержит в себе два массива, массив с категориями (category)
  // и комнатами (subcategroy)
  public $page;
  // Мета тег description
  public $description;
  // Шаблон
  public $layout = 'wrapper';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Displays a single items model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($name, $visible = 0)
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      $id = $this->page['articles'][$name]['id_articles'];

      if (!$id && !$visible)
        throw new NotFoundHttpException('Статья не найдена');

      // Если с флагом висибл то показываем скрытые статьи
      if ($visible) {
        $articles = (new \yii\db\Query())
        ->from("articles")
        ->where(["menu_url" => $name])
        ->one();
      } else {
        $articles = (new \yii\db\Query())
        ->from("articles")
        ->where(["id_articles" => $id])
        ->one();
      }

      if (!$articles || !$articles['file_name'] || !file_exists(Yii::getAlias('@app/views/articles/'.$articles['file_name'])))
        throw new NotFoundHttpException('Статья не найдена');

      return $this->render('view', [
          'articles' => $articles,
      ]);
    }

    /**
     * Finds the items model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Items the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Items::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
