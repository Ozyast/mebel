<?php

namespace frontend\controllers;

use Yii;
use app\models\Sale;
use app\models\Site;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Cookie;

/**
 * CartController implements the CRUD actions for Cart model.
 */
class SaleController extends Controller
{
  // Массив с меню содержит в себе два массива, массив с категориями (category)
  // и комнатами (subcategroy)
  public $page;
  // Мета тег description
  public $description;
  // Шаблон
  public $layout = 'wrapper';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'add' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Открываем все доступные акции
     */
    public function actionIndex()
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      $sale = (new \yii\db\Query())
        ->from("sale")
        ->where(["visible" => 1])
        ->all();

      return $this->render('index', [
          'sale' => $sale,
      ]);
    }

    /**
     * Finds the Cart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
