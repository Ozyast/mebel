<?php
namespace frontend\controllers;

use Yii;
use app\models\Site;
use app\models\Items;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
  // Массив с меню содержит в себе два массива, массив с категориями (category)
  // и комнатами (subcategroy)
  public $page;
  // Мета тег description
  public $description;
  // Шаблон
  public $layout = 'wrapper-top';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Главная
     *
     * @return mixed
     */
    public function actionIndex()
    {
      $this->page = @\app\models\Site::page();

      // МЕБЕЛЬ
      $criteria = Items::setFilter(Items::criteria(), ["AND", ["f.visible" => 1]]);
      $items_new = $criteria->orderBy("id_items DESC")->limit(15)->all();

      $slider = (new \yii\db\Query())
              ->from("slider")
              ->where(["visible" => 1])
              ->All();

      return $this->render('index', [
        "slider" => $slider,
        "items_new" => $items_new,
      ]);
    }

    /**
     * Контакты
     *
     * @return mixed
     */
    public function actionContact()
    {
        $this->page = @\app\models\Site::page();

        return $this->render('contact');
    }

    /**
     * О компании
     */
    public function actionAbout()
    {
      $this->page = @\app\models\Site::page();

      return $this->render('about');
    }

    /**
     * Доставка и сборка
     */
    public function actionDelivery()
    {
      $this->page = @\app\models\Site::page();

      return $this->render('delivery');
    }

    /**
     * Оплата товара
     */
    public function actionPayment()
    {
      $this->page = @\app\models\Site::page();

      return $this->render('payment');
    }

    /**
      * Вывод Ошибок
      */
    public function actionError()
    {
      $this->page = @\app\models\Site::page();

      $exception = Yii::$app->errorHandler->exception;

      if ($exception == null)
        return;

      $statusCode = $exception->statusCode;
      $name = $exception->getName();
      $message = $exception->getMessage();

      if (Yii::$app->request->isPost) {
        $data['status'] = "danger";
        $data['text'] = "Ошибка ($statusCode): $name. $message";

        return json_encode($data);
      } else {
        return $this->render('error', [
            'exception' => $exception,
            'statusCode' => $statusCode,
            'name' => $name,
            'message' => $message
        ]);
      }
    }
}
