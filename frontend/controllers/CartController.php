<?php

namespace frontend\controllers;

use Yii;
use app\models\Cart;
use app\models\CartItem;
use app\models\Site;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Cookie;

/**
 * CartController implements the CRUD actions for Cart model.
 */
class CartController extends Controller
{
  // Массив с меню содержит в себе два массива, массив с категориями (category)
  // и комнатами (subcategroy)
  public $page;
  // Мета тег description
  public $description;
  // Шаблон
  public $layout = 'wrapper';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['POST'],
                    'delete' => ['POST'],
                    'update' => ['POST'],
                    'done' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Открывает корзину пользователя (COOKIE)
     */
    public function actionIndex()
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();
      $order = [];

      $cartHash = (array) json_decode(Site::getCookies("cartHash"));
      if ($cartHash) {
        $order = (new \yii\db\Query())
          ->from("cart")
          ->where(["hash" => $cartHash])
          ->all();
      }

      return $this->render('index', [
          'order' => $order,
      ]);
    }

    /**
     * Открывает заказ пользователя (БД)
     */
    public function actionOrder($id)
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      $order = (new \yii\db\Query())
        ->from("cart")
        ->where(["hash" => $id])
        ->one();

      $order_item = (new \yii\db\Query())
        ->from("cart_item")
        ->where(["id_cart" => $order["id_cart"]])
        ->all();

      return $this->render('order', [
          'order' => $order,
          'order_item' => $order_item,
      ]);
    }


    /**
     * Открывает корзину пользователя (COOKIE)
     */
    public function actionCheckout()
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      // получим данные корзины
      $cart = (array) json_decode(Site::getCookies("cart"));

      // Если есть корзина
      if (!$cart){
        return Yii::$app->response->redirect(['cart/index']);
      }

      // Сравним цены в корзине и в БД
      $cartItem = Cart::checkPriceCartCookie($cart);

      // Если цены изменились, обновим данные в COOKIE
      if ($cartItem["recount"]) {
        // Пересчитаем данные корзины
        $cart = Cart::recountCartCookie($cartItem["cartItem"]);

        // Запишем новые значения корзины и новый массив с товаром
        Site::setCookies("cart", json_encode($cart));
        Site::setCookies("cartItem", json_encode($cartItem["cartItem"]));
      }

      return $this->render('checkout', [
          'cart' => $cart,
          'cart_item' => $cartItem["cartItem"],
          'check_instock' => $cartItem["check_instock"],
      ]);
    }

    /**
     * Завершение оформления заказа, сохранение корзины в БД
     */
    public function actionDone()
    {
      $this->layout = "wrapper-top";
      $this->page = @\app\models\Site::page();

      // Сравним цены в корзине и в БД
      $cartItem = Cart::checkPriceCartCookie();
      // Если какой то из товаров отстутствует в наличии
      if (!$cartItem['check_instock']) {
        return '{"status":"danger", "text":"Этот товар уже кпили или на складе нет такого колличества, пожалуйста обновите страницу"}';
      }

      $cart = (array) $cartItem["cart"];

      // Если цены изменились, обновим данные в COOKIE
      if ($cartItem["recount"]) {
        // Пересчитаем данные корзины
        $cart = Cart::recountCartCookie($cartItem["cartItem"]);

        // Запишем новые значения корзины и новый массив с товаром
        Site::setCookies("cart", json_encode($cart));
        Site::setCookies("cartItem", json_encode($cartItem["cartItem"]));

        return '{"status":"danger","text":"Ваша корзина изменилась или цена на товар не совпадала. Пожалуйста, обновите страницу и попробуйте снова"}';
      }

      // Заполним и сохраним нашу корзину
      $model = new Cart;
      $model->attributes = $_POST;
      $model->attributes = [
          "price" => $cart["price"],
          "sale" => $cart["sale"],
          "price_total" => $cart["total"],
          "user_phone" => Cart::phoneSet($_POST["user_phone"]),
      ];
      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      foreach ($cartItem["cartItem"] as $row) {
        $row = (array) $row;
        $model_item = new CartItem;
        $model_item->attributes = $row;
        $model_item->id_cart = $model->id_cart;
        $model_item->price_sale = $row["sale"];

        if (!$model_item->save()) {
          $message = $model_item->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '"}';
        }
      }

      $result = $model->sendMailOrder(["cart"=>$model->toArray(), "cartItem"=>$cartItem]);
      if (!$result['status'])
        Yii::debug('Ошибка отправки почты');

      // Удалим данные о корзине, вместе с данными о товаре
      Site::delCookies("cart");
      Site::delCookies("cartItem");

      // Создадим новую переменную дял ID оформленного заказа
      Cart::hashSave($model->hash);
      return '{"status":"success", "text":"Спасибо за Ваш заказ! Сейчас мы переадресуем Вас на страницу заказа", "id":"'.$model->hash.'"}';
    }

    /**
      * Удаление товара из пользовательской корзины (COOKIE)
      * @param id - ID товара или ключ, из COOKIE[cartItam] вида id_furintures.'_'.id_color (21_3)
      */
    public function actionDelete()
    {
      // Получим из Cookie все содержимое корзины
      $cartItem = (array) json_decode(Site::getCookies("cartItem"));

      // Удалим переменную если она есть
      if ( isset($cartItem[$_POST["id"]]) )
        unset($cartItem[$_POST["id"]]);

      // Пересчитаем данные корзины
      $cart = Cart::recountCartCookie($cartItem);

      // Запишем новые значения корзины и новый массив с товаром
      Site::setCookies("cart", json_encode($cart));
      Site::setCookies("cartItem", json_encode($cartItem));

      // Выведем код корзины для вставки
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/cart/_index_cart.php', [
          'cart' => $cart,
          'cart_item' => $cartItem,
      ]);
      $data['html_menu'] = $this->renderFile('@app/views/cart/_cart.php', [
          'cart' => $cart,
          'cart_item' => $cartItem,
      ]);

      return json_encode($data);
    }


    // public function actionTest()
    // {
    //   $cart = (array) json_decode(Site::getCookies("cart"));
    //   $cartItem = (array) json_decode(Site::getCookies("cartItem"));
    //   $cartHash = (array) json_decode(Site::getCookies("cartHash"));
    //
    //   echo "<pre>";
    //   print_r($cart);
    //   print_r($cartItem);
    //   print_r($cartHash);
    //   echo "</pre>";
    //   return;
    // }

    /**
      * Удаление товара из пользовательской корзины (COOKIE)
      * @param id - ID товара или ключ, из COOKIE[cartItam] вида id_furintures.'_'.id_color (21_3)
      * @param count - Новое кол-во товара
      */
    public function actionUpdate()
    {
      // Получим из Cookie все содержимое корзины
      $cartItem = (array) json_decode(Site::getCookies("cartItem"));

      // Если такой товар есть, изменим его кол-во и цену
      if ( isset($cartItem[$_POST["id"]]) ){
        $cartItem[$_POST["id"]] = (array) $cartItem[$_POST["id"]];
        $price = $cartItem[$_POST["id"]]["price"] / $cartItem[$_POST["id"]]["count"];
        $sale = $cartItem[$_POST["id"]]["sale"] / $cartItem[$_POST["id"]]["count"];
        $cartItem[$_POST["id"]]["count"] = (int) $_POST["count"];
        $cartItem[$_POST["id"]]["price"] = $price * $cartItem[$_POST["id"]]["count"];
        $cartItem[$_POST["id"]]["sale"] = $sale * $cartItem[$_POST["id"]]["count"];
      }

      // Пересчитаем данные корзины
      $cart = Cart::recountCartCookie($cartItem);

      // Запишем новые значения корзины и новый массив с товаром
      Site::setCookies("cart", json_encode($cart));
      Site::setCookies("cartItem", json_encode($cartItem));

      // Выведем код корзины для вставки
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/cart/_index_cart.php', [
          'cart' => $cart,
          'cart_item' => $cartItem,
      ]);
      $data['html_menu'] = $this->renderFile('@app/views/cart/_cart.php', [
          'cart' => $cart,
          'cart_item' => $cartItem,
      ]);

      return json_encode($data);
    }


    /**
      * Довавляет в Cookie корзину и данные по товару, который добавили в корзину
      * @param _POST[id_items] - id товара, который добавляем в корзину
      * @param _POST[color] - id цвета товара
      * @param _POST[count] - Кол-во товара
      */
    public function actionAdd()
    {
      // Создадим и запишем все данные о добавляемом товаре
      $cartItem = Cart::initCartItemCookie($_POST);
      if (!$cartItem["status"])
        return json_encode(["status"=>"danger", "text"=>$cartItem['text']]);
      $cartItem = $cartItem["data"];

      // Пересчитаем данные корзины
      $cart = Cart::recountCartCookie($cartItem);

      // Запишем новые значения корзины и новый массив с товаром
      Site::setCookies("cart", json_encode($cart));
      Site::setCookies("cartItem", json_encode($cartItem));

      // Выведем код корзины для вставки
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/cart/_cart.php', [
          'cart' => $cart,
          'cart_item' => $cartItem,
      ]);

      return json_encode($data);
    }


    /**
     * Finds the Cart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
