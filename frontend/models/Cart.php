<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id_cart
 * @property string $hash
 * @property string $status
 * @property integer $price
 * @property integer $sale
 * @property integer $price_total
 * @property integer $paid-up
 * @property string $created_at
 * @property string $updated_at
 * @property integer $done
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash', 'price', 'price_total', 'user_phone', 'user_name'], 'required'],
            [['price', 'sale', 'price_total', 'paid-up', 'done'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['hash', 'user_address'], 'string', 'max' => 250],
            [['status', 'user_name'], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 1000],
            [['user_phone'], 'string', 'max' => 20],
            [["sale", 'delivery'], "default", "value" => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cart' => 'Id Cart',
            'hash' => 'Идентификатор заказа',
            'status' => 'Status',
            'price' => 'Стоимость',
            'sale' => 'Скидка',
            'price_total' => 'Итого',
            'user_name' => "Ваше имя",
            'user_phone' => "Ваш телефон",
            'user_address' => "Ваш телефон",
            'comment' => "Комментарий к заказу",
            'paid-up' => 'Paid Up',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'done' => 'Done',
        ];
    }

    public function phoneSet($phone)
    {
        $phone = str_replace([" ", "(", ")", "+", "-"], "", $phone);
        if ( strlen($phone) == 11 && $phone[0] == "7")
          $phone = "8".substr($phone, 1);

        return $phone;
    }

    public function init()
    {
      parent::init();

      $this->hash = md5(md5(time()));
      $this->status = "Ожидает проверки менеджером";
    }

    /**
      * Пересчитывает корзину пользователя (COOKIE)
      */
    public function recountCartCookie($cartItem, $cart = 0)
    {
      if (!$cart)
        $cart = (array) json_decode(Site::getCookies("cart"));

      // Обнулим корзину и пересчитаем все заново
      $cart["total"] = $cart["price"] = $cart["sale"] = 0;
      foreach ($cartItem as $row) {
        $row = (array) $row;
        $cart["price"] += $row["price"];
        $cart["sale"] += $row["sale"];
        $cart["total"] += $row["price"] - $row["sale"];
      }

      return $cart;
    }

    /**
      * Сверяет пользовательскую корзину с ценами в БД (COOKIE -> БД)
      */
    public function checkPriceCartCookie($cart = 0, $cartItem = 0)
    {
      // Флаг указывает что весь товар в наличии
      $check_instock = 1;

      // Получаем данные из COOKIE
      if (!$cartItem)
        $cartItem = json_decode(Site::getCookies("cartItem"));
      if (!$cart)
        $cart = (array) json_decode(Site::getCookies("cart"));

      $id = [];
      // Получим все ID мебели из корзины
      foreach ($cartItem as $row) {
        $row = (array) $row;
        // Если такой ID уже есть то не будем его добавлять
        if (!in_array($row['id_items'], $id)){
          $id[] = (int) $row['id_items'];
        }
      }

      // Получим товар из БД
      $items = (new \yii\db\Query())
          ->from("items f")
          ->where(["id_items" => $id])
          ->All();

      // Переберем полученный массив, сделаем ключем массивов id мебели
      foreach ($items as $row) {
        $furniture[$row["id_items"]] = $row;
      }

      // Пройдем по массиву из корзины, и проверим совпадает ли цена
      $recount = $all_price = $all_sale = $all_total = 0;
      foreach ($cartItem as $key => $row) {
        $row = (array) $row;

        // Проверяем есть ли этот товар в наличии
        if (!Items::checkItemsInstock($furniture[$row["id_items"]], $row["count"], $row["color"])) {
          // Пометим глобальным флагом
          $check_instock =  0;
          // Пометим определенный товар, которого нет в наличии
          $cartItem->$key->check_instock = 0;
        } else {
          $cartItem->$key->check_instock = 1;
        }

        // Цена товара из БД
        $price = $furniture[$row["id_items"]]["price"] * $row["count"];
        // Скидка товара из БД
        $sale = ($furniture[$row["id_items"]]["price"] - $furniture[$row["id_items"]]["sale_price"]) * $row["count"];
        // Если скидки нет ставим 0
        $sale = !$furniture[$row["id_items"]]["sale"] ? 0 : $sale;

        // Соберем данные для проверки общей стоимости
        $all_price += $price;
        $all_sale += $sale;
        $all_total += $price + $sale;

        // Проверяем есть ли такой товар в БД если нет, то удалим из корзины
        if (!isset($furniture[$row["id_items"]])) {
          unset($cartItem->$key);
          $recount = 1;
        } else {
          // Проверим цену по скидке и без
          if ( ($price != $row["price"]) || ($sale != $row["sale"]) ) {
            // Если цена изменилась, исправим
            $cartItem->$key->price = $price;
            $cartItem->$key->sale = $sale;
            $recount = 1;
          }
        }
      }

      // Проверим совпадают ли итоговые цыфры с данными КУКОВ
      if ($cart['price'] != $all_price && $cart['sale'] != $all_sale && $cart-['total'] != $all_total) {
        $recount = 1;
      }

      return ["recount" => $recount, "cartItem" => $cartItem, "cart" => $cart, "items" => $furniture, 'check_instock' => $check_instock];
    }


    /**
      * Заполняет массивс данными о товаре,
      * который добавляется в корзину
      */
    public function hashSave($hash)
    {
      $cartHash = (array) json_decode(Site::getCookies("cartHash"));

      $cartHash[] = $hash;
      Site::setCookies("cartHash", json_encode($cartHash));
    }


    /**
      * Отправим на Email информацию о новом заказе
      */
    public function sendMailOrder($order)
    {
      // От кого письмо
      $from = Yii::$app->params['supportEmail'];
      // Кому письмо
      $to = Yii::$app->params['emailNotification'];

      $result = Yii::$app->mailer
        ->compose('@common/mail/mails/new_order.php', ["cart" => $order['cart'], "cartItem"=>$order['cartItem']]) // здесь устанавливается результат рендеринга вида в тело сообщения
        ->setFrom($from)
        ->setTo($to)
        ->setSubject("Новая заказ ".date("n M H:i", time()))
        ->send();

      if ($result) {
        return ["status" => 1];
      } else {
        return ["status"=> 0, 'text' => "Произошла ошибка. Попробуйте перезагрузить страницу"];
      }
    }


    /**
      * Заполняет массивс данными о товаре,
      * который добавляется в корзину
      */
    public function initCartItemCookie($post)
    {
      // Получаем данные из COOKIE
      $cartItem = (array) json_decode(Site::getCookies("cartItem"));
      // Для нового содержимого составим уникальное имя
      $id_item = $_POST["id_items"]."_".$_POST["color"];

      // Если такое товар уже добавлялся переведем его в массив
      if (isset($cartItem[$id_item])){
        $cartItem[$id_item] = (array) $cartItem[$id_item];
      }

      // Заполним массив товара данными от пользователя
      $cartItem[$id_item]["id_items"] = $post["id_items"];
      $cartItem[$id_item]["color"] = (int) $post["color"];
      $cartItem[$id_item]["count"] = (int) $post["count"];

      // Получим товар из БД
      $items = (new \yii\db\Query())
          ->select([
            "i.*",
            "(SELECT src FROM items_image ii WHERE ii.id_items = i.id_items AND ii.color = :id_color) as image_color",
          ])
          ->from("items i")
          ->where("id_items = :id_items")
          ->addParams([':id_color' => $post["color"], ":id_items" =>  $post["id_items"]])
          ->One();
      // Если товар не найден, выходим
      if (!$items)
        return ['status' => 0, 'text' => 'Товар, который вы пытаетесь добавить, не найден'];

      // Проверяем есть ли этот товар в наличии
      if (!Items::checkItemsInstock($items, $cartItem[$id_item]["count"], $cartItem[$id_item]["color"]))
        return ['status' => 0, 'text' => 'Этот товар уже купили или на складе нет такого колличества, пожалуйста обновите страницу'];


      // Заполним массив товара данными из БД
      $cartItem[$id_item]["title"] = $items["title"];
      $cartItem[$id_item]["price"] = $items["price"] * $cartItem[$id_item]["count"];
      $cartItem[$id_item]["sale"] = $items["sale"] ? ($items["price"] - $items["sale_price"]) * $cartItem[$id_item]["count"] : 0;
      $cartItem[$id_item]["image"] = $items['image_color'] ? $items['image_color'] : explode(",", $items["image"])[0];
      $cartItem[$id_item]["image"] = str_replace("/", "/@", $cartItem[$id_item]["image"]);

      return ['status' => 1, 'data' => $cartItem];
    }
}
