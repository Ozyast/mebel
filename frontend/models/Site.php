<?php

namespace app\models;

use Yii;

class Site extends \yii\db\ActiveRecord
{

    public function page()
    {
      // проверим есть ли в кэше данные по меню
      // Если нет то получим их из БД
      return Yii::$app->cache->getOrSet("page", function () {
        $seo_page;
        $page_new;

        // Данные о SEO. Заголовки страниц
        $page = (new \yii\db\Query())
                ->select('id_page, url')
                ->from("seo_page")
                ->where(["visible" => "1"])
                ->orderBy("url ASC")
                ->All();
        $articles = (new \yii\db\Query())
                ->select('id_articles, menu_url, menu_title')
                ->from("articles")
                ->where(["visible" => "1"])
                ->orderBy("id_articles ASC")
                ->All();

        // Данные о категориях
        $category = (new \yii\db\Query())
                ->from("category")
                ->where(["visible" => "1"])
                ->orderBy("name_ru ASC")
                ->All();
        // Данные о подкатегориях
        if (Yii::$app->params['siteSubCategory']) {
          $subcategory = (new \yii\db\Query())
                  ->from("subcategory")
                  ->where(["visible" => "1"])
                  ->orderBy("name_ru ASC")
                  ->All();
        }

        // Изменим ключи для категорий
        $category = Helper::indexByKey($category, 'name_en');
        $category_id = Helper::indexByKey($category, 'id_category');
        if (Yii::$app->params['siteCategoryGroup'])
          $category_group = Helper::indexByKey($category, 'group', 1);

        // Статьи
        $articles = Helper::indexByKey($articles, 'menu_url');

        // Изменим ключи для подкатегорий
        if (Yii::$app->params['siteSubCategory'] && $subcategory)
          $subcategory = Helper::indexByKey($subcategory, 'id_subcategory');

        // Для быстрого поиска сформируем еще массив с SEO
        foreach ($page as $row) {
          $seo_page[$row['id_page']] = $row;
          $page_new[$row['id_page']] = $row['url'];
        }

        return [
          "category" => $category,
          'category_id' => $category_id,
          'category_group' => $category_group,
          "subcategory" => $subcategory,
          'page' => $page_new,
          'seo_page' => $seo_page,
          'articles' => $articles,
        ];
      });
    }


    public function hasCookies($cookie_name){
      return Yii::$app->request->cookies->has($cookie_name);
    }

    public function delCookies($cookie_name){
      return Yii::$app->response->cookies->remove($cookie_name);
    }

    public function getCookies($cookie_name){
      return Yii::$app->request->cookies->getValue($cookie_name);
    }

    public function setCookies($name, $value, $expire = 0){
      $cookie = [
        'name' => $name,
        'value' => $value,
        'expire' => $expire ? $expire : time() + 86400 * 30,
      ];

      Yii::$app->response->cookies->add(new \yii\web\Cookie($cookie));
      return $cookie;
    }


    /**
      * Получаем ссылку страныцы на которую мы переходим разбираем ее
      * и проверяем ее в массие сео_страниц. Если страница есть вернем данные
      * по этой странице: заголовок, описание
      */
    public static function getSeoPage()
    {
      $url = $_SERVER['REQUEST_URI'];

      // Если есть параметры
      if (strpos($url, '?')) {
        $url = substr($url, 0, strpos($url, '?'));

        $params = explode('&', $_SERVER['QUERY_STRING']);
        $param = [];
        // переберм параметры и оставим только важные
        foreach ($params as $value) {
          preg_match('/^([^=]*)=(.*)/i', $value, $new);
          if ($new[1] == 'page')
          $param[0] = $new[1].'='.$new[2];

          if ($new[1] == 'id')
          $param[1] = $new[1].'='.$new[2];
        }

        // Если есть параметры сформируем новый УРЛ
        if (!empty($param))
        $url = $url.'?'.implode("&", $param);
      }

      // Ищем такой УРЛ в массиве
      if (!empty(Yii::$app->controller->page['page'])) {
        $key = array_search($url, Yii::$app->controller->page['page']);
        if ($key) {
          // Данные о SEO. Заголовки страниц
          $page = (new \yii\db\Query())
                  ->from("seo_page")
                  ->where(["id_page" => $key])
                  ->One();
          if ($page)
            return $page;
        }
      }

      return;
    }
}
