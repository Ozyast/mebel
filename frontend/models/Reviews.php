<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_review".
 *
 * @property integer $id_items_review
 * @property string $user_name
 * @property string $user_email
 * @property string $user_ip
 * @property string $text
 * @property integer $id_items
 * @property string $created_at
 * @property string $updated_at
 * @property integer $visible
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'user_email', 'text'], 'required'],
            [['text'], 'string'],
            [['id_items', 'visible'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_name', 'user_email'], 'string', 'max' => 100],
            ['user_ip', 'default', 'value' => function ($model, $attribute) {
              return Yii::$app->getRequest()->getUserIP();
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_items_review' => 'ID',
            'user_name' => 'Ваше имя',
            'user_email' => 'Ваш Email',
            'user_ip' => 'IP',
            'text' => 'Текст отзыва',
            'id_items' => 'ID мебели',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'visible' => 'Visible',
        ];
    }
}
