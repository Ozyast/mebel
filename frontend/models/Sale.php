<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sale".
 *
 * @property integer $id_sale
 * @property string $image
 * @property string $title
 * @property string $sub_title
 * @property string $text
 * @property string $button_p_name
 * @property string $button_p_url
 * @property string $button_s_name
 * @property string $button_s_url
 * @property integer $visible
 */
class Sale extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'sale';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['image', 'title', 'sub_title', 'button_p_name', 'button_p_url', 'button_s_name', 'button_s_url'], 'required'],
      [['text'], 'string'],
      [['visible'], 'integer'],
      [['image', 'title', 'sub_title', 'button_p_name', 'button_p_url', 'button_s_name', 'button_s_url'], 'string', 'max' => 100],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_sale' => 'Id Sale',
      'image' => 'Image',
      'title' => 'Title',
      'sub_title' => 'Sub Title',
      'text' => 'Text',
      'button_p_name' => 'Button P Name',
      'button_p_url' => 'Button P Url',
      'button_s_name' => 'Button S Name',
      'button_s_url' => 'Button S Url',
      'visible' => 'Visible',
    ];
  }
}
