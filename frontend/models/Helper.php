<?php

namespace app\models;

use Yii;

class Helper
{

   // Отправляет сообщение в скрипт alert.js
   // public static function send($message, $type)
   // {
   //    $message = CJavaScript::encode(array(
   //                "message" => $message,
   //                "type" => $type,
   //    ));
   //    Yii::app()->clientScript->registerScript(
   //            'appConfig', "var message = " . $message . ";", CClientScript::POS_HEAD);
   // }


   /**
    * Преобразует время в формат (3 мин. назад)
    */
   public static function getTimeAgo($date_time)
   {
      $timeAgo = time() - (int) $date_time;
      $timePer = array(
          'day' => array(3600 * 24, 'дн.'),
          'hour' => array(3600, ''),
          'min' => array(60, 'мин.'),
          'sek' => array(1, 'сек.'),
      );

      foreach ($timePer as $type => $tp) {
         $tpn = floor($timeAgo / $tp[0]);
         if ($tpn) {
            switch ($type) {
               case 'hour':
                  if (in_array($tpn, array(1, 21))) {
                     $tp[1] = 'час';
                  } elseif (in_array($tpn, array(2, 3, 4, 22, 23))) {
                     $tp[1] = 'часa';
                  } else {
                     $tp[1] = 'часов';
                  }
                  break;
            }
            return $tpn . ' ' . $tp[1] . ' назад';
         }
      }
   }


   public static function getTimeType1($date_time)
   {
      $month_ru = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
      $date = date_format(date_create($date_time), "j, Y");
      $month = date_format(date_create($date_time), "n");

      return $month_ru[$month]." ".$date;
   }


   /**
     * Принимает массив из БД
     * Возвращает масссив индексами которого является значение $key
     * $group - группировать элементы. (Например разбиваем по группам и в каждой
     *          группе по два элемента, без группировки один перезапишет другой,
     *          а с группировкой поместяться в массив оба)
     */
   public static function indexByKey($array, $key, $group = 0)
   {
     $new_array = [];
     foreach ($array as $row) {
       if (!is_array($row)) {
         if ($group) {
           $new_array[$row->$key][] = $row;
         } else {
           $new_array[$row->$key] = $row;
         }
       } else {
         if ($group) {
           $new_array[$row[$key]][] = $row;
         } else {
           $new_array[$row[$key]] = $row;
         }
       }
     }

     return $new_array;
   }

   /**
    * Подготовит данные для постраничной разбивки
    *
    * Входные данные:
    * $count_data - кол-во всех записей.
    * $count_data_str - кол-во записей на страницу.
    * $str - страница на которой сейчас находимся
    */
   public static function pages($count_data, $count_data_str, $str = 1)
   {
      // Узнаем кол-во страниц (всего)
      $pages['length'] = ceil($count_data / $count_data_str);
      $pages['str'] = $str;
      $pages['count_data'] = $count_data;

      //Определяем URL страницы для доступа к следующим и предыдущим новостям
      $baseUrl = Yii::$app->getRequest();
      // pathInfo для ссылок с особыми правилами
      if (Yii::$app->controller->action->id != 'index')
        $pages['controller'] = $baseUrl->pathInfo;
      else
        $pages['controller'] = Yii::$app->controller->id . "/" . Yii::$app->controller->action->id;

      // Получим GET параметр (var=val) из http://site.ru/forum/index?var=val
      $getQueryUrl = $baseUrl->getQueryString();
      $getQueryUrl = preg_replace("/&*page=[0-9]+/i", '', $getQueryUrl);
//      $getQueryUrl = preg_replace("/\?*/i", '', $getQueryUrl);

      //$pages['url'] = Yii::$app->urlManager->createUrl($pages['url']);

      // Если есть GET параметры, запиишем их к URL
      if (strlen($getQueryUrl))
         $pages['get'] = $getQueryUrl[0] == '&' ? $getQueryUrl : "&".$getQueryUrl;

      return $pages;
   }

   /**
    * Создает массив со страницами дял вывода пагинации на страницу
    * Входные данные:
    * $pages - Массив с данными полученный из финкции Helper::pages();
    * Изменено 3.10.16 (Самая свежая)
    * Изменена: 3.10.17 ( - Добавлено: выделение первой страницы когда пользователь пришел без "/page/1")
    * Изменена: 3.10.17 ( - Добавлено: вывод дефолтных значений для вперед\Назад, чтобы они не показывались когда страница всего одна)
    * Изменена: 2.10.18 ( - Вывел вычисления в Helper, представление сделал чистым )
    */
   public static function pagesView($pages)
   {
     // Обычная страница, к примеру первая страница без /page/
     $pages['home_url'] = $_SERVER["REQUEST_URI"];
     // Сколько страниц выводить
     $pages["count_str"] = isset($pages['count_str']) ? $pages['count_str'] : 6;
     $pages['next_text'] = isset($pages['next_text']) ? $pages['next_text'] : ">";
     $pages['back_text'] = isset($pages['back_text']) ? $pages['back_text'] : "<";

     // Выводить ли кнопки назад
     $back = $next = $pages['str'];
     $back_visible = $next_visible = 0;
     $url_back = $url_next = $pages['home_url'];
     // назад
     if( ($pages['str'] > 1) && ($pages['length'] > 1) ){
       $back = $pages['str'] - 1;
       $pages['page'][] = ["page" => $back, "visible" => $back_visible, "text" => $pages['back_text']];
     }

     // Если страниц больше одной
     if ($pages['length'] > 1) {

       // Найдем крайнюю левую и правую страницу страницу
       // Если активная 4 страница, а $count_str=4 то крайняя левая = 2 а правая = 6
       $str_left = $pages['str'] - ceil($pages["count_str"] / 2);
       $str_right = $pages['str'] + ceil($pages["count_str"] / 2);
       if ($str_left <= 1) {
         $lim_str_left = $pages['str'];
       } else {
         $lim_str_left = $str_left;
       }

       if ($str_right >= $pages['length']) {
         $lim_str_right = $pages['length'] - 1;
       } else {
         $lim_str_right = $str_right;
       }

       // Выведем страницу 1
       $href = str_replace("?page={{page}}", "", $pages['url']);
       $href = str_replace("?$", "", $href);
       $href = str_replace("/index", "", $href);
       // Если мы на этой странице находимся
       $active = $pages['str'] <= 1 ? 1 : 0;
       $pages['page'][] = ["page" => 1, "active" => $active, "text" => 1];

       if ($pages['str'] > 4 && $pages["count_str"] != $pages['length']) {
         $pages['page'][] = ["page" => 0, "active" => $active, "text" => ".."];
       }

       //Если активная страница больше чем $count_str (7)
       if (($pages['str'] >= $pages["count_str"]) and ( $pages['length'] > $pages["count_str"])) {
         $last_print = 0;
         for ($i = $lim_str_left; $i <= $lim_str_right; $i++) {
           $active = $i == $pages['str'] ? 1 : 0;
           $pages['page'][] = ["page" => $i, "active" => $active, "text" => $i];

           $last_print = $i;
         }
         //Если активная страниц меньше чем $count_str (9)
       } else {
         $last_print = 0;
         for ($i = 2; $i <= $pages["count_str"] and $i <= $pages['length']-1; $i++) {
           $active = $i == $pages['str'] ? 1 : 0;
           $pages['page'][] = ["page" => $i, "active" => $active, "text" => $i];
           $last_print = $i;
         }
       }

       if ($pages['str'] < ($pages['length'] - ceil($pages["count_str"] / 2)) && $pages["count_str"] != $pages['length']) {
         $pages['page'][] = ["page" => 0, "active" => 0, "text" => ".."];
       }

       // Выведем последнюю страницу
       $active = $pages['str'] == $pages['length'] ? 1 : 0;
       $pages['page'][] = ["page" => $pages['length'], "active" => $active, "text" => $pages['length']];
     }

     // и вперед
     if(($pages['str'] < $pages['length']-1) && ($pages['length']>1)){
       $next = $pages['str'] + 1;
       $pages['page'][] = ["page" => $next, "visible" => $next_visible, "text" => $pages['next_text']];
     }

     return $pages;
   }


    // Отправляем письмо на почту пользователя
    // public static function sendMail($email, $title, $template, $data = null)
    // {
    //    $from = "Servios <support@servios.ru>";
    //
    //    // Указываем правильный MIME-тип сообщения:
    //    $headers = 'MIME-Version: 1.0' . "\r\n";
    //    $headers .= 'Content-type: text/html;
    //      charset=utf-8' . "\r\n";
    //    // Добавляем необходимые заголовки
    //    $headers .='To: ' . $email . "\r\n";
    //    $headers .='From: ' . $from . "\r\n";
    //
    //    $controller = new CController('context');
    //    $template = $controller->renderPartial($template, array(
    //        'title' => $title,
    //        'data' => $data,
    //            ), true);
    //
    //    $return = mail($email, $title, $template, $headers);
    //    return $return;
    // }

   /**
    *
    * @param type $answer
    * @param int $return
    * @param type $bd
    * @param int $echo
    * @param type $data
    * @return string
    * Helper::look($model->open_ssh("10.50.70.7"), 1, function($answer, $data){
    *    echo "<pre>";
    *    print_r($data);
    *    echo "</pre>";
    * }, 0, ["model"=>$model]);
    */
  //  static public function look($answer, $echo, $bd, $return, $data = null)
  //  {
  //   if (!$answer["status"]) {
  //     if ($echo && !is_callable($echo)) {
  //       echo json_encode($answer);
  //     } elseif (is_callable($echo)) {
  //       $echo($answer, $data);
  //     }
  //
  //     if ($bd && !is_callable($bd)) {
  //       Status::add($data->id_equipment, $answer['text'], "danger", 1);
  //     } elseif (is_callable($bd)) {
  //       $bd($answer, $data);
  //     }
  //
  //     if ($return && !is_callable($return)) {
  //       if ($answer['status'] == 0)
  //         $answer['status'] = "danger";
  //       elseif ($answer['status'] == 1)
  //         $answer['status'] = "success";
  //
  //       //      if (!strlen($answer['text']) && $answer['status'] == "success")
  //       echo json_encode($answer);
  //       exit(0);
  //     }elseif (is_callable($return)) {
  //       $return($answer, $data);
  //     }
  //   }
  //   return $answer;
  // }

  /**
    * Переводит русский текст в транслит
    */
  // static public function ruEnTranslate($text)
  // {
  //   $converter = array(
  //       'а' => 'a',   'б' => 'b',   'в' => 'v',
  //       'г' => 'g',   'д' => 'd',   'е' => 'e',
  //       'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
  //       'и' => 'i',   'й' => 'y',   'к' => 'k',
  //       'л' => 'l',   'м' => 'm',   'н' => 'n',
  //       'о' => 'o',   'п' => 'p',   'р' => 'r',
  //       'с' => 's',   'т' => 't',   'у' => 'u',
  //       'ф' => 'f',   'х' => 'h',   'ц' => 'c',
  //       'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
  //       'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
  //       'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
  //
  //       'А' => 'A',   'Б' => 'B',   'В' => 'V',
  //       'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
  //       'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
  //       'И' => 'I',   'Й' => 'Y',   'К' => 'K',
  //       'Л' => 'L',   'М' => 'M',   'Н' => 'N',
  //       'О' => 'O',   'П' => 'P',   'Р' => 'R',
  //       'С' => 'S',   'Т' => 'T',   'У' => 'U',
  //       'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
  //       'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
  //       'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
  //       'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
  //   );
  //   return strtr($text, $converter);
  // }


  // Выводим ошибку
   // Helper::wtf(array("status"=>"danger", "text"=>$message[0]), array("echo", "bd", "return"));
//   static public function wtf($wtf, $action){
//
//     if ((is_string($action) && $action == "echo") || (is_array($action) && array_search("echo", $action) !== false)){
//       if (!isset($wtf['status']))
//         $wtf["status"] = "danger";
//
//       echo json_encode($wtf);
//       exit(0);
//     }
//
//     if ((is_string($action) && $action == "bd") || (is_array($action) && array_search("bd", $action) !== false))
//      Error::add($wtf['message'], $wtf['code'], $wtf['text']);
//
//     if ((is_string($action) && $action == "return") || (is_array($action) && array_search("return", $action) !== false))
//      return array("status"=>0, "text"=>$wtf['message'], "code"=>$wtf['code'], "data"=>$wtf['text']);
//   }


}
