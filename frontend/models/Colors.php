<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "colors".
 *
 * @property integer $id_colors
 * @property string $image
 * @property string $name
 * @property integer $main_color
 */
class Colors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'colors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['main_color'], 'integer'],
            [['main_color'], 'default', 'value' => 0 ],
            [['image', 'name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_colors' => 'Id Colors',
            'image' => 'Image',
            'name' => 'Name',
            'main_color' => 'Main Color',
        ];
    }
}
