<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Items".
 *
 * @property integer $id_items
 * @property string $title
 * @property string $image
 * @property string $color
 * @property integer $price
 * @property integer $sale
 * @property integer $sale_price
 * @property integer $id_modules
 * @property string $id_collections
 * @property string $additions
 * @property string $services
 * @property string $category
 * @property string $iterior
 * @property integer $instock
 * @property integer $total_sold
 * @property string $created_at
 * @property string $updated_at
 * @property integer $visible_catalog
 * @property integer $visible
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'image', 'color', 'id_collections', 'additions', 'services', 'category', 'iterior'], 'required'],
            [['price', 'sale', 'sale_price', 'id_modules', 'instock', 'total_sold', 'visible_catalog', 'visible'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'id_collections', 'additions', 'services', 'category', 'iterior'], 'string', 'max' => 250],
            [['image', 'color'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_items' => 'Id Items',
            'title' => 'Title',
            'image' => 'Image',
            'color' => 'Color',
            'price' => 'Price',
            'sale' => 'Sale',
            'sale_price' => 'Sale Price',
            'id_modules' => 'Id Modules',
            'id_collections' => 'Id Collections',
            'additions' => 'Additions',
            'services' => 'Services',
            'category' => 'Category',
            'iterior' => 'Iterior',
            'instock' => 'Instock',
            'total_sold' => 'Total Sold',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'visible_catalog' => 'Visible Catalog',
            'visible' => 'Visible',
        ];
    }

    /**
      * ЦВЕТА мебели
      * @return \yii\db\ActiveQuery
      */
    public function getColors()
    {
      return $this->hasMany(Colors::className(), ['id_colors' => 'id_colors'])
              ->viaTable('items_colors_relationship', ['id_items' => 'id_items']);
    }

    /*
    * Возвращает критерии для поиска данных в главную таблицу, на индекс.
    */
    public static function criteria() {
      // Критерии для поиска
      return Items::find()
              ->where(["items.visible" => 1])
              ->orderBy("sold ASC");
    }

    /**
      * Кол-во товаров на страницу
      */
    public static function getCountSortBar()
    {
      return [10, 20, 40, 60];
    }

    public static function getOrderSortBar()
    {
      return [
        ['name' => '----', 'code' => "id_items DESC"],
        ['name' => 'Цена: Сначала дешевые', 'code' => 'price ASC'],
        ['name' => 'Цена: Сначала дорогие', 'code' => 'price DESC'],
        ['name' => 'По наличию', 'code' => 'sold ASC'],
        ['name' => 'Акции', 'code' => 'sale DESC']
      ];
    }

    /**
      * Получает данные для фильтра
      */
    public function getFilter()
    {
      // проверим есть ли в кэше данные по фильтрам
      // Если нет то получим их из БД
      return Yii::$app->cache->getOrSet("filterBar", function () {
        // МИН и МАКС для фильтра
        $items_min_max = (new \yii\db\Query())
                ->select([
                  "(SELECT min(sale_price) FROM items WHERE sale = 1 AND visible = 1) as min_sale",
                  "(SELECT min(price) FROM items WHERE visible = 1) as min",
                  "(SELECT max(price) FROM items WHERE visible = 1) as max",
                ])
                ->One();
        // ЦВЕТА для фильтра
        $color = (new \yii\db\Query())
                ->from("colors")
                ->where(["main_color" => 0])
                ->All();
        // ПРОИЗВОДИТЕЛИ для фильтра
        $manufacturer = (new \yii\db\Query())
                ->from("manufacturer")
                ->where(["visible" => 1])
                ->All();

        // Если цена по скидки меньше главной цены
        if ($items_min_max['min_sale'] && $items_min_max['min_sale'] < $items_min_max['min'])
          $items_min_max['min'] = $items_min_max['min_sale'];

        return ["color" => $color, "min_max" => $items_min_max, "manufacturer" => $manufacturer];
      });
    }

    /**
      * Устанавливает дополнительный фильтр для запроса.
      */
    public function setFilter($criteria)
    {
      // ЦЕНА
      if (isset($_GET["pf"]) || isset($_GET["pt"])) {
        $pf = (int) $_GET["pf"];
        $pt = (int) $_GET["pt"];
        // Цена больше $pf и меньше $pt или цена по скидки больше $pf и меньше $pt и флаг скидки активирован
        $criteria->andWhere(["or", ["and", [">=", "price", $pf], ["<=", "price", $pt], ["=", "sale", 0]], ["and", [">=", "sale_price", $pf], ["<=", "sale_price", $pt], ["=", "sale", 1]]]);
      }

      // СОРТИРОВКА
      if (isset($_GET["so"]) && $_GET["so"]) {
        $so = (int) $_GET['so'];
        $order = Items::getOrderSortBar();
        $criteria->orderBy($order[$so]['code']);
      }

      // ЦВЕТ
      if (isset($_GET["cl"])) {
        $cl = $_GET["cl"];
        $colors = explode(",", $cl);
        $color;
        foreach ($colors as $key => $row){
          $color[] = (int) $row;
        }
        $criteria->innerJoin("items_colors_relationship fcr", "items.id_items = fcr.id_items AND fcr.id_colors IN (".implode(",", $color).")");
        $criteria->groupBy("items.id_items");
        // $criteria->joinWith([
        //   'colors' => function ($query) use ($color) {
        //       $query->andWhere(['in', 'colors.id_colors', $color]);
        //   },
        // ]);
      }

      // Производитель
      if (isset($_GET["ma"])) {
        $ma = $_GET["ma"];
        $manufacturers = explode(",", $ma);
        $manufacturer = ["OR"];
        foreach ($manufacturers as $key => $row){
          $row = (int) $row;
          $manufacturer[] = ["=", "id_manufacturer", "$row"];
        }
        $criteria->andWhere(["AND", $manufacturer]);
      }

      return $criteria;
    }


    /**
      * Посчитаем кол-во забронированных товаров
      */
    // public function getReserve($id)
    // {
    //   // Получим все товары которые куплены но не подтверждены
    //   $count_reserve = (new \yii\db\Query())
    //         ->select("ci.color, ci.count")
    //         ->from("cart_item ci")
    //         ->where("ci.id_items = :id", [":id" => $id])
    //         ->rightJoin("cart c", "c.id_cart = ci.id_cart AND done = 0")
    //         ->all();
    //     $count_reserve_sort['count'] = 0;
    //
    //     if ($count_reserve) {
    //       foreach ($count_reserve as $key => $row) {
    //         // Если товар такого цвета уже был, прибавим к его кол-ву этот товар
    //         if (isset($count_reserve_sort[$row['color']])){
    //           $count_reserve_sort[$row['color']]['count'] += $row['count'];
    //         } else {
    //           $count_reserve_sort[$row['color']] = $row;
    //         }
    //         // Подсччитываем общее кол-во товара
    //         $count_reserve_sort['count'] += $row['count'];
    //       }
    //     }
    //     return $count_reserve_sort;
    // }


    /**
      * Получаем похожие товары
      */
    public function getSimilar($id, $category)
    {
      // Найдем похожие товары по таблице "похожих товаров"
      $similar = (new \yii\db\Query())
            ->from("similar s")
            ->where("s.id_items_master = :id OR s.id_items_slave = :id", [":id" => $id])
            ->rightJoin("items i", "(i.id_items = s.id_items_master OR i.id_items = s.id_items_slave) AND i.id_items != :id AND i.visible = 1")
            ->groupBy("i.id_items")
            ->limit(10)
            ->all();

      // Если мы не нашли похожих товаров в таблице, возьмом товары из той же категории что и наш товар
      if (!$similar) {
        $similar = (new \yii\db\Query())
        ->from("items i")
        ->where("i.id_items != :id AND i.visible = 1", [":id" => $id])
        ->rightJoin("items_category_relationship icr", "i.id_items = icr.id_items AND icr.id_category = :id_category", [":id_category" => $category])
        ->groupBy("i.id_items")
        ->limit(10)
        ->all();
      }

      return $similar;
    }


    /**
      * Проверяем какое кол-во товара осталось в наличии по цветам
      */
    public function checkAllItemsInstock($items, $color)
    {
      if (!$items)
        return false;

      // Если товар отмечен как бесконечный sold = 0 и instock = 0
      if (!$items['instock'] && !$items['sold'] && !$items['instock_color']) {
        $reserve['active'] = 1;

      // Если кол-во товара равно нулю
      } elseif ($items['instock'] <= 0) {
        $reserve['active'] = 0;

        // Если количество товара зависит от кол-ва товара разного цвета
      } elseif ($items['instock_color']) {
        // Получим кол-во нашего товара который в резерве
        $reserve_bd = (new \yii\db\Query())
          ->select("icr.visible, icr.instock, SUM(ci.count) as count, ci.color")
          ->from("cart_item ci")
          ->where("ci.id_items = :id")
          ->rightJoin("cart c", "c.id_cart = ci.id_cart AND done = 0")
          ->leftJoin("items_colors_relationship icr", 'icr.id_items = :id AND icr.id_colors = ci.color')
          ->addParams([":id" => $items['id_items']])
          ->groupBy("ci.color")
          ->All();

        $reserve['count'] = 0;
        foreach ($reserve_bd as $row) {
          $reserve_color[$row['color']] = $row;
          $reserve['count'] += $row['count'];
        }

        if ($items['instock'] - $reserve['count'] <= 0){
          $reserve['active'] = 0;
        } else {
          foreach ($color as $key => $row) {
            if (!$row['visible'] || !$row['instock']) {
              $color[$key]['active'] = 0;
            // Проверим кол-ва в остатках товара этого цвета
            }elseif (($row['instock'] - $reserve_color[$row['id_colors']]['count']) > 0 ) {
              $color[$key]['active'] = 1;
            } else {
              $color[$key]['active'] = 0;
            }
          }
        }

        // Если кол-во товара указано общее
      } else {
        // Получим кол-во нашего товара который в резерве
        $reserve_bd = (new \yii\db\Query())
              ->select([
                "SUM(ci.count) as count",
              ])
              ->from("cart_item ci")
              ->where("ci.id_items = :id")
              ->rightJoin("cart c", "c.id_cart = ci.id_cart AND done = 0")
              ->addParams([":id" => $items['id_items']])
              ->One();

        $reserve['count'] = $reserve_bd['count'];

        if ((($items['instock'] - $reserve_bd['count'])) > 0) {
          $reserve['active'] = 1;
        } else {
          $reserve['active'] = 0;
        }
      }

      $reserve['colors'] = $color;
      return $reserve;
    }

    /**
      * Проверяем доступен ли товар с указанным кол-вом и цветом к заказу
      * $items - Данные товара из БД
      * $count - Какое количество товара нужно
      * $color - Какой цвет нужен (Если увета у товара нет то 0)
      */
    public function checkItemsInstock($items, $count, $color = 0)
    {
      if (!$items)
        return false;

      // Если товар отмечен как бесконечный sold = 0 и instock = 0
      if (!$items['instock'] && !$itms['sold']  && !$items['instock_color']){
        return true;
      // Если кол-во товара равно нулю
      }

      if ($items['instock'] <= 0) {
        return false;
      }

      // Если количество товара зависит от кол-ва товара разного цвета
      if ($items['instock_color']) {
        // Получим кол-во нашего товара
        $colors = (new \yii\db\Query())
          ->select("icr.visible, icr.instock, icr.id_colors, icr.id_items")
          ->from("items_colors_relationship icr")
          ->where("icr.id_items = :id AND icr.id_colors = :color")
          ->addParams([":id" => $items['id_items'], ":color" => $color])
          ->one();
        // Получим кол-во резерва
        $reserve = (new \yii\db\Query())
            ->select([
              "SUM(ci.count) as count",
            ])
            ->from("cart_item ci")
            ->where("ci.color = :color AND ci.id_items = :id")
            ->rightJoin("cart c", "c.id_cart = ci.id_cart AND done = 0")
            ->addParams([":id" => $items['id_items'], ":color" => $color])
            ->groupBy("ci.color")
            ->one();

        if ((($colors['instock'] - $reserve['count']) - $count) >= 0) {
          return true;
        } else {
          return false;
        }

      // Если кол-во товара указано общее
      } else {
        // Получим кол-во нашего товара который в резерве
        $reserve = (new \yii\db\Query())
        ->select([
          "SUM(ci.count) as count",
        ])
        ->from("cart_item ci")
        ->where("ci.id_items = :id")
        ->rightJoin("cart c", "c.id_cart = ci.id_cart AND done = 0")
        ->addParams([":id" => $items['id_items']])
        ->one();

        if ((($items['instock'] - $reserve['count']) - $count) >= 0) {
          return true;
        } else {
          return false;
        }

      }

      return false;
    }
}
