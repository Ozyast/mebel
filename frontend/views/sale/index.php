<?php
use app\models\Helper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Акции - Экспресс Мебель';
$this->context->description = 'Акции';
$h1 = "Акции";

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>

<div class="breadcrumb-banner-area ptb-30">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?= $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span>Акции</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="blog-page-area pt-30 pb-60">
  <div class="container-fulid">
    <div class="row">

      <?php if (isset($seoPage['text'])): ?>
        <div class="col-xs-10 col-xs-offset-1 pb-40">
          <?= $seoPage['text'] ?>
        </div>
      <?php endif; ?>

      <?php foreach ($sale as $row): ?>
        <div class="col-md-offset-2 col-md-8 pt-70">
          <div class="blog-page-wrapper mb-40">
            <div class="blog-page-img">
              <a href="<?= $row['button_p_url'] ?>"><img src="/img/sale/<?= $row['image'] ?>" alt="" /></a>
            </div>
            <div class="blog-page-title text-center">
              <span><?= Helper::getTimeType1($row['created_at']) ?></span>
              <h3><a href="<?= $row['button_p_url'] ?>"><?= $row['title'] ?></a></h3>
              <p><?= $row['text'] ?></p>
            </div>
          </div>
        </div>
      <?php endforeach;?>

    </div>
  </div>
</div>
