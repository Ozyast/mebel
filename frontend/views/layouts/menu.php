<?php
use yii\helpers\Url;

if (!isset($cart) && app\models\Site::hasCookies("cart")) {
  $cart = (array) json_decode(app\models\Site::getCookies("cart"));
  $cartItem = (array) json_decode(app\models\Site::getCookies("cartItem"));
} else {
  $cartItem = [];
  $cart = [];
}
?>

<div class="row" id="nav-menu">
  <div class="col-md-12 pl-0">
    <div class="main-menu hidden-sm hidden-xs">
      <nav class="text-right">
        <ul>
          <li class="active"><a href="/">Главная</a></li>
          <li><a href="<?= Url::to(['site/about']) ?>">О компании</a></li>
          <li><a href="<?= Url::to(['site/payment']) ?>">Оплата товара</a></li>
          <li><a href="<?= Url::to(['site/delivery']) ?>">Доставка и сборка</a></li>
          <li><a href="<?= Url::to(['site/contact']) ?>">Контакты</a></li>
          <li><a href="<?= Url::to(['sale/index']) ?>">Акции</a></li>
          <?php if (isset($articles) && count($articles)): ?>
            <li><a href="<?= Url::to(['articles/index']) ?>">Статьи</a>
              <ul class="submenu">
                <?php foreach ($articles as $art): ?>
    							<li><a href="<?= Url::to(['articles/view', 'name'=>$art['menu_url']]) ?>"><?= $art['menu_title'] ?></a></li>
                <?php endforeach; ?>
  						</ul>
            </li>
          <?php endif; ?>
        </ul>
      </nav>
    </div>
    <div class="mobile-menu visible-xs visible-sm">
      <div class="col-xs-9" id="mob-menu-logo">
        <a href="<?= Url::to(['/']) ?>"><img src="/img/logo/logo-phone.png" alt=""/></a>
      </div>
      <div class="col-xs-2">
        <nav id="mob-menu">
          <ul>
            <li class="active"><a href="/">Главная</a></li>
            <li class="acticomve"><a href="javascript:void(0)">Категории <i class="fa fa-angle-down"></i></a>
              <ul class="submenu">
                <?php foreach ($category as $key => $row): ?>
                  <li><a href="<?= Url::to(['items/category', 'cat'=>$row['name_en']]) ?>"><?= $row['name_ru'] ?></a></li>
                <?php endforeach; ?>
              </ul>
            </li>

            <li><a href="<?= Url::to(['sale/index']) ?>">Акции</a></li>

            <li class="active"><a href="javascript:void(0)">О нас <i class="fa fa-angle-down"></i></a>
              <ul class="submenu">
                <li><a href="<?= Url::to(['site/about']) ?>">О компании</a></li>
                <li><a href="<?= Url::to(['site/payment']) ?>">Оплата товара</a></li>
                <li><a href="<?= Url::to(['site/delivery']) ?>">Доставка и сборка</a></li>
                <li><a href="<?= Url::to(['site/contact']) ?>">Контакты</a></li>
              </ul>
            </li>

            <?php if (isset($articles) && count($articles)): ?>
              <li><a href="<?= Url::to(['articles/index']) ?>">Статьи</a>
                <ul class="submenu">
                  <?php foreach ($articles as $art): ?>
      							<li><a href="<?= Url::to(['articles/view', 'name'=>$art['menu_url']]) ?>"><?= $art['menu_title'] ?></a></li>
                  <?php endforeach; ?>
    						</ul>
              </li>
            <?php endif; ?>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="row" id="nav-logo">
  <div class="col-md-3 col-xs-4 hidden-sm hidden-xs">
    <div class="logo">
      <a href="<?= Url::to(['/']) ?>"><img src="/img/logo/logo.png" alt=""/></a>
    </div>
  </div>

  <div class="col-md-8 col-xs-8 text-center">
    <p class="m0 mt1">
      <span class="phone hidden-xs">+7 (918) 667-45-00</span>
      <a href="tel:+79186674500" class="phone visible-xs">+7 (918) 667-45-00</a>
      <br/>
      <span class="hidden-sm hidden-xs">г. Краснодар, Ейское шоссе, п. Березовый 89/1</sapn>
    </p>
  </div>

  <div class="col-md-1 col-xs-4">
    <div class="block-right f-right">
      <div class="cart-icon">
        <ul>
          <li>
            <a href="<?= Url::to(['cart/index']) ?>">
              <img src="/img/logo/cart.png" alt="" />
              <span id="cart-total"><?= count($cartItem) ?></span>
            </a>
            <div class="cart-item">

              <?php
              echo Yii::$app->view->renderFile('@app/views/cart/_cart.php', [
                "cart" => $cart,
                "cart_item" => $cartItem
              ]);
              ?>

            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="main-menu hidden-sm hidden-xs" id="nav-category">
    <nav>
      <ul>
        <?php foreach ($category as $key => $row): ?>
        <li class="active text-center">
          <a href="<?= Url::to(['items/category', 'cat'=>$row['name_en']]) ?>">
            <img src="/img/category/i<?= $row['id_category'] ?>.png"><br/>
            <?= $row['name_ru'] ?>
          </a>
        </li>
        <?php endforeach; ?>

      </ul>
    </nav>
  </div>
</div>
