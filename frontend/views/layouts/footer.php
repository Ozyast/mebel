<?php
  use yii\helpers\Url;
?>

<div class="footer-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="footer-wrapper text-center">
        <div class="footer-logo">
          <a href="/"><img src="/img/logo/logo_white.png" alt="" /></a>
        </div>
        <div class="footer-content">
          <div class="text-white text-center pt2">
            <b>+7 (918) 667-45-00</b><br/>
            <span>г. Краснодар, Ейское шоссе, п. Березовый 89/1</sapn>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom-area">
    <div class="container">
      <div class="row">
        <div class="single-footer">
          <ul class="footer-menu text-center">
            <li><a href="<?= Url::to(["/"]) ?>">Главная</a></li>
            <li><a href="<?= Url::to(["site/about"]) ?>">О нас</a></li>
            <li><a href="<?= Url::to(["site/delivery"]) ?>">Доставка и Сборка</a></li>
            <li><a href="<?= Url::to(["site/payment"]) ?>">Оплата товара</a></li>
            <li><a href="<?= Url::to(["site/contact"]) ?>">Контакты</a></li>
          </ul>

          <div class="copyright text-center">
            <p>&copy; <?= date('Y') ?> Экспресс Мебель</p>
          </div>

          <small class="text-center col-xs-8 col-xs-offset-2 mt2">
            Вся представленная информация носит информационный характер и ни при
            каких условиях не является публичной офертой.
            Ввиду разницы в цветопередаче реальный цвет изделия может отличаться
            от представленного на сайте.
          </small>
        </div>
      </div>
    </div>
  </div>
</div>
