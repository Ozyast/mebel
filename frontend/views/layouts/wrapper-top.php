<?php
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html class="no-js" lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="<?= Html::encode($this->context->description) ?>">

    <!-- <meta name="format-detection" content="telephone=no" /> -->
    <link rel="shortcut icon" href="/img/logo/favicon.png"/>
    <link rel="apple-touch-icon" href="/img/logo/favicon.png">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link href="/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">

    <script src="/js/plugin.min.js"></script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
    (function (d, w, c) {
      (w[c] = w[c] || []).push(function() {
        try {
          w.yaCounter51172133 = new Ya.Metrika2({
            id:51172133,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
          });
        } catch(e) { }
      });

      var n = d.getElementsByTagName("script")[0],
      s = d.createElement("script"),
      f = function () { n.parentNode.insertBefore(s, n); };
      s.type = "text/javascript";
      s.async = true;
      s.src = "https://mc.yandex.ru/metrika/tag.js";

      if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
      } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/51172133" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>

  <div class="wrapper-top">
    <!-- header-start -->
    <div class="header-top">
      <?php
      echo Yii::$app->view->renderFile('@app/views/layouts/menu.php', [
        "category" => $this->context->page["category"],
        "articles" => $this->context->page["articles"],
      ]);
      ?>
    </div>
    <!-- header-end -->

    <?= $content ?>


    <!-- footer-area-start -->
    <footer>
      <?php
      echo Yii::$app->view->renderFile('@app/views/layouts/footer.php', [
        "category" => $this->context->page["category"],
      ]);
      ?>
    </footer>
    <!-- footer-area-end -->
  </div>
  <!-- all js here -->
  <script src="/js/main.min.js"></script>
  <script src="/js/page/pages.js"></script>

  <!-- BEGIN JIVOSITE CODE {literal} -->
  <script type='text/javascript'>
  (function(){ var widget_id = 'zbHvcgfzbV';var d=document;var w=window;function l(){
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
  <!-- {/literal} END JIVOSITE CODE -->
</body>
</html>
