<?php
/* Входные параметры ( Обязательные ) */
// data_table - Массив со всеми данными из таблицы items
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Helper;

$this->title = "Мебель от производителей - Экспресс Мебель";
$this->context->description = 'Мебель от производителей';
$h1 = "Мебель от производителей";

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}

$category = $this->context->actionParams['id'];
?>
<script src="/js/page/items.js"></script>

<div class="breadcrumb-banner-area ptb-20">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span>Продукция</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="shop-area pt-100 pb-70">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <?php
        echo Yii::$app->view->renderFile('@app/views/widget/_filter.php', [
          "price_min" => $filter_min_max["min"],
          "price_max" => $filter_min_max["max"],
          "color" => $filter_color,
          "manufacturer" => $filter_manufacturer,
        ]);
        ?>
      </div>
      <div class="col-md-9">
        <div class="product-tab">
          <div class="shop-select-bar">
            <?php
            echo Yii::$app->view->renderFile('@app/views/widget/_sort_bar.php', [
            ]);
            ?>

            <div id="items_main">
              <?php
              echo Yii::$app->view->renderFile('@app/views/items/_index.php', [
                "data_table" => $data_table,
                "pages" => $pages,
              ]);
              ?>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
