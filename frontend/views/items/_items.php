<?php
use yii\helpers\Url;
?>

<?php foreach ($data_table as $row): ?>
  <div class="col-md-4 col-sm-6">
    <div class="product-wrapper mb-40">
      <div class="product-img">
        <a href="<?= Url::to(['items/view', 'id'=>$row['id_items']]) ?>">
          <?php
            $image = explode(",", $row['image']);
          ?>
          <img src="/img/items/<?= str_replace("/", "/@", $image[0]) ?>" alt="" />
          <?php if(isset($image[1])): ?>
            <img class="secondary-img" src="/img/items/<?= str_replace("/", "/@", $image[1]) ?>" alt="" />
          <?php endif; ?>
        </a>
        <?php if ($row["sale"]): ?>
          <div class="label-product">
            <span class="new"> - <?= round( (1 - $row["sale_price"] / $row["price"]) * 100 ) ?>%</span>
          </div>
        <?php endif; ?>
      </div>
      <div class="product-content text-center">
        <h5>
          <a href="<?= Url::to(['items/view', 'id'=>$row['id_items']]) ?>">
            <?= $row["title"] ?>
          </a>
        </h5>
        <div class="price-box">
          <?php if ($row["sale"] && !$row['sold']): ?>
            <span class="new-price sale"> <?= $row["sale_price"] ?> р. </span>
            <span class="old-price"> <?= $row["price"] ?> </span>
          <?php elseif ($row['sold']): ?>
            <span> Нет в наличии </span>
          <?php else: ?>
            <span class="new-price"> <?= $row["price"] ?> р. </span>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
