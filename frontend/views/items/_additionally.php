<?php
  // Переберем вопросы и найдем на них ответы, если они есть
  $question;
  if (isset($items_question) && count($items_question)) {
    foreach ($items_question as $row) {
      if ($row['id_answer']) {
        $question[$row['id_answer']]["answer"][] = $row;
      } else {
        $question[$row['id_items_question']] = $row;
      }
    }
  }
?>

<div class="pro-info-box">
  <ul class="pro-info-tab" role="tablist">
    <?php if (count($items_modules)): ?>
      <li class="active"><a href="#modules" data-toggle="tab">Модули</a></li>
    <?php endif; ?>
    <li <?= !count($items_modules) ? 'class="active"' : "" ?>><a href="#reviews" data-toggle="tab">Отзывы</a></li>
    <li><a href="#question" data-toggle="tab">Вопросы</a></li>
  </ul>

  <div class="tab-content">

    <?php if (count($items_modules)): ?>
      <div class="tab-pane active" id="modules">
        <ul class="modules-list">
          <?php echo Yii::$app->view->renderFile('@app/views/items/_modules.php', [
                "modules" => $items_modules,
                "active" => 1,
              ]);
          ?>
        </ul>
      </div>
    <?php endif; ?>

    <div class="tab-pane <?= !count($items_modules) ? 'active' : "" ?>" id="reviews">
      <div class="pro-desc">
          <ul class="media-list mb5">
            <?php echo Yii::$app->view->renderFile('@app/views/reviews/_reviews.php', [
                  "items_review" => $items_review,
                ]);
            ?>
          </ul>

          <div class="reviews-form">
            <?php echo Yii::$app->view->renderFile('@app/views/reviews/_form.php', [
                  "items" => $items,
                ]);
            ?>
          </div>

        <button type="button" id="reviews-open-form">Оставить отзыв</button>
      </div>
    </div>

    <div class="tab-pane" id="question">
      <div class="pro-desc">
        <ul class="media-list mb5">
          <?php echo Yii::$app->view->renderFile('@app/views/question/_question.php', [
                "items_question" => $items_question,
              ]);
          ?>
        </ul>

        <div class="question-form">
          <?php echo Yii::$app->view->renderFile('@app/views/question/_form.php', [
                "items" => $items,
              ]);
          ?>
        </div>

        <button type="button" id="question-open-form">Задать вопрос</button>
      </div>
    </div>

  </div>
</div>
