<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $items["title"]." - Экспресс Мебель";
$this->context->description = $items["title"];
$h1 = $items["title"];

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}

$category = explode(",", $items["category"]);
?>
<script src="/js/page/items_view.js"></script>

<div class="breadcrumb-banner-area ptb-20">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="breadcrumb-text text-center">
					<div class="breadcrumb-menu">
						<h1 class="page-title"><?= $h1 ?></h1>
						<ul>
							<li><a href="<?= Url::to(["/"]) ?>">Главная</a></li>
							<li><span><?= $this->context->page['category_id'][$category[0]]["name_ru"] ?></span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if (isset($seoPage['text'])): ?>
  <div class="container pt-40">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <?= $seoPage['text'] ?>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="shop-area">
  <div class="container">
    <div class="row">

      <div class="col-sm-12 col-md-7">
        <div class="product-zoom" id="items-image">
          <!-- Tab panes -->
          <div class="tab-content">
            <?php foreach ($items_image as $key => $image): ?>
              <div class="tab-pane <?= !$key ? "active" : "" ?>" id="image<?= $image["color"] ? $image["color"] : "k".$key ?>">
                <div class="pro-large-img">
                  <img src="/img/items/<?= str_replace("/", "/@", $image["src"]) ?>" alt="" />
                  <a class="popup-link" href="/img/items/<?= $image["src"] ?>">Увеличить <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
          <!-- Nav tabs -->
          <div class="details-tab owl-carousel">
            <?php foreach ($items_image as $key => $image): ?>
              <div <?= !$key ? "class='active'" : "" ?>>
                <a href="#image<?= $image["color"] ? $image["color"] : "k".$key ?>" data-toggle="tab">
                  <img src="/img/items/<?= str_replace("/", "/@", $image["src"]) ?>" alt="" />
                </a>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-md-5">
        <div class="product-details pt-40">

          <div class="pro-ref">
            <p>
              <label>Категория: </label>
              <span>
                <?php foreach (explode(",", $items["category"]) as $key => $value): ?>
                  <?= $key ? "," : "" ?>
                  <a href="<?= Url::to(['items/category', 'cat'=>$this->context->page['category_id'][$value]["name_en"]]) ?>"><?= $this->context->page['category_id'][$value]["name_ru"] ?></a>
                <?php endforeach; ?>
              </span>
            </p>
          </div>

          <?php if (isset($items_description) && count($items_description["description"])): ?>
          <hr/>
          <ul>
            <?php foreach ($items_description["description"] as $key => $value): ?>
              <li><?= $key ?>: <?= $value ?></li>
            <?php endforeach; ?>
          </ul>
          <?php endif ?>

          <div class="price-box mt6">
            <?php if ($items["sale"]): ?>
              <span class="product-details-price"><?= $items['sale_price'] ?> р.</span>
              <span class="product-old-price"><?= $items['price'] ?> р.</span>
            <?php else: ?>
              <span class="product-details-price"><?= $items['price'] ?> р.</span>
            <?php endif; ?>

						<?php if ( !(!$items['instock'] && !$items['sold']) ): ?>
							<p>
								Осталось: <?= $items['instock'] - $reserve['count'] ?> шт.<br/>
								<?php if ($reserve['count']): ?>
									В резерве: <?= $reserve['count'] ?> шт.
								<?php endif; ?>
							</p>
						<?php endif; ?>
          </div>

          <hr/>

          <?php if (count($items_color)): ?>
            <div class="colors" id="items-select-color">
              <p>Выберите цвет:</p>
              <?php foreach ($items_color as $c):
								if (isset($reserve['active']))
									$c['active'] = $reserve['active'];
								?>
                <a href="#image<?= $c["id_colors"] ?>" data-toggle="tab" title="<?= $c["name"] ?>" data-id="<?= $c["id_colors"] ?>"<?= !$c['active'] ? "class='not-active'" : "" ?>>
                  <i class="fa fa-check"></i>
                  <img src="/img/colors/<?= $c["image"] ?>"/>
                </a>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>


          <div class="box-quantity mt5">
            <form id="form-add-to-cart">
              <label>Кол-во</label>
              <input type="hidden" id="id_items" value="<?= $items["id_items"] ?>" />
              <input type="number" id="count" value="1" />
              <button type="submite">В корзину</button>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="pro-info-area ptb-80">
  <div class="container">

    <?php
    echo Yii::$app->view->renderFile('@app/views/items/_additionally.php', [
      "items" => $items,
      "items_question" => $items_question,
      "items_review" => $items_review,
      "items_modules" => $items_modules,
    ]);
    ?>

  </div>
</div>

<?php if (count($items_similar)): ?>
  <div class="product-area pb-100">
    <div class="container">
      <div class="section-title text-center">
        <h2>Похожие товары </h2>
        <!-- <p>Shop Laptop feature only the best laptop deals on the market.</p> -->
      </div>
      <div class="row">

        <div class="product-active owl-carousel">
          <?php
          echo Yii::$app->view->renderFile('@app/views/items/_items.php', [
            "data_table" => $items_similar,
          ]);
          ?>
        </div>

      </div>
    </div>
  <?php endif; ?>
</div>
