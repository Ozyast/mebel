<?php
/* Входные параметры ( Обязательные ) */
// data_table - Массив со всеми данными из таблицы items
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Helper;

$category_id = $this->context->actionParams['cat'];
$category_name = $this->context->page['category'][$category_id]['name_ru'];

$this->title = strlen($category_name) ? $category_name : "Мебель от производителей - Экспресс Мебель";
$this->context->description = 'Мебель от производителей';
$h1 = strlen($category_name) ? $category_name : "Мебель от производителей";

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>

<script src="/js/page/items.js"></script>

<div class="breadcrumb-banner-area ptb-20">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <?php if (strlen($category_name)): ?>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span><?= $category_name ?></span></li>
            </ul>
          <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if (isset($seoPage['text'])): ?>
  <div class="container pt-40">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <?= $seoPage['text'] ?>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="shop-area pt-20 pb-70">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <?php
        echo Yii::$app->view->renderFile('@app/views/widget/_filter.php', [
          "price_min" => $filter_min_max["min"],
          "price_max" => $filter_min_max["max"],
          "color" => $filter_color,
          "manufacturer" => $filter_manufacturer,
        ]);
        ?>
      </div>
      <div class="col-md-9">
        <div class="product-tab">
          <div class="shop-select-bar">
            <?php
            echo Yii::$app->view->renderFile('@app/views/widget/_sort_bar.php', [
            ]);
            ?>

            <div id="items_main">
              <?php
              echo Yii::$app->view->renderFile('@app/views/items/_index.php', [
                "data_table" => $data_table,
                "pages" => $pages,
              ]);
              ?>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
