<?php
use yii\helpers\Url;
?>

<?php foreach ($data_table as $row): ?>
  <div class="col-md-2 col-sm-4 column-2">
    <div class="product-wrapper mb-40">
      <div class="product-img">
        <a href="<?= Url::to(['items/view', 'id'=>$row['id_items']]) ?>">
          <?php
            $image = explode(",", $row['image']);
          ?>
          <img src="/img/items/<?= str_replace("/", "/@", $image[0]) ?>" alt="" />
          <?php if(isset($image[1])): ?>
            <img class="secondary-img" src="/img/items/<?= str_replace("/", "/@", $image[1]) ?>" alt="" />
          <?php endif; ?>
        </a>
        <div class="label-product">
        <?php if ($row["sale"]): ?>
            <span class="new">- <?= round( (1 - $row["sale_price"] / $row["price"]) * 100 ) ?>%</span>
        <?php else: ?>
          <span>New</span>
        <?php endif; ?>
        </div>
        <!-- <div class="product-action">
          <div class="action-inner">
            <a data-toggle="tooltip" title="Add to Cart" href="#"><i class="icon_cart_alt"></i></a>
            <a data-toggle="tooltip" title="Compare this Product" href="#"><i class="icon_tags_alt" aria-hidden="true"></i></a>
            <a data-toggle="tooltip" title="Add to Wish List" href="#"><i class="icon_star_alt" aria-hidden="true"></i></a>
          </div>
        </div> -->
      </div>
      <div class="product-content text-center">
        <h5>
          <a href="<?= Url::to(['items/view', 'id'=>$row['id_items']]) ?>">
            <?= $row["title"] ?>
          </a>
        </h5>
        <div class="price-box">
          <?php if ($row["sale"]): ?>
            <span class="new-price"> <?= $row["sale_price"] ?> р. </span>
            <span class="old-price"> <?= $row["price"] ?> </span>
          <?php else: ?>
            <span class="new-price"> <?= $row["price"] ?> р. </span>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
