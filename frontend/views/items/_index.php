<div class="row" id="items-list">
<?php
echo Yii::$app->view->renderFile('@app/views/items/_items.php', [
  "data_table" => $data_table,
]);
?>
</div>

<div class="row toolbar4">
  <?php
  echo Yii::$app->view->renderFile('@app/views/widget/_str.php', [
    "pages" => $pages,
  ]);
  ?>
</div>
