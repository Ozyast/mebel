<?php foreach ($modules as $module): ?>
  <li class="modules-item <?= $active ? "active" : "" ?>">
    <div class="modules-image">
      <img src="/img/collections/<?= $module["image"] ?>"/>
    </div>
    <div class="modules-description">
      <span class="modules-name"><?= $module["name"] ?></span><br/>
      <span class="modules-characteristics"><?= $module["characteristics"] ?></span><br/>
      <span class="modules-price"><?= $module["price"] ?> р.</span>
    </div>
  </li>
<?php endforeach; ?>
