<?php
use yii\helpers\Url;
?>

<?php if (isset($cart) && count($cart) && count($cart_item)): ?>
<div class="table-content table-responsive">
  <table>
    <thead>
      <tr>
        <th class="product-thumbnail">Фото</th>
        <th class="product-name">Наименование</th>
        <th class="product-price">Цена</th>
        <th class="product-quantity">Количество</th>
        <th class="product-subtotal">Стоимость</th>
        <th class="product-remove">Удалить</th>
      </tr>
    </thead>
    <tbody>

      <?php
        echo Yii::$app->view->renderFile('@app/views/cart/_index_cart_item.php', [
          "cart_item" => $cart_item,
        ]);
      ?>

    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="cart_totals">
      <h3>К оплате</h3>
      <br />
      <table>
        <tbody>
          <tr class="cart-subtotal">
            <th>Цена без скидки:</th>
            <td><span class="amount"><?= $cart["price"] ?></span></td>
          </tr>
          <tr class="cart-subtotal">
            <th>Скидка:</th>
            <td><span class="amount"><?= $cart["sale"] ?></span></td>
          </tr>
          <tr class="order-total">
            <th>Итого:</th>
            <td>
              <strong><span class="amount"><?= $cart["total"] ?></span></strong>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="wc-proceed-to-checkout">
        <a href="<?= Url::to(['cart/checkout']) ?>">Оформить заказ</a>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
  <p class="empty-alert mt4">В корзине пока пусто :-(</p>
<?php endif; ?>
