<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Оформление заказа - Экспресс Мебель';
$this->context->description = 'Оформление заказа';
$h1 = 'Оформление заказа';

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>

<script src="/js/page/checkout.js"></script>

<div class="breadcrumb-banner-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><a href="<?= Url::to(['cart/index']) ?>">Корзина</a></li>
              <li><span>Оформление</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="checkout-area pt-30 pb-30">
  <div class="container">
    <div class="row">
      <form id="form-checkout">
        <div class="col-lg-6 col-md-6">
          <div class="checkbox-form">
            <h3>Детали заказа</h3>
            <div class="row">
              <div class="col-md-12">
                <div class="checkout-form-list">
                  <label>Имя <span class="required">*</span></label>
                  <input type="text" id="user_name" name="user_name" />
                </div>
              </div>
              <div class="col-md-12">
                <div class="checkout-form-list">
                  <label>Телефон <span class="required">*</span></label>
                  <input type="text" id="user_phone" name="user_phone" placeholder="7 900 123 456 78" />
                </div>
              </div>
              <div class="col-md-12">
                <div class="checkout-form-list create-acc">
                  <input type="checkbox" id="delivery" name="delivery" />
                  <label for="delivery">Нужна доставка?</label>
                </div>
                <div id="delivery_address" class="checkout-form-list">
                  <p>Напишите, пожалуйста, полный адрес доставки, чтобы наша доставка согла Вас найти.</p>
                  <label>Адрес  <span class="required">*</span></label>
                  <input type="text" id="user_address" name="user_address" placeholder="ул. Красная д. 123 п. 2" />
                </div>
              </div>
              <div class="col-md-12">
                <div class="checkout-form-list">
                  <label>Комментарий к заказу</label>
                  <textarea id="comment" name="comment" rows="10" placeholder="Текст комментария" ></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="your-order">
            <h3>Ваш заказ</h3>
            <div class="your-order-table table-responsive">
              <table>
                <thead>
                  <tr>
                    <th class="product-name">Наименование</th>
                    <th class="product-total">Стоимость</th>
                  </tr>
                </thead>
                <tbody>

                  <?php if (isset($cart_item) && count($cart_item))
                    foreach ($cart_item as $key => $row):
                    $row = (array) $row; ?>
                    <tr class="cart_item">
                      <td class="product-name">
                        <?= $row["title"] ?>
                        <?php if ($row["color"]): ?>
                          (Цвет: <?= $row["color"] ?>)
                        <?php endif; ?>
                        <strong class="product-quantity"> × <?= $row["count"] ?> </strong>
                        <?php if (!$row['check_instock']): ?>
                          <span class="text-danger">Товара нет в наличии</span>
                        <?php endif; ?>
                      </td>
                      <td class="product-total">
                        <span class="amount"><?= $row["sale"] ? ($row["price"] - $row["sale"]) : $row["price"] ?> р.</span>
                      </td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>
                <tfoot>
                  <tr class="cart-subtotal">
                    <th>Цена без скидки:</th>
                    <td><span class="amount"><?= $cart["price"] ?> р.</span></td>
                  </tr>
                  <tr class="cart-subtotal">
                    <th>Скидка:</th>
                    <td><span class="amount"><?= $cart["sale"] ?> р.</span></td>
                  </tr>
                  <tr class="order-total">
                    <th>Итого:</th>
                    <td><strong><span class="amount"><?= $cart["total"] ?> р.</span></strong>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="payment-method">
              <div class="order-button-payment">
                <button type="submit"<?= !$check_instock ? "disabled" : "" ?>>Разместить заказ</button>
                <?php if (!$check_instock): ?>
                  <p class="text-danger text-center mt2">В вашей корзине есть товар, которого нет в наличии. Пожалуйста выберите другой товар или удалите его из корзины.</p>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
