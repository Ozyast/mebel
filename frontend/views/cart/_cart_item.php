<?php
use yii\helpers\Url;
?>

<?php if (count($cart_item)): ?>
  <?php foreach ($cart_item as $row):
    $row = (array) $row; ?>
    <li id="cart-item">
      <div class="cart-img">
        <a href="<?= Url::to(['items/view', 'id'=>$row["id_items"]]) ?>"><img src="/img/items/<?= $row["image"] ?>" alt="" /></a>
      </div>
      <div class="cart-title">
        <a href="<?= Url::to(['items/view', 'id'=>$row["id_items"]]) ?>"><h4><?= $row["title"] ?></h4></a>
        <span class="quantity"> × <?= $row["count"] ?></span>
        <span class="cart-price"><?= $row["price"] ?> р.</span>
      </div>
    </li>
  <?php endforeach; ?>
<?php endif; ?>
