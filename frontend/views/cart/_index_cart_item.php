<?php
use yii\helpers\Url;
?>

<?php if (count($cart_item)): ?>
  <?php foreach ($cart_item as $key => $row):
    $row = (array) $row;
    $price = $row["price"] - $row["sale"]; ?>

    <tr id="<?= $key ?>">
      <td class="product-thumbnail">
        <a href="/img/items/<?= str_replace("/@", "/", $row["image"]) ?>" class="popup-link">
          <img src="/img/items/<?= $row["image"] ?>" alt="" />
        </a>
      </td>

      <td class="product-name"><a href="<?= Url::to(['items/view', 'id'=>$row["id_items"]]) ?>"><?= $row["title"] ?></a></td>
      <td class="product-price"><span class="amount"><?= ( $price/$row["count"] ) ?> р.</span></td>
      <td class="product-quantity">
        <input type="number" name="cart-item-count" value="<?= $row["count"] ?>" min="1" max="100" />
      </td>
      <td class="product-subtotal"><?= $price ?> р.</td>

      <td class="product-remove">
        <a href="javascript:void(0);" id="cart-item-delete">
          <i class="fa fa-times"></i>
        </a>
      </td>
    </tr>

  <?php endforeach; ?>
<?php endif; ?>
