<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Ваша корзина - Экспресс Мебель';
$this->context->description = 'Ваша корзина';
$h1 = 'Корзина';

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}

if (!isset($cart) && app\models\Site::hasCookies("cart")) {
  $cart = (array) json_decode(app\models\Site::getCookies("cart"));
  $cart_item = (array) json_decode(app\models\Site::getCookies("cartItem"));
} else {
  $cart = [];
  $cart_item = [];
}
?>

<script src="/js/page/cart.js"></script>

<div class="breadcrumb-banner-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span>Корзина</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="cart-main-area pb-50">
  <div class="container">
    <div class="col-md-12">
      <?php if (isset($order)): ?>
      <div class="isotope-filter faq-filter text-center mt6">
        <button data-filter="#cart">Корзина <?= count($cart_item) ? "(".count($cart_item).")" : "" ?></button>
        <button data-filter="#order">Мои заказы <?= count($order) ? "(".count($order).")" : "" ?></button>
      </div>
      <?php endif; ?>
    </div>

    <div class="col-md-12">
      <div class="isotope-grid">

        <div class="isotope-item faq-isotope-item" id="cart">
          <?php
          echo Yii::$app->view->renderFile('@app/views/cart/_index_cart.php', [
            "cart" => $cart,
            "cart_item" => $cart_item,
          ]);
          ?>
        </div>

        <?php if (isset($order)): ?>
        <div class="isotope-item faq-isotope-item" id="order">
          <div class="section-title text-center">
            <div class="list-group">
              <?php foreach ($order as $key => $row): ?>
                <a href="<?= Url::to(["cart/order", "id" => $row["hash"]]) ?>" class="list-group-item">
                  Заказ №<?= $key + 1 ?> от <i><?= date("d.m.Y", strtotime($row["created_at"])) ?></i> на сумму <b><?= $row["price_total"] ?></b>
                </a>
              <?php endforeach ?>
            </div>
          </div>
        </div>
        <?php endif; ?>

      </div>
    </div>
  </div>
</div>
