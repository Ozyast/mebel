<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Заказа №".$order['id_cart']." от ". date("d.m.Y", strtotime($order["created_at"]))." - Экспресс Мебель";
$this->context->description = 'Данные заказа';
$h1 = "Заказа №$order[id_cart] от ".date("d.m.Y", strtotime($order[created_at]));

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
// Показать проезд на карте с помощью этого -
// https://developers.google.com/maps/documentation/javascript/shapes
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4xUfijrTzcH9Sf0HbMR8O6Rvi4YbwX8"></script>
<script src="/js/page/order.js"></script>

<div class="breadcrumb-banner-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><a href="<?= Url::to(['cart/index']) ?>">Корзина</a></li>
              <li><span>Мои заказы</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="checkout-area pt-30 pb-30">
  <div class="container">
    <div class="row">

        <div class="col-lg-6 col-md-6">
          <div class="your-order">
            <h3>Информация по заказу</h3>

            <dl class="dl-horizontal">

              <dt>Контакты для связи: </dt>
              <dd><?= $order["user_phone"] ?> (<?= $order["user_name"] ?>)</dd>

              <dt>Доставка: </dt>
              <dd><?= $order["delivery"] ? "Да" : "Нет" ?></dd>

              <?php if ($order["delivery"]): ?>
                <dt>Адрес доставки: </dt>
                <dd><?= $order["user_address"] ?></dd>
              <?php endif; ?>

              <?php if (strlen($order["comment"])): ?>
                <dt>Комментарий к заказу: </dt>
                <dd><?= $order["comment"] ?></dd>
              <?php endif; ?>

              <br/>

              <dt>Статус заказа: </dt>
              <dd><?= $order["status"] ?></dd>

              <dt>Заказ оплачен: </dt>
              <dd><?= $order["paid-up"] ? "Да" : "Нет" ?></dd>

              <dt>Заказ создан: </dt>
              <dd><?= date("d.m.Y в H:i", strtotime($order["created_at"])) ?></dd>

              <?php if (strtotime($order["updated_at"]) > 0): ?>
                <dt>Последние изменения: </dt>
                <dd><?= date("d.m.Y в H:i", strtotime($order["updated_at"])) ?></dd>
              <?php endif; ?>

              <dt>Заказ завершен: </dt>
              <dd><?= $order["done"] ? "Да" : "Нет" ?></dd>
            </dl>
          </div>

          <div class="your-order mt3">
            <h3>Ваш заказ</h3>

            <div class="your-order-table table-responsive">
              <table>
                <thead>
                  <tr>
                    <th class="product-name">Наименование</th>
                    <th class="product-total">Стоимость</th>
                  </tr>
                </thead>
                <tbody>

                  <?php if (isset($order_item) && count($order_item))
                    foreach ($order_item as $key => $row):
                    $row = (array) $row; ?>
                    <tr class="cart_item">
                      <td class="product-name">
                        <?= $row["title"] ?>
                        <?php if ($row["color"]): ?>
                          (Цвет: <?= $row["color"] ?>)
                        <?php endif; ?>
                        <strong class="product-quantity"> × <?= $row["count"] ?> </strong>
                      </td>
                      <td class="product-total">
                        <span class="amount"><?= $row["price_sale"] ? ($row["price"] - $row["price_sale"]) : $row["price"] ?> р.</span>
                      </td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>
                <tfoot>
                  <tr class="cart-subtotal">
                    <th>Цена без скидки:</th>
                    <td><span class="amount"><?= $order["price"] ?> р.</span></td>
                  </tr>
                  <tr class="cart-subtotal">
                    <th>Скидка:</th>
                    <td><span class="amount"><?= $order["sale"] ?> р.</span></td>
                  </tr>
                  <tr class="order-total">
                    <th>Итого:</th>
                    <td><strong><span class="amount"><?= $order["price_total"] ?> р.</span></strong>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="payment-method">
              <?php if (!$order["paid-up"]): ?>
                <!-- <div class="order-button-payment">
                  <button type="submit">Оплатить заказ</button>
                </div> -->
              <?php endif; ?>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-md-6">
          <div class="checkbox-form">
            <h3>Как забрать заказ</h3>
            <div class="row">

              <div class="map-area mb-80">
        				<div class="container-fulid">
        					<div id="map"></div>
        				</div>
        			</div>

              <p class="text-right">
                <b>Телефон:</b> +7(918)98-24-000 <br/>
                <b>Email:</b> gorizont-krd@mail.ru  <br/><br/>
                <b>Адрес:</b> г. Краснодар, Ейское шоссе, <br/>
                п. Березовый 89/1 (промзона Форпост)
              </p>
            </div>
          </div>
        </div>

    </div>
  </div>
</div>
