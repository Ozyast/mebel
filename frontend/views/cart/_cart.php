<?php
use yii\helpers\Url;
?>

<?php if (count($cart_item)): ?>
  <ul>
    <?php
    echo Yii::$app->view->renderFile('@app/views/cart/_cart_item.php', [
      "cart_item" => $cart_item,
    ]);
    ?>
    <li>
      <div class="subtotal">
        <div class="subtotal-titles">
          <h3>Цена без скидки :</h3>
          <span><?= $cart["price"] ?></span>
        </div>
        <div class="subtotal-titles">
          <h3>Скидка :</h3>
          <span><?= $cart["sale"] ?></span>
        </div>
        <div class="subtotal-titles">
          <h3>Итого :</h3>
          <span><?= $cart["total"] ?></span>
        </div>
      </div>
    </li>
    <li>
      <div class="cart-btns">
        <span class="default-btn">
          <a href="<?= Url::to(['cart/index']) ?>">Открыть</a>
        </span>
        <span class="default-btn">
          <a href="<?= Url::to(['cart/checkout']) ?>">Оформить</a>
        </span>
      </div>
    </li>
  </ul>
<?php endif; ?>
