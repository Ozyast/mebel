<form id="reviews-form">
  <input type="hidden" id="id_items" value="<?= $items["id_items"] ?>">

  <div class="row">
    <div class="col-md-6">
      <div class="leave-form">
        <input type="text" name="user_name" id="user_name" placeholder="Имя *">
      </div>
    </div>
    <div class="col-md-6">
      <div class="leave-form">
        <input type="email" name="user_email" id="user_email" placeholder="E-mail *">
      </div>
    </div>
    <div class="col-md-12">
      <div class="text-leave">
        <textarea name="text" id="text" placeholder="Отзыв *"></textarea>
        <button type="submit" class="submit">Отправить отзыв</button>
      </div>
    </div>
  </div>
</form>
