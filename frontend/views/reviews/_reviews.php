<?php if (isset($items_review) && count($items_review)): ?>
  <?php foreach ($items_review as $review): ?>
    <li class="media">
      <div class="media-body">
        <h4 class="media-heading"><?= $review["user_name"] ?></h4>
        <p><?= nl2br($review["text"]) ?></p>
      </div>
    </li>
  <?php endforeach; ?>
<?php else: ?>
    <p class="text-center text-muted">К сожалению, еще нет ни одного отзыва на товар.</p>
<?php endif; ?>
