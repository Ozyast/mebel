<?php
  // Переберем вопросы и найдем на них ответы, если они есть
  $question = [];
  if (isset($items_question) && count($items_question)) {
    foreach ($items_question as $row) {
      if ($row['id_answer']) {
        $question[$row['id_answer']]["answer"][] = $row;
      } else {
        $question[$row['id_items_question']] = $row;
      }
    }
  }
?>

<?php if (count($question)): ?>
  <?php foreach ($question as $row): ?>
    <li class="media">
      <div class="media-left"><i class="fa fa-question fa-3x"></i></div>
      <div class="media-body">
        <h4 class="media-heading"><?= $row["user_name"] ?></h4>
        <p><?= nl2br($row["text"]) ?></p>

        <?php if (isset($row["answer"]) && count($row["answer"])): ?>
            <?php foreach ($row["answer"] as $row2): ?>
              <div class="media">
                <div class="media-left"><i class="fa fa-ellipsis-h"></i></div>
                <div class="media-body">
                  <h4 class="media-heading"><?= $row2["user_name"] ?></h4>
                  <p><?= nl2br($row2["text"]) ?></p>
                </div>
              </div>
            <?php endforeach; ?>
        <?php endif; ?>

      </div>
    </li>
  <?php endforeach; ?>
<?php else: ?>
    <p class="text-center text-muted">К сожалению, еще нет ни одного вопроса о товаре.</p>
<?php endif; ?>
