<?php
use yii\helpers\Url;

$this->title = 'Наши контакты - Экспресс Мебель';
$this->context->description = 'Наши контакты';
$h1 = 'Наши контакты';

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4xUfijrTzcH9Sf0HbMR8O6Rvi4YbwX8"></script>
<script src="/js/page/contact.js"></script>

<!-- breadcrumb-banner-area -->
<div class="breadcrumb-banner-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span>Контакты</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- breadcrumb-banner-area-end -->
<!-- contact-area-start -->
<div class="contact-area pt-100 pb-70">
  <div class="container-fulid">
    <div class="row">
      <div class="col-md-7">
        <div class="contact-wrapper mb-30">
          <div class="contact-form-area">
            <form id="contact-form">
              <input type="text" name="name" id="name" placeholder="Ваше имя">
              <input type="email" name="email" id="email" placeholder="Ваш email">
              <textarea name="message" name="text" id="text" cols="30" rows="10" placeholder="Текст сообщения" id="message"></textarea>
              <button type="submit" class="submit">Отправить</button>
            </form>
          </div>
          <p class="form-message"></p>
        </div>
      </div>

      <div class="col-lg-5 col-md-5 col-sm-12">
        <div class="communication mb-30">
          <div class="single-communication">
            <div class="communication-icon">
              <i aria-hidden="true" class="fa fa-home"></i>
            </div>
            <div class="communication-text">
              <h3>Адрес:</h3>
              <p>г. Краснодар, Ейское шоссе, <br/>
              п. Березовый 89/1 (промзона Форпост)</p>
            </div>
          </div>
          <div class="single-communication">
            <div class="communication-icon">
              <i aria-hidden="true" class="fa fa-phone"></i>
            </div>
            <div class="communication-text">
              <h3>Телефон:</h3>
              <p> +7 (918) 667-45-00 </p>
            </div>
          </div>
          <div class="single-communication">
            <div class="communication-icon">
              <i aria-hidden="true" class="fa fa-envelope-o"></i>
            </div>
            <div class="communication-text">
              <h3>Email:</h3>
              <p><a href="#">support@express-mebel.ru</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- contact-area-end -->
<!-- map-area -->
<div class="map-area mb-100">
  <div class="container-fulid">
    <div id="map"></div>
  </div>
</div>
<!-- map-end -->
