<?php
use yii\helpers\Url;

$this->title = 'Главная - Экспресс Мебель';
$this->context->description = 'Экспресс Мебель';
$h1 = "Главная";

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>

<?php
if (isset($slider))
  echo Yii::$app->view->renderFile('@app/views/widget/_slider.php', [
    "slider" => $slider,
  ]);
?>

<div class="static-policy-area pt-80 pb-100">
  <div class="container">
    <div class="static-border">
      <div class="row">
        <div class="section-title text-center visible-xs">
          <h3>Почему мы</h3>
        </div>

        <div class="col-md-4">
          <div class="static-policy-wrapper mb-30">
            <div class="static-img">
              <i class="fa fa-rocket"></i>
            </div>
            <div class="static-content">
              <h4>Бесплатная быстрая доставка</h4>
              <p>Быстро и бесплатно доставим ваш заказ к вашему дому</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="static-policy-wrapper mb-30">
            <div class="static-img">
              <i class="fa fa-industry"></i>
            </div>
            <div class="static-content">
              <h4>Работаем напрямую</h4>
              <p>Мы работаем напрямую с фабриками производителями</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="static-policy-wrapper mb-30">
            <div class="static-img">
              <i class="fa fa-ruble"></i>
            </div>
            <div class="static-content">
              <h4>Без предоплаты</h4>
              <p>Заказ до 10 000 р. оплачивайте при получении! Все варианты оплаты</p>
            </div>
          </div>
        </div>
      </div>
    </div>

      <?php if (isset($seoPage['text'])): ?>
        <div class="col-xs-10 col-xs-offset-1 pt-40">
          <div class="static-border">
          <h1 class="text-center"><?= $h1 ?></h1>
          <p class="pt-20">
            <?= $seoPage['text'] ?>
          </p>
        </div>
        </div>
      <?php endif; ?>
  </div>
</div>

<div class="product-area pb-100">
  <div class="container">
    <div class="section-title text-center">
      <h4>Наши новинки</h4>
      <p>Мы стараемся как можно чаще расширять свой ассортимент новыми и интересными моделями мебели.<br/>
        Мы стараемся создать максимальный выбор для покупателя.</p>
    </div>
    <div class="row">
      <div class="trendy-product-active indicator owl-carousel">

        <?php
        if (isset($items_new))
          echo Yii::$app->view->renderFile('@app/views/items/_items.php', [
            "data_table" => $items_new,
          ]);
        ?>

      </div>
    </div>
  </div>
</div>
