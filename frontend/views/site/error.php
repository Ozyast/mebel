<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->context->description = $name;
?>

 <!-- error404-area-start -->
<div class="error404-area pt-110 pb-140">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="error-wrapper text-center">
        <div class="error-text">
          <h1><?= Html::encode($exception->statusCode) ?></h1>
          <h2><?= Html::encode($this->title) ?></h2>
          <p><?= nl2br(Html::encode($message)) ?></p>
        </div>
        <div class="search-error">
        <!-- <form id="search-form" action="#">
          <input type="text" placeholder="Search">
          <button><i class="fa fa-search"></i></button>
        </form> -->
        </div>
        <div class="error-button">
          <a href="<?= Url::to(["/"]) ?>">Вернуться на главную</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- error404-area-start -->
