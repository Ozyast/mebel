<?php
use yii\helpers\Url;

$this->title = 'Доставка и сборка - Экспресс Мебель';
$this->context->description = 'Доставка и сборка';
$h1 = "Доставка и сборка";

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>

<!-- breadcrumb-banner-area -->
<div class="breadcrumb-banner-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span>Доставка</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- breadcrumb-banner-area-end -->
<!-- about-section-area-start -->
<div class="about-section-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-12">
        <div class="about-wrapper">

            <h4>Доставка и сборка товара</h4>

            <p>
              <b>Доставка по Москве до подъезда</b> : БЕСПЛАТНО<br/>
              <b>Доставка от Мкад по Московской области и Владимирской области.</b><br/>
              от 1 км до 60 км +30 руб за 1 км<br/>
              от 60 км до 150 км +40 руб за 1 км<br/>
              <b>Доставка осуществляется по следующим дням : вторник, четверг, суббота</b><br/>
            </p>

        </div>
      </div>
    </div>

    <div class="row mt-30">
      <div class="col-md-6 col-sm-12">
        <div class="mute-area">
          <h3>Подъем товара</h3>

          <table  class="table table-striped">
            <tbody>
              <tr>
                <td width="60%">
                  <b>Наименование</b>
                </td>
                <td width="10%">
                  <b>За этаж</b>
                </td>
                <td width="10%">
                  <b>Лифт</b>
                </td>
                <td width="10%">
                  <b>1 этаж</b>
                </td>
                <td width="10%">
                  <b>Сборка</b>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  <b>МЯГКАЯ МЕБЕЛЬ</b>
                </td>
                <td width="10%">
                  <br>
                </td>
                <td width="10%">
                  <br>
                </td>
                <td width="10%">
                  <br>
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Диваны
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  300
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Диваны сп.м более 140
                </td>
                <td width="10%">
                  350
                </td>
                <td width="10%">
                  700
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  300
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Детские,кухонные угловые диваны
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  500
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Угловые диваны
                </td>
                <td width="10%">
                  350
                </td>
                <td width="10%">
                  900
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  500
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Диваны
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  300
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Комплекты ММ
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  900
                </td>
                <td width="10%">
                  900
                </td>
                <td width="10%">
                  800
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Кресло
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  200
                </td>
              </tr>
              <tr>
                <td width="60%">
                  <b>КОРПУСНАЯ МЕБЕЛЬ </b>
                </td>
                <td width="10%">
                  <br>
                </td>
                <td width="10%">
                  <br>
                </td>
                <td width="10%">
                  <br>
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Стенки до 3м в т.ч. детские,серванты
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Стенки более 3м в т.ч. детские,серванты
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  900
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Прихожие
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Спальни
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  1000
                </td>
                <td width="10%">
                  450
                </td>
                <td width="10%">
                  500
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Комоды,тумбы,столики в разобранном виде
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Комоды,тумбы,столики в собранном виде
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Книжные шкафы,стеллажи
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Компьютерные столы
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Кухни до 5 упаковок
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Кухни до 10 упаковок
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Кухни более 10 упаковок
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Кровати
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  массив 400
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Матрасы
                </td>
                <td width="10%">
                  100
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  100
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Кухонные уголки
                </td>
                <td width="10%">
                  200
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Шкафы распашные,угловые
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Шкафы -купе до 1,50
                </td>
                <td width="10%">
                  250
                </td>
                <td width="10%">
                  600
                </td>
                <td width="10%">
                  300
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Шкафы-купе от 1,50 до 2,50
                </td>
                <td width="10%">
                  350
                </td>
                <td width="10%">
                  700
                </td>
                <td width="10%">
                  400
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
              <tr>
                <td width="60%">
                  Шкафы-купе более 2,50
                </td>
                <td width="10%">
                  450
                </td>
                <td width="10%">
                  900
                </td>
                <td width="10%">
                  500
                </td>
                <td width="10%">
                  <br>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-md-6 col-sm-12">
        <div class="mute-area">
          <h3>Сборка товара</h3>

          <table class="table table-striped">
            <tbody>
              <tr>
                <td width="10%">
                  <b>№п/п</b>
                </td>
                <td width="70%">
                  <b>Сборка 10%, но не менее:</b>
                </td>
                <td width="20%" align="center">
                  <b>Цена</b>
                </td>
              </tr>
              <tr>
                <td>
                  1
                </td>
                <td>
                  комоды, кровати,комп.столы,тумбы ТВ
                </td>
                <td align="center">
                  1000руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  2
                </td>
                <td colspan="1">
                  кровати с подъёмным механизмом,комоды-трюмо
                </td>
                <td colspan="1" align="center">
                  1000руб
                </td>
              </tr>
              <tr>
                <td>
                  3
                </td>
                <td>
                  шкафы,прихожие(до 1,5м.)
                </td>
                <td align="center">
                  1000руб
                </td>
              </tr>
              <tr>
                <td>
                  4
                </td>
                <td>
                  прихожие более 1,5м.
                </td>
                <td align="center">
                  1200руб
                </td>
              </tr>
              <tr>
                <td>
                  5
                </td>
                <td>
                  стенки до 3м.(кроме Сильвы,Эльвиры,Фортуны,ст Купе,Наталья Мирослава,Кардинал),детские
                </td>
                <td align="center">
                  1500руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  6
                </td>
                <td colspan="1">
                  стенки более 3-х метров(в том числе Сильва,Эльвира,Фортуна, Купе,Наталья,Мирослава,Кардинал)
                </td>
                <td align="center">
                  2000руб
                </td>
              </tr>
              <tr>
                <td>
                  7
                </td>
                <td>
                  спальни
                </td>
                <td align="center">
                  2000руб
                </td>
              </tr>
              <tr>
                <td>
                  8
                </td>
                <td>
                  стенки,детские угловые более 4-х метров
                </td>
                <td align="center">
                  2500руб
                </td>
              </tr>
              <tr>
                <td>
                  9
                </td>
                <td>
                  мягкая мебель,диваны
                </td>
                <td align="center">
                  1000руб
                </td>
              </tr>
              <tr>
                <td>
                  <br>
                </td>
                <td>
                  <b>Выезд сборщика за МКАД:</b>
                </td>
                <td>
                  <br>
                </td>
              </tr>
              <tr>
                <td>
                  10
                </td>
                <td>
                  до 10км
                </td>
                <td align="center">
                  200руб
                </td>
              </tr>
              <tr>
                <td>
                  11
                </td>
                <td>
                  свыше 10км
                </td>
                <td align="center">
                  цена договорная <br>
                </td>
              </tr>
              <tr>
                <td>
                  <br>
                </td>
                <td>
                  <b>Дополнительные работы</b>
                </td>
                <td>
                  <br>
                </td>
              </tr>
              <tr>
                <td>
                  1
                </td>
                <td>
                  межсекционная стяжка(секция)
                </td>
                <td align="center">
                  100руб
                </td>
              </tr>
              <tr>
                <td>
                  2
                </td>
                <td>
                  подключение подсветки 1 точка(без расходных материалов)
                </td>
                <td align="center">
                  150руб
                </td>
              </tr>
              <tr>
                <td>
                  3
                </td>
                <td>
                  вырез под розетку в оргалите
                </td>
                <td align="center">
                  100руб
                </td>
              </tr>
              <tr>
                <td>
                  4
                </td>
                <td>
                  врезка замка в дверь шкафа,установка механизма и ответных планок(без цены замка)
                </td>
                <td align="center">
                  350руб
                </td>
              </tr>
              <tr>
                <td>
                  5
                </td>
                <td>
                  Повес изделия на стенку за единицу
                </td>
                <td align="center">
                  200руб
                </td>
              </tr>
              <tr>
                <td>
                  6
                </td>
                <td>
                  сборка в ограниченных условиях, на балконах,сборка в нише, на уголках, стоя при нарушении технологий)
                </td>
                <td align="center">
                  увелечение на 30 % от стоимости работ
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  7
                </td>
                <td colspan="1">
                  сверление под светильник( 1 точка)
                </td>
                <td colspan="1" align="center">
                  200руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  8
                </td>
                <td colspan="1">
                  вклейка зеркала
                </td>
                <td colspan="1" align="center">
                  100руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  9
                </td>
                <td colspan="1">
                  вызов мастера на рекламацию&nbsp;
                </td>
                <td colspan="1" align="center">
                  300руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  10
                </td>
                <td colspan="1">
                  занос мебели по этажам,зданию
                </td>
                <td colspan="1" align="center">
                  по ценам грузчиков
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  11
                </td>
                <td colspan="1">
                  поклейка шлегеля мастера
                </td>
                <td colspan="1" align="center">
                  300-500руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  12
                </td>
                <td colspan="1">
                  замер(шкафы-купе, встроеные шкафы)
                </td>
                <td colspan="1" align="center">
                  1000руб
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  13
                </td>
                <td colspan="1">
                  сборка встроеных шкафов-купе
                </td>
                <td colspan="1" align="center">
                  15-20% от стоимости шкафа
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </div>

  </div>
</div>
<!-- about-section-area-end -->
