<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $articles['menu_title'];
$this->context->description = $articles['menu_title'];
$h1 = $articles['menu_title'];

$seoPage = @app\models\Site::getSeoPage();
if (!empty($seoPage)) {
  $this->title = $seoPage['title'];
  if (strlen($seoPage['description']))
    $this->context->description = $seoPage['description'];
  if (strlen($seoPage['h1']))
    $h1 = $seoPage['h1'];
}
?>

<div class="breadcrumb-banner-area ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-text text-center">
          <div class="breadcrumb-menu">
            <h1 class="page-title"><?=  $h1 ?></h1>
            <ul>
              <li><a href="<?= Url::to(['/']) ?>">Главная</a></li>
              <li><span>Статья</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="blog-details-area ptb-100">
  <div class="container-fulid">
    <div class="row">
      <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="blog-details-wrapper">
          <?php
            echo Yii::$app->view->renderFile('@app/views/articles/'.$articles['file_name']);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
