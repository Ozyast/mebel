<?php
use yii\helpers\Url;
use app\models\Items;

$so_array = Items::getOrderSortBar();
$so = isset($_GET['so']) && $_GET['so'] ? $_GET['so'] : 0;

$ps_array = Items::getCountSortBar();
$ps = isset($_GET['ps']) && $_GET['ps'] ? $_GET['ps'] : 20;
?>

<div class="shop-select-bar">
  <div class="selector-field hidden-xs">
    <form id="sort-items">
      <label>Сортировка</label>
      <select name="select">
        <?php foreach ($so_array as $key => $row): ?>
          <option value="<?= $key ?>" <?= $key == $so ? 'selected':'' ?>><?= $row['name'] ?></option>
        <?php endforeach; ?>
      </select>
    </form>
  </div>
  <div class="selector-field">
    <form id="count-items">
      <label>Показывать : </label>
      <select name="select">
        <?php foreach ($ps_array as $value): ?>
          <option <?= $ps == $value ? 'selected':'' ?>><?= $value ?></option>
        <?php endforeach; ?>
      </select>
    </form>
  </div>
</div>
