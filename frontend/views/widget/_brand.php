<div class="our-brand-area">
  <div class="container">
    <div class="brand-border">
      <div class="row">
        <div class="brand-active owl-carousel">
          
          <?php foreach ($manufacturer as $row): ?>
            <div class="col-md-12">
              <div class="single-brand">
                <div class="brand-img">
                  <img src="/img/manufacturer/<?= $row["image"] ?>" alt="" />
                </div>
              </div>
            </div>
          <?php endforeach; ?>

        </div>
      </div>
    </div>
  </div>
</div>
