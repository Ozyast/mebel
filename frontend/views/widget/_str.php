<?php
use yii\helpers\Url;
use app\models\Helper;

/**
  * Настройки
  */
// Сколько страниц выводить
$pages['count_str'] = 4;
// текст кнопки назад и вперед
$pages['back_text'] = "&lt;";
$pages['next_text'] = "&gt;";

$pages = Helper:: pagesView($pages);
// Array
// (
//     [length] => 6
//     [str] => 1
//     [count_data] => 6
//     [controller] => items/index
//     [home_url] => /frontend/web/items/index
//     [get] => &ass=12&gg=34
//     [page] => Array
//         (
//             [0] => Array
//                 (
//                     [page] => 1
//                     [active] => 1
//                     [text] => 1
//                 )
//         )
//
// )
?>

<div class="col-sm-6">
  <ul class="pagination">
    <?php if (isset($pages['page']))
          foreach ($pages['page'] as $row): ?>

      <?php if (!$row['page']): ?>
        <li><span><?= $row['text'] ?></span></li>
      <?php elseif ($row['active']): ?>
        <li class="active"><span><?= $row['text'] ?></span></li>
      <?php elseif($row['page'] == 1): ?>
        <li><a href="<?= Url::to([$pages['controller']]).preg_replace("/&/", "?", $pages['get'], 1) ?>"><?= $row['text'] ?></a></li>
      <?php else: ?>
        <li><a href="<?= Url::to([$pages['controller'], "page"=>$row['page']]).$pages['get'] ?>"><?= $row['text'] ?></a></li>
      <?php endif; ?>

    <?php endforeach; ?>
  </ul>
</div>
