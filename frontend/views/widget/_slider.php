<div class="slider-area">
  <div class="sliders-active owl-carousel">
    <?php foreach ($slider as $row): ?>
      <img src="/img/slider/<?= $row['image'] ?>" alt="">
    <?php endforeach; ?>
  </div>
</div>
