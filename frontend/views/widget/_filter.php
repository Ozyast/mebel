<?php
use yii\helpers\Url;

$active_color = isset($_GET["cl"]) ? explode(",", $_GET["cl"]) : [];
$active_manufacturer = isset($_GET["ma"]) ? explode(",", $_GET["ma"]) : [];
?>

<div class="panel-group" id="filter">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#filter" href="#filterAll"> <i class="fa fa-angle-up"></i> Фильтр</a>
      </h4>
    </div>

    <div id="filterAll" class="panel-collapse collapse in">
      <div class="panel-body">

        <div class="">
          <div class="widget-shop">
            <span>По цене</span>
            <div class="price-filter">
              <div id="slider-range"></div>
              <div class="price-space">
                <div class="price-input">
                  <input type="text" id="amount" data-min="<?= isset($price_min) ? $price_min : "0"  ?>" data-max="<?= isset($price_max) ? $price_max : "0"  ?>" data-value-min="<?= isset($_GET["pf"]) ? $_GET["pf"] : 1000 ?>" data-value-max="<?= isset($_GET["pt"]) ? $_GET["pt"] : $price_max ?>">
                  <button type="submit" class="price-button" id="slider-range-price">Готово</button>
                </div>
              </div>
            </div>
          </div>
          <?php if (isset($manufacturer) && count($manufacturer)): ?>
            <div class="widget-shop">
              <span>По производителю</span>
              <div class="shop-list" id="filter-manufacturer">
                <?php foreach ($manufacturer as $row): ?>
                  <a href="javascript:void(0)" id="<?= $row["id_manufacturer"] ?>" <?= in_array($row["id_manufacturer"], $active_manufacturer) ? 'class="active"' : "" ?>>
                    <i class="fa fa-square-o"></i><?= $row["name"] ?>
                  </a>
                <?php endforeach; ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if (isset($color) && count($color)): ?>
            <div class="widget-shop">
              <span>По цвету</span>
              <div class="shop-list colors" id="filter-color">
                <?php foreach ($color as $row): ?>
                  <a href="javascript:void(0)" <?= in_array($row["id_colors"], $active_color) ? 'class="active"' : "" ?> data-toggle="tooltip" title="<?= $row["name"] ?>" id="<?= $row["id_colors"] ?>">
                    <i class="fa fa-check"></i>
                    <img src="/img/colors/<?= $row["image"] ?>"/>
                  </a>
                <?php endforeach; ?>
              </div>
            </div>
          <?php endif; ?>
        </div>

      </div>
    </div>

  </div>
</div>

<?php return; ?>
