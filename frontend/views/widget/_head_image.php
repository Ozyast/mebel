<?php
use yii\helpers\Url;
?>

<div class="slider-area">
  <div class="slider-wrapper-1" style="background-image:url(<?= $image ?>)">
    <div class="slider-wrapper-white"></div>

    <div class="container">
      <div class="slider-info-1">
        <h3><?= $title ?></h3>
        <?php if (isset($breadcrumbs) && is_array($breadcrumbs)): ?>
          <ul class="breadcrumb1-area">
            <?php foreach ($breadcrumbs as $key => $row): ?>
              <?php if (is_array($row)): ?>
                <li><a href="<?= Url::to($row) ?>"><?= $key ?></a></li>
              <?php else: ?>
                <li><?= $row ?></li>
              <?php endif ?>
            <?php endforeach; ?>
          </ul>
        <?php endif ?>
      </div>
    </div>
  </div>
</div>
