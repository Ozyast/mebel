
// Фильтр по цене
$('body').on("click", "#slider-range-price", function () {
  var price = $( "#slider-range" ).slider( "values" );
  var pricefrom = price[0];
  var priceto = price[1];

  var url = urlGet({"pf":pricefrom, "pt":priceto, "page":""});

  post(url, {}, function (answer) {
      var data = JSON.parse(answer);
      answerJson(answer, {
          success: function () {
            history.pushState(null, null, url);

            $("#items_main").html(data["html"]);
          }
      });
  });
})

// Фильтр по цвету
$('body').on("click", "#filter-manufacturer a", function () {
  $(this).toggleClass("active");
  var id_manufacturer = [];
  $("#filter-manufacturer a.active").each(function (i, el) {
    id_manufacturer.push(parseInt($(el).attr("id")));
  });

  var url = urlGet({"ma":id_manufacturer.join(','), "page":""});

  post(url, {}, function (answer) {
      var data = JSON.parse(answer);
      answerJson(answer, {
          success: function () {
            history.pushState(null, null, url);

            $("#items_main").html(data["html"]);
          }
      });
  });
})

// Сортировка
$('body').on("change", "#sort-items select", function () {
  var sort = parseInt($(this).val()) | 0;

  if(sort){
    var url = urlGet({"so":sort, "page":""});
  } else {
    var url = urlGet({"so":"", "page":""});
  }


  post(url, {}, function (answer) {
      var data = JSON.parse(answer);
      answerJson(answer, {
          success: function () {
            history.pushState(null, null, url);

            $("#items_main").html(data["html"]);
          }
      });
  });
})


// Кол-во
$('body').on("change", "#count-items select", function () {
  var count = parseInt($(this).val()) | 0;

  var url = urlGet({"ps":count, "page":""});

  post(url, {}, function (answer) {
      var data = JSON.parse(answer);
      answerJson(answer, {
          success: function () {
            history.pushState(null, null, url);

            $("#items_main").html(data["html"]);
          }
      });
  });
})

// Фильтр по цвету
$('body').on("click", "#filter-color a", function () {
  $(this).toggleClass("active");
  var id_color = [];
  $("#filter-color a.active").each(function (i, el) {
    id_color.push(parseInt($(el).attr("id")));
  });

  var url = urlGet({"cl":id_color.join(','), "page":""});

  post(url, {}, function (answer) {
      answerJson(answer, {
          success: function () {
            var data = JSON.parse(answer);
            history.pushState(null, null, url);

            $("#items_main").html(data["html"]);
          }
      });
  });
})
