
// Удаление товара из корзины
$('body').on("click", "#cart-item-delete", function () {
  var id = $(this).parents("tr").attr("id");

  post("/cart/delete", {id:id}, function (answer) {
      answerJson(answer, {
          success: function () {
            var data = JSON.parse(answer);
            $("#cart").html(data.html);

            $(".cart-item").html(data.html_menu);
            $("#cart-total").text($(".cart-item li#cart-item").length);
          }
      });
  });
})

// Обновление кол-во товара в корзине
$('body').on("change", "input[name=cart-item-count]", function () {
  var id = $(this).parents("tr").attr("id");
  var count = $(this).val();

  post("/cart/update", {id:id, count:count}, function (answer) {
      answerJson(answer, {
          success: function () {
            var data = JSON.parse(answer);
            $("#cart").html(data.html);

            $(".cart-item").html(data.html_menu);
            $("#cart-total").text($(".cart-item li#cart-item").length);
          }
      });
  });
})

$(document).ready(function () {
  $(".isotope-grid").isotope({ filter: '#cart' });
})
