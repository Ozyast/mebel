// Обновление кол-во товара в корзине
$('body').on("submit", "#form-checkout", function () {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));

  if (data.delivery && !data.user_address.length){
    $.hulla.send("Пожалуйста введите адрес доставки", "danger");
    return;
  }

  post("/cart/done", data, function (answer) {
      var data = JSON.parse(answer);
      answerJson(answer, {
          success: function () {
            location.replace("/cart/order?id="+data.id);
            return;
          }
      });
  });
})

/* Отображение поля ввода адреса */
$(document).ready(function () {
  $( '#delivery' ).on('click', function() {
    $( '#delivery_address' ).slideToggle(900);
  });
})
