
// Выбор цвета
$('body').on("click", "#items-select-color a:not(.not-active)", function () {
  $("#items-select-color a").removeClass("active");
  $(this).addClass("active");
})

// Открыть форму добавления вопроса и отзыва
$('body').on("click", "#reviews-open-form, #question-open-form", function () {
  $(".question-form, .reviews-form", $($(this).parents(".pro-desc"))).show("slow");
  $(this).hide();
})

// Добавление товара в корзину
$('body').on("submit", "#form-add-to-cart", function () {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  data.color = $("#items-select-color a.active").attr("data-id");

  if (typeof data.color === "undefined" && $("#items-select-color a").length) {
    $.hulla.send("Пожалуйста выберите цвет", "danger");
    return;
  }

  post("/cart/add", data, function (answer) {
      var data = JSON.parse(answer);
      answerJson(answer, {
          success: function () {
            $(".cart-item").html(data.html);
            $("#cart-total").text($(".cart-item li#cart-item").length);

            showCart();
            $("#scrollUp").click();
          }
      });
  });
})


$(document).ready(function () {
  // ОТЗЫВЫ
  $('body').on("submit", "#reviews-form", function (event) {
    var self = $(this);
    var data = FormRead($(this), event, $("button[type=submit]", $(this)));

    post("/reviews/create", data, function (answer) {
        var data = JSON.parse(answer);
        answerJson(answer, {
            success: function () {
              $("#text", $(self)).val("");


              if ($("#reviews ul li").length)
                $(data.html).appendTo($("#reviews ul"));
              else
                $("#reviews ul").html(data.html);
            }
        });
    });
  })
  // ВОПРОСЫ
  $('body').on("submit", "#question-form", function (event) {
    var self = $(this);
    var data = FormRead($(this), event, $("button[type=submit]", $(this)));

    post("/question/create", data, function (answer) {
        var data = JSON.parse(answer);
        answerJson(answer, {
            success: function () {
              $("#text", $(self)).val("");
              if ($("#question ul li").length)
                $(data.html).appendTo($("#question ul"));
              else
                $("#question ul").html(data.html);
            }
        });
    });
  })

})
