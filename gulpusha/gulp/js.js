/**
 * Работа с JS - файлами
 */

var gulp = require('gulp');
var gulpIf = require('gulp-if');
// Сжатие JS
var uglify = require('gulp-uglifyes');
// Конкатенация файлов
var concat = require('gulp-concat');
//var sourcemaps = require('gulp-sourcemaps');

module.exports = function(options){

    return function(callback) {
        return gulp.src(options.js_folder_source)
            .pipe(uglify())
            .pipe(gulpIf(options.concat, concat(options.js_filename_out)))
            .pipe(gulp.dest(options.js_folder_out))
    }
}
