var gulp = require('gulp');
var gulpIf = require('gulp-if');
// Конкатенация файлов
var concat = require('gulp-concat');
// Сжатие CSS
var csso = require('gulp-csso');
//var rename = require('gulp-rename');
//var cssmin = require('gulp-cssmin'); // Сжимает хуже чем csso и не перезаписывает 
                                       //файл а добавляет туда новые строки
var sourcemaps = require('gulp-sourcemaps');

module.exports = function(options){
    return function(callback){
        return gulp.src(options.css_folder_source)
            .pipe(gulpIf(!options.isProduction, sourcemaps.init()))
            .pipe(csso({
                debug: true
            }))
            .pipe(gulpIf(!options.isProduction, sourcemaps.write('.')))
            .pipe(concat(options.css_filename_out))
            .pipe(gulp.dest(options.css_folder_out));
    }
}