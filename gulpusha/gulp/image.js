/**
 * Оптимизация изображений
 */
var gulp = require('gulp');
var gulpIf = require('gulp-if');
// Сжатие изображений
var tinify = require('gulp-tinify');    // Только в продакшн. Лимит на использование.
var smushit = require('gulp-smushit');	//  Только во время разработки. Плохо сжимает.

//gulp.task('tinify', function () {
//    return gulp.src(img_folder_source)
//        .pipe(tinify('3ziaoP7Q7d6LIdgUF7ysxweT0BmHiHCf'))
//        .pipe(gulp.dest('image'));
//});
//
//gulp.task('smushit', function () {
//    return gulp.src(img_folder_source)
//        .pipe(smushit())
//        .pipe(gulp.dest(img_folder_out));
//});

module.exports = function(options){
    return function(callback){
        return gulp.src(options.img_folder_source)
            .pipe(gulpIf(options.isProduction, tinify('3ziaoP7Q7d6LIdgUF7ysxweT0BmHiHCf'), smushit()))
            .pipe(gulp.dest(options.img_folder_out));
    }
}
