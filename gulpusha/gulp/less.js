/**
 * Препроцессор LESS
 */
var gulp = require('gulp');
//var gulpIf = require('gulp-if');

//var sourcemaps = require('gulp-sourcemaps');
var less = require('gulp-less');
var path = require('path');

module.exports = function(options){
    return function(callback){
        return gulp.src(options.less_folder_source)
        .pipe(less({
          paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest(options.less_folder_out));
    }
}