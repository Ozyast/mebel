var gulp = require('gulp');

// Пути для CSS
var css_folder_source = "../frontend/web/source/css/**/*.css"; // Источник
var css_folder_out = "../frontend/web/css";           // Назначение
var css_filename_out = "all.min.css"; // Имя файла
// Пути для LESS
// var less_folder_source = "source/less/style.less"; // Источник
// var less_folder_out = "css/less";           // Назначение
//const less_filename_out = "less.css"; // Имя файла
// Пути для JS
var js_folder_source = "../frontend/web/source/js/*.js"; // Источник
var js_folder_out = "../frontend/web/js";           // Назначение
var js_filename_out = "main.min.js"; // Имя файла
// Пути для JS2
var js_folder_source2 = "../frontend/web/source/js/page/**/*.js"; // Источник
var js_folder_out2 = "../frontend/web/js/page";           // Назначение
var js_filename_out2 = "all.min.js"; // Имя файла
// Пути для IMG
// var img_folder_source = "img/**/*"; // Источник
// var img_folder_out = "image";           // Назначение

// Проверяем какую сборку собирать, продакшн или девелопмент
var isProduction = !process.env.NODE_ENV || process.env.NODE_ENV === 'production';

gulp.task('default', function(done){
    console.log("build");
    done();
});

function lazyRequireTask(taskName, path, options){
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, function(callback){
        var task = require(path).call(this, options);

        return task(callback);
    })
}

// Препроцессор LESS
// lazyRequireTask('less', './gulp/less.js', {
//     less_folder_source: less_folder_source,
//     less_folder_out: less_folder_out,
// })

// Сжатие CSS
lazyRequireTask('csso', './gulp/css.js', {
    css_folder_source: css_folder_source,
    css_filename_out: css_filename_out,
    css_folder_out: css_folder_out,
    isProduction: isProduction,
})

// Сжатие JS
lazyRequireTask('uglify', './gulp/js.js', {
    js_folder_source: js_folder_source,
    js_filename_out: js_filename_out,
    js_folder_out: js_folder_out,
    concat: 1,
})

// Сжатие JS
lazyRequireTask('uglify2', './gulp/js.js', {
    js_folder_source: js_folder_source2,
    js_filename_out: js_filename_out2,
    js_folder_out: js_folder_out2,
    concat: 0,
})

// Оптимизация изображений
// lazyRequireTask('imageOptimizer', './gulp/image.js', {
//     img_folder_source: img_folder_source,
//     img_folder_out: img_folder_out,
//     isProduction: isProduction,
// })


gulp.task("build", gulp.series('csso', 'uglify', 'uglify2'));
