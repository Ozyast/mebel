<?php

use yii\db\Migration;

class m180831_161925_trigger_date_update_items_image extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
      $sql = "CREATE TRIGGER `items_image_date_update` BEFORE UPDATE ON `items_image` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
      $this->execute($sql);
    }

    public function down()
    {
      $sql = "DROP TRIGGER IF EXISTS `items_image_date_update`;";
      $this->execute($sql);
    }
}
