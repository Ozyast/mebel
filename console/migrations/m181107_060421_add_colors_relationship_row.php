<?php

use yii\db\Migration;

class m181107_060421_add_colors_relationship_row extends Migration
{
  public function safeUp()
  {
    // Есть ли в наличии
    $this->addColumn('items_colors_relationship', 'instock', $this->integer()->defaultValue(0));
  }

  public function safeDown()
  {
    $this->dropColumn('items_colors_relationship', 'instock');
  }
}
