<?php

use yii\db\Migration;

class m180830_092836_create_subcategory_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subcategory', [
          'id_subcategory' => $this->primaryKey(),
          // Имя продукта (Шкафы)
          'name_ru' => $this->string(50)->notNull(),
          // Имя продукта в транслите (Shkaf)
          'name_en' => $this->string(50)->notNull(),
          // Доступные цвета (["1","2"])
          'category' => $this->string(100)->notNull(),
          // Изображение интерьера, для отображения на странице в шапке
          'image' => $this->string(50)->notNull(),
          // Флаг, Показывать ли продукт
          'visible' => $this->boolean()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subcategory');
    }
}
