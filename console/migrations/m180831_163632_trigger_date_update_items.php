<?php

use yii\db\Migration;

class m180831_163632_trigger_date_update_items extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `items_date_update` BEFORE UPDATE ON `items` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `items_date_update`;";
    $this->execute($sql);
  }
}
