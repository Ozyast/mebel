<?php

use yii\db\Migration;

class m181129_104907_trigger_date_update_articles extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `articles_date_update` BEFORE UPDATE ON `articles` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `articles_date_update`;";
    $this->execute($sql);
  }
}
