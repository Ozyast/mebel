<?php

use yii\db\Migration;

class m181109_110511_edit_items_row_count extends Migration
{
  public function safeUp()
  {
    $this->renameColumn('items', 'services', 'instock_color');
    $this->alterColumn('items', 'instock_color', $this->boolean()->defaultValue(0));
  }

  public function safeDown()
  {
    $this->renameColumn('items', 'instock_color', 'services');
    $this->alterColumn('items', 'services', $this->string(250));
  }
}
