<?php

use yii\db\Migration;

class m180907_083201_trigger_date_update_items_review extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `items_review_date_update` BEFORE UPDATE ON `items_review` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `items_review_date_update`;";
    $this->execute($sql);
  }
}
