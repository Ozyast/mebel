<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items_description`.
 */
class m180907_054804_create_items_description_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('items_description', [
            'id_items_description' => $this->primaryKey(),
            // Описание через JSON ({"width":"2000"})
            'description' => $this->text(),
            // ID мебели которуму принадлежит описание
            'id_items' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('items_description');
    }
}
