<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m180914_054112_create_slider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('slider', [
          'id_slider' => $this->primaryKey(),
          // Ссылка на изображение
          'image' => $this->string(100)->notNull(),
          // Заголовок
          'title' => $this->string(100)->notNull(),
          // Подзаголовок, расположен выше заголовка
          'sub_title' => $this->string(100)->notNull(),
          // Текст
          'text' => $this->text(),
          // Ссылка, надпись на кнопке
          'button_name' => $this->string(100)->notNull(),
          // Ссылка, урл кнопки
          'button_url' => $this->string(100)->notNull(),
          // Отображать
          'visible' => $this->boolean()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('slider');
    }
}
