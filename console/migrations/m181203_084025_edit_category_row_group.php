<?php

use yii\db\Migration;

class m181203_084025_edit_category_row_group extends Migration
{
  public function safeUp()
  {
    // Есть ли в наличии
    $this->addColumn('category', 'group', $this->integer()->defaultValue(0));
  }

  public function safeDown()
  {
    $this->dropColumn('category', 'group');
  }
}
