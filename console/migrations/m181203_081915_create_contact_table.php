<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m181203_081915_create_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contact', [
          'id_contact' => $this->primaryKey(),
          // Имя пользователя
          'name' => $this->string(250)->notNull(),
          // Email пользователя
          'email' => $this->string(100)->notNull(),
          // Текст сообщения
          'text' => $this->text(),
          // IP Пользователя
          'ip' => $this->string(15),
          // ID основного сообщения если это сообщение является ответом
          'reply' => $this->integer()->defaultValue(0),
          // Прочитанно
          'read' => $this->boolean()->defaultValue(0),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contact');
    }
}
