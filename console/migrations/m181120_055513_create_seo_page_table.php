<?php

use yii\db\Migration;

/**
 * Handles the creation of table `seo_page`.
 */
class m181120_055513_create_seo_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('seo_page', [
          'id_page' => $this->primaryKey(),
          // page url
          'url' => $this->string(250)->notNull(),
          // Заголовок
          'title' => $this->string(220)->notNull(),
          // Описание
          'description' => $this->string(220)->notNull(),
          // Флаг, Показывать ли
          'visible' => $this->boolean()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('seo_page');
    }
}
