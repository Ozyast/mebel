<?php

use yii\db\Migration;

class m180910_112138_trigger_date_update_cart_item extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `cart_item_date_update` BEFORE UPDATE ON `cart_item` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `cart_item_date_update`;";
    $this->execute($sql);
  }
}
