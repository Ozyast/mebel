<?php

use yii\db\Migration;

class m181207_112421_edit_category_row_image_icon extends Migration
{
  public function safeUp()
  {
    // изображение
    $this->addColumn('category', 'image', $this->string(100));
    // Флаг обозначет что вместо изображения используется иконка
    $this->addColumn('category', 'icon', $this->boolean()->defaultValue(0));
  }

  public function safeDown()
  {
    $this->dropColumn('category', 'image, icon');
  }
}
