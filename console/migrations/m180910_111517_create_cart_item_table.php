<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cart_item`.
 */
class m180910_111517_create_cart_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cart_item', [
          'id_cart_item' => $this->primaryKey(),
          // ID мебели
          'id_items' => $this->integer()->notNull(),
          // Хэш продукта, по которому будет идентифицировать пользователя
          'id_cart' => $this->integer()->notNull(),
          // Заголовок продукта
          'title' => $this->string(250)->notNull(),
          // Главное фото продукта (1.jpg)
          'image' => $this->string(100)->notNull(),
          // Доступные цвета
          'color' => $this->integer()->notNull(),
          // Кол-во
          'count' => $this->integer()->notNull(),
          // Цена продукта (20000)
          'price' => $this->integer()->notNull()->defaultValue(0),
          // Цена продукта оп скидки
          'price_sale' => $this->integer()->notNull()->defaultValue(0),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
          // Дата обновления
          'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cart_item');
    }
}
