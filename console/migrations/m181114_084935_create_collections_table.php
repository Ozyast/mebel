<?php

use yii\db\Migration;

/**
 * Handles the creation of table `collections`.
 */
class m181114_084935_create_collections_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('collections', [
          'id_collections' => $this->primaryKey(),
          // имя коллекции
          'name' => $this->string(100)->notNull(),
          // Флаг, Показывать ли
          'visible' => $this->boolean()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('collections');
    }
}
