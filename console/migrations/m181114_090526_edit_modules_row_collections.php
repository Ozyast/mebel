<?php

use yii\db\Migration;

class m181114_090526_edit_modules_row_collections extends Migration
{
  public function safeUp()
  {
    $this->renameColumn('modules', 'id_items', 'id_collections');
  }

  public function safeDown()
  {
    $this->renameColumn('modules', 'id_collections', 'id_items');
  }
}
