<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items_modules_relationship`.
 */
class m180915_074914_create_items_modules_relationship_table extends Migration
{
    /**
      * Настройки для миграции
      */
    // имя промежуточной таблицы
    const table_relationship =  "items_modules_relationship";
    // имя промежуточной таблицы в сокращении для составления имени индекса и ключа
    const table_relationship_min =  "items_modules";

    /**
      * Первое поле
      */
    // Имя Другой таблицы
    const table_oposite_1 =  "items";
    // Имя поля в другой таблице и имя поля к которому применяем индекс
    const table_oposite_row_name_1 =  "id_items";

    /**
      * Второе поле
      */
    // Имя другой таблицы
    const table_oposite_2 =  "modules";
    // Имя поля в другой таблице и имя поля к которому применяем индекс
    const table_oposite_row_name_2 =  "id_modules";

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::table_relationship, [
          'id' => $this->primaryKey(),
          // ID
          self::table_oposite_row_name_1 => $this->integer()->notNull()->defaultValue(0),
          // ID
          self::table_oposite_row_name_2 => $this->integer()->notNull()->defaultValue(0),
          // отображать
          'visible' => $this->boolean()->notNull()->defaultValue(1),
        ]);

        // creates index for column `id_items`
        $this->createIndex(
            // имя индекса
            'idx-'.self::table_relationship_min.'-'.self::table_oposite_row_name_1,
            // имя промежуточной таблицы
            self::table_relationship,
            // Имя поля к которому применяем индекс
            self::table_oposite_row_name_1
        );

        // add foreign key for table `items`
        $this->addForeignKey(
            // Имя внешнего ключа
            'fk-'.self::table_relationship_min.'-'.self::table_oposite_row_name_1,
            // Имя таблицы
            self::table_relationship,
            // Имя поля в этой таблице
            self::table_oposite_row_name_1,
            // Имя Другой таблицы
            self::table_oposite_1,
            // Имя поля в другой таблице
            self::table_oposite_row_name_1,
            'CASCADE'
        );

        // creates index for column `id_colors`
        $this->createIndex(
            // имя индекса
            'idx-'.self::table_relationship_min.'-'.self::table_oposite_row_name_2,
            // имя таблицы
            self::table_relationship,
            // Имя поля к которому применяем индекс
            self::table_oposite_row_name_2
        );

        // add foreign key for table `colors`
        $this->addForeignKey(
            // Имя внешнего ключа
            'fk-'.self::table_relationship_min.'-'.self::table_oposite_row_name_2,
            // Имя таблицы
            self::table_relationship,
            // Имя поля в этой таблице
            self::table_oposite_row_name_2,
            // Имя Другой таблицы
            self::table_oposite_2,
            // Имя поля в другой таблице
            self::table_oposite_row_name_2,
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      // drops foreign key for table `items_colors_relationship`
      $this->dropForeignKey(
          // Имя внешнего ключа
          'fk-'.self::table_relationship_min.'-'.self::table_oposite_row_name_1,
          // Имя таблицы
          self::table_relationship
      );

      // drops index for column `post_id`
      $this->dropIndex(
          // Имя индекса
          'idx-'.self::table_relationship_min.'-'.self::table_oposite_row_name_1,
          // Имя таблицы
          self::table_relationship
      );

      // drops foreign key for table `tag`
      $this->dropForeignKey(
          // Имя внешнего ключа
          'fk-'.self::table_relationship_min.'-'.self::table_oposite_row_name_2,
          // Имя таблицы
          self::table_relationship
      );

      // drops index for column `tag_id`
      $this->dropIndex(
          // Имя индекса
          'idx-'.self::table_relationship_min.'-'.self::table_oposite_row_name_2,
          // Имя таблицы
          self::table_relationship
      );

      $this->dropTable(self::table_relationship);
    }
}
