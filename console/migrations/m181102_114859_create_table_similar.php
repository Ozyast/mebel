<?php

use yii\db\Migration;

class m181102_114859_create_table_similar extends Migration
{
    public function up()
    {
      $this->createTable('similar', [
        'id_similar' => $this->primaryKey(),
        // ID
        'id_items_master' => $this->integer()->notNull()->defaultValue(0),
        // ID
        'id_items_slave' => $this->integer()->notNull()->defaultValue(0),
      ]);


      // creates index for column `id_items_master`
      $this->createIndex(
          // имя индекса
          'idx-id_items_master-id_items',
          // имя промежуточной таблицы
          'similar',
          // Имя поля к которому применяем индекс
          'id_items_master'
      );

      // add foreign key for table `id_items_master`
      $this->addForeignKey(
          // Имя внешнего ключа
          'fk-id_items_master-id_items',
          // Имя таблицы
          'similar',
          // Имя поля в этой таблице
          'id_items_master',
          // Имя Другой таблицы
          'items',
          // Имя поля в другой таблице
          'id_items',
          'CASCADE'
      );

      // creates index for column `id_items_slave`
      $this->createIndex(
          // имя индекса
          'idx-id_items_slave-id_items',
          // имя промежуточной таблицы
          'similar',
          // Имя поля к которому применяем индекс
          'id_items_slave'
      );

      // add foreign key for table `id_items_slave`
      $this->addForeignKey(
          // Имя внешнего ключа
          'fk-id_items_slave-id_items',
          // Имя таблицы
          'similar',
          // Имя поля в этой таблице
          'id_items_slave',
          // Имя Другой таблицы
          'items',
          // Имя поля в другой таблице
          'id_items',
          'CASCADE'
      );
    }

    public function down()
    {
      // drops foreign key for table `id_items_master`
      $this->dropForeignKey(
          // Имя внешнего ключа
          'fk-id_items_master-id_items',
          // Имя таблицы
          'similar'
      );

      // drops index for column `id_items_master`
      $this->dropIndex(
          // Имя индекса
          'idx-id_items_master-id_items',
          // Имя таблицы
          'similar'
      );

      // drops foreign key for table `id_items_slave`
      $this->dropForeignKey(
          // Имя внешнего ключа
          'fk-id_items_slave-id_items',
          // Имя таблицы
          'similar'
      );

      // drops index for column `id_items_slave`
      $this->dropIndex(
          // Имя индекса
          'idx-id_items_slave-id_items',
          // Имя таблицы
          'similar'
      );

      $this->dropTable('similar');
    }
}
