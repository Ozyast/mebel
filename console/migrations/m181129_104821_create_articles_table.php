<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m181129_104821_create_articles_table extends Migration
{
  /**
  * @inheritdoc
  */
  public function up()
  {
    $this->createTable('articles', [
      'id_articles' => $this->primaryKey(),
      // Имя подменю
      'menu_title' => $this->string(250)->notNull(),
      // Ссылка в подменю
      'menu_url' => $this->string(100)->notNull(),
      // Имя файла с кодом страницы
      'file_name' => $this->string(100)->notNull(),
      // Дата создания
      'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
      // Дата обновления
      'updated_at' => $this->timestamp(),
      // Отображать
      'visible' => $this->boolean()->notNull()->defaultValue(0),
    ]);
  }

  /**
  * @inheritdoc
  */
  public function down()
  {
    $this->dropTable('articles');
  }
}
