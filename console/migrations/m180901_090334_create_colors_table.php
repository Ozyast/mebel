<?php

use yii\db\Migration;

/**
 * Handles the creation of table `colors`.
 */
class m180901_090334_create_colors_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('colors', [
          'id_colors' => $this->primaryKey(),
          // Ссылка на изображение
          'image' => $this->string(100)->notNull(),
          // Название цвета
          'name' => $this->string(100)->notNull(),
          // Если цвет со вставками, указываем ID основного цвета
          'main_color' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('colors');
    }
}
