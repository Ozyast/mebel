<?php

use yii\db\Migration;

class m180831_163717_trigger_date_update_modules extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `modules_date_update` BEFORE UPDATE ON `modules` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `modules_date_update`;";
    $this->execute($sql);
  }
}
