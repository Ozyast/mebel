<?php

use yii\db\Migration;

class m180910_111000_trigger_date_update_cart extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `cart_date_update` BEFORE UPDATE ON `cart` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `cart_date_update`;";
    $this->execute($sql);
  }
}
