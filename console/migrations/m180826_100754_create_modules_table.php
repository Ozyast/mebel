<?php

use yii\db\Migration;

/**
 * Handles the creation of table `modules`.
 */
class m180826_100754_create_modules_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('modules', [
          'id_modules' => $this->primaryKey(),
          // Заголовок коллекции
          'name' => $this->string(100)->notNull(),
          // Характеристики
          'characteristics' => $this->string(250)->notNull(),
          // Фото коллекции
          'image' => $this->string(100)->notNull(),
          // Цена коллекции (20000)
          'price' => $this->integer()->notNull()->defaultValue(0),
          // ID мебели
          'id_items' => $this->integer()->notNull(),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
          // Дата обновления
          'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('modules');
    }
}
