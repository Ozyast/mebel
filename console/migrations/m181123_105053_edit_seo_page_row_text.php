<?php

use yii\db\Migration;

class m181123_105053_edit_seo_page_row_text extends Migration
{
  public function safeUp()
  {
    // Есть ли в наличии
    $this->addColumn('seo_page', 'text', $this->text());
    // Заголовок
    $this->addColumn('seo_page', 'h1', $this->string(220));
  }

  public function safeDown()
  {
    $this->dropColumn('seo_page', 'text, h1');
  }
}
