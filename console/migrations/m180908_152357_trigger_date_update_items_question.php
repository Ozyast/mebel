<?php

use yii\db\Migration;

class m180908_152357_trigger_date_update_items_question extends Migration
{
  public function up()
  {
    $sql = "CREATE TRIGGER `items_question_date_update` BEFORE UPDATE ON `items_question` FOR EACH ROW BEGIN SET NEW.updated_at = now(); END";
    $this->execute($sql);
  }

  public function down()
  {
    $sql = "DROP TRIGGER IF EXISTS `items_question_date_update`;";
    $this->execute($sql);
  }
}
