<?php

use yii\db\Migration;

/**
 * Handles the creation of table `manufacturer`.
 */
class m180914_045624_create_manufacturer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('manufacturer', [
          'id_manufacturer' => $this->primaryKey(),
          // Ссылка на логотип
          'image' => $this->string(100)->notNull(),
          // Название цвета
          'name' => $this->string(100)->notNull(),
          // Отображать
          'visible' => $this->boolean()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('manufacturer');
    }
}
