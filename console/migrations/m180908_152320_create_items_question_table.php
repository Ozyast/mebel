<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items_question`.
 */
class m180908_152320_create_items_question_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('items_question', [
          'id_items_question' => $this->primaryKey(),
          // Имя пользователя
          'user_name' => $this->string(100)->notNull(),
          // Почта пользователя
          'user_email' => $this->string(100)->notNull(),
          // IP пользователя
          'user_ip' => $this->string(17)->notNull(),
          // Текст отзыва
          'text' => $this->text(),
          // ID мебели
          'id_items' => $this->integer()->notNull()->defaultValue(0),
          // ID вопроса, если это ответ.
          'id_answer' => $this->integer()->notNull()->defaultValue(0),
          // Флаг, ответили ли на письмо, для поиска новых вопросов
          'answer_check' => $this->boolean()->notNull()->defaultValue(0),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
          // Дата обновления
          'updated_at' => $this->timestamp(),
          // Флаг, Показывать или нет
          'visible' => $this->boolean()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('items_question');
    }
}
