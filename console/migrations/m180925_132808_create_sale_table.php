<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale`.
 */
class m180925_132808_create_sale_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
      $this->createTable('sale', [
        'id_sale' => $this->primaryKey(),
        // Ссылка на изображение
        'image' => $this->string(100)->notNull(),
        // Заголовок
        'title' => $this->string(100)->notNull(),
        // Подзаголовок, расположен выше заголовка
        'sub_title' => $this->string(100)->notNull(),
        // Текст
        'text' => $this->text(),
        // Ссылка, надпись на главной кнопке (primary)
        'button_p_name' => $this->string(100)->notNull(),
        // Ссылка, урл главной кнопки (primary)
        'button_p_url' => $this->string(100)->notNull(),
        // Ссылка, надпись на второй кнопке (slave)
        'button_s_name' => $this->string(100)->notNull(),
        // Ссылка, урл второй кнопки (slave)
        'button_s_url' => $this->string(100)->notNull(),
        // Отображать
        'visible' => $this->boolean()->notNull()->defaultValue(0),
      ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sale');
    }
}
