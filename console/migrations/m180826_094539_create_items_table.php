<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items`.
 */
class m180826_094539_create_items_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('items', [
          'id_items' => $this->primaryKey(),
          // Заголовок продукта
          'title' => $this->string(250)->notNull(),
          // Главное фото продукта (1.jpg)
          'image' => $this->string(100)->notNull(),
          // Доступные цвета (["1","2"])
          // 'id_colors' => $this->integer()->notNull(),
          // Цена продукта (20000)
          'id_manufacturer' => $this->integer()->notNull()->defaultValue(0),
          // Цена продукта (20000)
          'price' => $this->integer()->notNull()->defaultValue(0),
          // Скидка на продукт (1)
          'sale' => $this->boolean()->notNull()->defaultValue(0),
          // Цена продукта по скидки (19000)
          'sale_price' => $this->integer()->notNull()->defaultValue(0),
          // К какой коллекции относится данный товар (1)
          // 'id_collections' => $this->integer()->notNull()->defaultValue(0),
          // ID модуля из которого собрана коллекция (["1","2","3","4"])
          // 'id_modules' => $this->string(250)->notNull(),
          // Дополнения доступные для товара (["1","2","3","4"])
          'additions' => $this->string(250)->notNull(),
          // Услуги доступные для товара (["1","2","3","4"])
          'services' => $this->string(250)->notNull(),
          // Категория продукта (["1","2","3","4"])
          // 'category' => $this->string(250)->notNull(),
          // Интерьер (["1","2","3","4"])
          // 'interior' => $this->string(250)->notNull(),
          // Есть ли в наличии
          'instock' => $this->boolean()->notNull()->defaultValue(0),
          // Всего проданно
          'total_sold' => $this->integer()->notNull()->defaultValue(0),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
          // Дата обновления
          'updated_at' => $this->timestamp(),
          // Отображать в каталоге (елси это дополнение то можно его не показывать)
          'visible_catalog' => $this->boolean()->notNull()->defaultValue(0),
          // Отображать
          'visible' => $this->boolean()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('items');
    }
}
