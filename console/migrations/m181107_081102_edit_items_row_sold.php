<?php

use yii\db\Migration;

class m181107_081102_edit_items_row_sold extends Migration
{
  public function safeUp()
  {
    $this->renameColumn('items', 'total_sold', 'sold');
    $this->alterColumn('items', 'sold', $this->boolean()->defaultValue(0));
    $this->alterColumn('items', 'instock', $this->integer()->defaultValue(0));
  }

  public function safeDown()
  {
    $this->renameColumn('items', 'sold', 'total_sold');
    $this->alterColumn('items', 'total_sold', $this->integer()->defaultValue(0));
    $this->alterColumn('items', 'instock', $this->boolean()->defaultValue(0));
  }
}
