<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cart`.
 */
class m180910_110120_create_cart_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cart', [
          'id_cart' => $this->primaryKey(),
          // Хэш продукта, по которому будет идентифицировать пользователя
          'hash' => $this->string(250)->notNull(),
          // Статус заказа
          'status' => $this->string(100)->notNull(),
          // Цена продукта (20000)
          'price' => $this->integer()->notNull()->defaultValue(0),
          // Скидка
          'sale' => $this->integer()->notNull()->defaultValue(0),
          // Скидка
          'price_total' => $this->integer()->notNull()->defaultValue(0),
          // Имя пользователя
          'user_name' => $this->string(100)->notNull(),
          // Телефон пользователя
          'user_phone' => $this->string(20)->notNull(),
          // Нужна доставка
          'delivery' => $this->boolean()->notNull()->defaultValue(0),
          // Адрес доставки
          'user_address' => $this->string(250)->notNull(),
          // Коментарий к заказу
          'comment' => $this->text(),
          // Товар оплачен
          'paid-up' => $this->boolean()->notNull()->defaultValue(0),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
          // Дата обновления
          'updated_at' => $this->timestamp(),
          // Товар выдан
          'done' => $this->boolean()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cart');
    }
}
