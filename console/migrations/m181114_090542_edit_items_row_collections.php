<?php

use yii\db\Migration;

class m181114_090542_edit_items_row_collections extends Migration
{
  public function safeUp()
  {
    $this->renameColumn('items', 'additions', 'id_collections');
    $this->alterColumn('items', 'id_collections', $this->integer()->defaultValue(0));
  }

  public function safeDown()
  {
    $this->renameColumn('items', 'id_collections', 'additions');
    $this->alterColumn('items', 'additions', $this->string(250));
  }
}
