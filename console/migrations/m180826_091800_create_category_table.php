<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_list`.
 */
class m180826_091800_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
          'id_category' => $this->primaryKey(),
          // Имя продукта (Шкафы)
          'name_ru' => $this->string(50)->notNull(),
          // Имя продукта в транслите (Shkaf)
          'name_en' => $this->string(50)->notNull(),
          // Флаг, Показывать ли продукт
          'collection' => $this->boolean()->notNull()->defaultValue(0),
          // Флаг, Показывать ли продукт
          'visible' => $this->boolean()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_list');
    }
}
