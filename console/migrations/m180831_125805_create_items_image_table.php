<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items_image`.
 */
class m180831_125805_create_items_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('items_image', [
          'id_items_image' => $this->primaryKey(),
          // Ссылка на изображение
          'src' => $this->string(100)->notNull(),
          // ID мебели к которуму принадлежит фото
          'id_items' => $this->integer()->notNull()->defaultValue(0),
          // ID Цвет мебели
          'color' => $this->integer()->notNull()->defaultValue(0),
          // Дата создания
          'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
          // Дата обновления
          'updated_at' => $this->timestamp(),
          // Флаг, Показывать ли фото
          'visible' => $this->boolean()->notNull()->defaultValue(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('items_image');
    }
}
