<?php
return [
    'language' => 'ru',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=wm79265_mebel',
            'username' => 'wm79265_mebel',
            'password' => 'kMwz85Vg',
            'charset' => 'utf8',
        ],
        // Отпарвка почты
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // 'useFileTransport' => false,
            'viewPath' => '@common/mail',
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
        ],
    ],
];
