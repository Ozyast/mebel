<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=mebel',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        // Отпарвка почты
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // 'useFileTransport' => false,
            'viewPath' => '@common/mail',
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'ozy@nxt.ru',
                'password' => 'Ot3mfpow',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
    ],
];
