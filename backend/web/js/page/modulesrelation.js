var model = "modulesrelation";

/*
 * FURNITURES
 */

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});

// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");

  var data = {};
  if($("input[type=checkbox]", $(this)).prop('checked')) {
    data.visible = 1;
    data.instock = 1;
  } else {
    data.visible = 0;
    data.instock = 0;
  }

  TableEditeSave(id, data, model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});

// EDIT: open modal
$('body').on("click", "#data-edit", function(){
  var id = $(this).parents("tr").attr("data-tr-id");
  TableEditForm(id, model, null, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})
// EDIT: save modal update
$("body").on("submit", "#modal-table[data-action=edit] form", function(event){
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableEditeSave(data.id, data, model);
})

// ADD: open modal
$('body').on("click", "#data-add", function() {
  var id_items = $("#main_table").attr("data-main-id");

  TableAddForm(model, {id_items:id_items}, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})
// ADD: save add modal
$("body").on("submit", "#modal-table[data-action=add] form", function(event) {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableAddSave(data, model);
})

// SORT
// $("body").on("click", "#main_table th:not(.not-sort) i", function(){
//     var row = $(this).parents("th").attr("id");
//     var sort = $(this).attr("id");
//
//     TableSort(model, row, sort);
// })

/**
* ДОБВАЛЕНИЕ МОДУЛЕЙ с Лерома
*/
$('body').on("click", "#data-add-url", function (event) {
  var link = prompt('Введите ссылку', '');
  var row = [];
  var id_items = $("#main_table").attr("data-main-id");
  var self = $(this);

  if (!link.length) {
    return;
  }
  look_load($(self), "");

  // Прокси =)
  link = "http://srv82925.ht-test.ru/index.php?url="+link;

  post("/admin/modules/getpage", {link:link}, function(data) {
    answerJson(data, {
      success: function () {
        data = JSON.parse(data);
        var modules = $(".module_block", data.html);
        $("div.item", modules).each(function(i, el) {
          var tr = {};
          tr.name = $(".property", el).text();
          row.push(tr);
        });
      }
    });

    look_load($(self), "");
    post("/admin/"+ model +"/addall?id_items="+id_items, {data:row}, function(data) {
      answerJson(data);
    });
  });
})

/**
  *  Инициализация Dropzone
  */
function initDropzone(){
  var myDropzone = new Dropzone(".dropzone-in-form", {
    url: '/admin/imageupload/dropzone',
    maxFiles: 1,
    thumbnailMethod: 'crop',
    dictMaxFilesExceeded: 'Можно загрузить только один файл',
    init: function () {
      this.on("success", function (file) {
          answerJson(file.xhr.response, {
              success: function () {
                data = JSON.parse(file.xhr.response);
                // Запомним имя файла, чтобы потом его использовать
                file.filename = data.name;
              }
          });
      });
      this.on("error", function (file, text) {
          $.hulla.send(text, 'danger');
          myDropzone.removeFile(file);
      });
  }});
}
