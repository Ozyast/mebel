var model = "category";

/*
 * FURNITURES
 */

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});

// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");

  var data = {};
  if($("input[type=checkbox]", $(this)).prop('checked'))
    data.visible = 1; else data.visible = 0;

  TableEditeSave(id, data, model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});

// EDIT: open modal
$('body').on("click", "#data-edit", function(){
  var id = $(this).parents("tr").attr("data-tr-id");
  TableEditForm(id, model, null, null, function() {
    // Инициализация Chosen
    $('.select-chosen').chosen({width: "100%", search_contains: true});

    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})
// EDIT: save modal update
$("body").on("submit", "#modal-table[data-action=edit] form", function(event){
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableEditeSave(data.id_category, data, model);
})

// ADD: open modal
$('body').on("click", "#data-add", function() {
  TableAddForm(model, null, function() {
    // Инициализация Chosen
    $('.select-chosen').chosen({width: "100%", search_contains: true});

    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})
// ADD: save add modal
$("body").on("submit", "#modal-table[data-action=add] form", function(event) {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableAddSave(data, model);
})

// SORT
// $("body").on("click", "#main_table th:not(.not-sort) i", function(){
//     var row = $(this).parents("th").attr("id");
//     var sort = $(this).attr("id");
//
//     TableSort(model, row, sort);
// })

// TABLE: Удалить фото
$('body').on('click', "#data-delete-image",function() {
  var id = $(this).parents("tr").attr("data-tr-id");

  post("/admin/"+model+"/imageremove?id="+id, null, function (data) {
    answerJson(data, {
      success: function () {
        data = JSON.parse(data);
        $("tr[data-tr-id="+id+"]").replaceWith(data.html);
      }
    });
  });
});

// Переключение между изображением и иконкой
$("body").on("change", "#form-category #icon", function(){
  var form = $("#form-category");

  if ($("input[id=icon]", form).prop("checked")) {
    $("div[id=icon-field]", form).removeClass("hidden");
    $("div[id=icon-field] input", form).attr("id", "image");
    $("div[id=image-field]", form).addClass("hidden");
    $("div[id=image-field] input", form).removeAttr("id");
  } else {
    $("div[id=image-field]", form).removeClass("hidden");
    $("div[id=image-field] input", form).attr("id", "image");
    $("div[id=icon-field]", form).addClass("hidden");
    $("div[id=icon-field] input", form).removeAttr("id");
  }
})

/**
  *  Инициализация Dropzone
  */
function initDropzone(){
  var myDropzone = new Dropzone(".dropzone-in-form", {
    url: '/admin/imageupload/dropzone',
    maxFiles: 1,
    thumbnailMethod: 'crop',
    dictMaxFilesExceeded: 'Можно загрузить только один файл',
    init: function () {
      this.on("success", function (file) {
          answerJson(file.xhr.response, {
              success: function () {
                data = JSON.parse(file.xhr.response);
                // Запомним имя файла, чтобы потом его использовать
                file.filename = data.name;
              }
          });
      });
      this.on("error", function (file, text) {
          $.hulla.send(text, 'danger');
          myDropzone.removeFile(file);
      });
  }});
}
