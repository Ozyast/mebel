var model = "price";

/*
 * price
 */

// DELETE COLs\ROWs BUTTON
$('body').on('click', "#data-delete-rows",function() {
  var tr = $(this).parents("tr") ;
  $(tr).hide();
});
$('body').on('click', "#data-delete-cols",function() {
  var index = parseInt($(this).parents("th").attr("id")) + 1;

  $("tbody td:nth-child("+index+")").hide();
  $("thead th:nth-child("+index+")").hide();
});

// Сохранаяем новые цены
$('body').on('click', "#data-save",function() {
  var data = [];

  $("thead tr:nth-child(1)").hide();
  $("tbody td:nth-child(1)").hide();
  $("tbody tr td:last-child").after("<td id='status'><i class='fa fa-spinner fa-spin'></td>");
  $(this).hide();

  $("tbody tr:visible").each(function (indx, element) {
    data[indx] = [];
    $("td:visible", element).each(function (i, el) {
      data[indx][i] = ($(el).text()).trim();
    });

    post("/admin/"+model+"/add", {data: data[indx]}, function (data) {
      answerJson(data, {
        done: function () {
          data = JSON.parse(data);
          $(element).addClass(data.status);
          if (data.description)
            $("td#status", element).html(data.description);
          else
            $("td#status", element).html(data.text);
        }
      });
    });
  });
});

// Инициализация Dropzone
if ($(".block").is(".dropzone"))
  initDropzone();

/**
  *  Инициализация Dropzone
  */
function initDropzone(){
  var myDropzone = new Dropzone("form.dropzone", {
    // url: '/admin/imageupload/dropzone',
    maxFiles: 1,
    thumbnailMethod: 'crop',
    dictMaxFilesExceeded: 'Можно загрузить только один файл',
    init: function () {
      this.on("complete", function (file) {
          answerJson(file.xhr.response, {
              success: function () {
                data = JSON.parse(file.xhr.response);
                // Запомним имя файла, чтобы потом его использовать
                file.filename = data.name;
              },
              error: function () {
                data = JSON.parse(file.xhr.response);
                file.previewElement.classList.add("dz-error");
                $('.dz-error-message', $(file.previewElement)).text(data.text)
              }
          });
      });
      this.on("error", function (file, text) {
          $.hulla.send(text, 'danger');
          myDropzone.removeFile(file);
      });
  }});
}

// CHECKBOX
// $('body').on('change', ".data-checkbox",function() {
//   var id = $(this).parents("tr").attr("data-tr-id");
//   TableCheckboxSave(id, $(this), model, null, 1);
// });
//
// // SEARCH
// $("body").on("submit", "#form-search", function(event) {
//   var s = $("input[id=data-search]", $(this)).val();
//   var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
//   var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");
//
//   TableSearch(s, r, t, model);
// });
//
// // EDIT
// $('body').on("click", "#data-edit", function(){
//   var id = $(this).parents("tr").attr("data-tr-id");
//   TableEditForm(id, model, null, null, function() {
//     // Инициализация Dropzone
//     if ($("#modal-table div").is(".dropzone"))
//       initDropzone();
//   });
// })
//
// // SAVE MODAL
// $("body").on("submit", "#modal-table[data-action=edit] form", function(event){
//   var data = FormRead($(this), event, $("button[type=submit]", $(this)));
//   TableEditeSave(data.id_page, data, model);
// })
//
// // ADD
// $('body').on("click", "#data-add", function() {
//   TableAddForm(model, null, function() {
//     // Инициализация Dropzone
//     if ($("#modal-table div").is(".dropzone"))
//     initDropzone();
//   });
// })
//
// // SAVE ADD MODAL
// $("body").on("submit", "#modal-table[data-action=add] form", function(event) {
//   var data = FormRead($(this), event, $("button[type=submit]", $(this)));
//   TableAddSave(data, model);
// })
//
// // SORT
// $("body").on("click", "#main_table th:not(.not-sort) i", function(){
//     var row = $(this).parents("th").attr("id");
//     var sort = $(this).attr("id");
//
//     TableSort(model, row, sort);
// })
//
