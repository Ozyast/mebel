var model = "similar";

/*
 * FURNITURES
 */

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});

// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  TableCheckboxSave(id, $(this), model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});

// EDIT: open modal
$('body').on("click", "#data-edit", function(){
  var id = $(this).parents("tr").attr("data-tr-id");
  TableEditForm(id, model, null, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})
// EDIT: save modal update
$("body").on("submit", "#modal-table[data-action=edit] form", function(event){
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableEditeSave(data.id_similar, data, model);
})

// ADD: open modal
$('body').on("click", "#data-add", function() {
  TableAddForm(model, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})
// ADD: save add modal
$("body").on("submit", "#modal-table[data-action=add] form", function(event) {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableAddSave(data, model);
})

// SORT
$("body").on("click", "#main_table th:not(.not-sort) i", function(){
    var row = $(this).parents("th").attr("id");
    var sort = $(this).attr("id");

    TableSort(model, row, sort);
})
