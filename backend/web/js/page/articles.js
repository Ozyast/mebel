var model = "articles";

/*
 * FURNITURES
 */

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});

// Удалить фото
$('body').on('click', "#data-delete-image",function() {
  var id = $(this).parents("tr").attr("data-tr-id");

  post("/admin/"+model+"/imageremove?id="+id, null, function (data) {
    answerJson(data, {
      success: function () {
        data = JSON.parse(data);
        $("tr[data-tr-id="+id+"]").replaceWith(data.html);
      }
    });
  });
});

// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  TableCheckboxSave(id, $(this), model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});

// EDIT
$('body').on("click", "#data-edit", function(){
  var id = $(this).parents("tr").attr("data-tr-id");
  TableEditForm(id, model, null, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})

// SAVE MODAL
$("body").on("submit", "#modal-table[data-action=edit] form", function(event){
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableEditeSave(data.id_articles, data, model);
})

// ADD
$('body').on("click", "#data-add", function() {
  TableAddForm(model, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
    initDropzone();
  });
})

// SAVE ADD MODAL
$("body").on("submit", "#modal-table[data-action=add] form", function(event) {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableAddSave(data, model);
})

// SORT
$("body").on("click", "#main_table th:not(.not-sort) i", function(){
    var row = $(this).parents("th").attr("id");
    var sort = $(this).attr("id");

    TableSort(model, row, sort);
})

/**
  * ФОТО
  */
// Добавление/Удаление фото
$('body').on("click", "#data-image", function () {
    look_load($(this), "");
    var id = $(this).parents("tr").attr("data-tr-id");

    modal_dialog_open("/admin/"+model+"/image?id="+id, null, function(data) {

      // Инициализация Dropzone
      if ($("#modal-dialog form").is(".dropzone"))
        initDropzone();
    });
})
// Удаление фото
$('body').on('click', ".modal-dialog #data-delete-photo",function() {
  var src = $(this).parents("div").attr("data-src");

  if (confirm("Вы действительно хотите удалить фото - "+src)) {
    post("/admin/"+model+"/imagedelete?src="+src, null, function (data) {
        answerJson(data, {
            success: function () {
                $(".gallery-image-options[data-src='" + src + "']").parents("div.col-sm-3").hide();
            }
        });
    })
  }
});

/**
  * ФАЙЛ
  */
// Открываем редактирование файла
$('body').on("click", "#data-file", function () {
    look_load($(this), "");
    var id = $(this).parents("tr").attr("data-tr-id");

    modal_dialog_open("/admin/"+model+"/file?id="+id, null, function(data) {
    });
})
// Удаление фото
$('body').on('submit', ".modal-dialog #form_articles_file",function() {
  var id = $(this).attr("data-id");
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));

  post("/admin/"+model+"/filesave?id="+id, data, function (data) {
      answerJson(data, {
          success: function () {
            data = JSON.parse(data);
            $("#modal-dialog").modal('hide');
            $("tr[data-tr-id=" + id + "]").replaceWith(data.html);
          }
      });
  })
});

/**
  *  Инициализация Dropzone
  */
function initDropzone(){
  var myDropzone = new Dropzone("form.dropzone", {
    // url: '/admin/imageupload/dropzone',
    // maxFiles: 1,
    thumbnailMethod: 'crop',
    // dictMaxFilesExceeded: 'Можно загрузить только один файл',
    init: function () {
      this.on("complete", function (file) {
          answerJson(file.xhr.response, {
              success: function () {
                data = JSON.parse(file.xhr.response);
                // Запомним имя файла, чтобы потом его использовать
                file.filename = data.name;
              },
              error: function () {
                data = JSON.parse(file.xhr.response);
                file.previewElement.classList.add("dz-error");
                $('.dz-error-message', $(file.previewElement)).text(data.text)
              }
          });
      });
      this.on("error", function (file, text) {
          $.hulla.send(text, 'danger');
          myDropzone.removeFile(file);
      });
  }});
}
