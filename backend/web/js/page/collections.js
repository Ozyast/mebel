var model = "collections";

/*
 * FURNITURES
 */

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});

// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  TableCheckboxSave(id, $(this), model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});

// EDIT
$('body').on("click", "#data-edit", function(){
  var id = $(this).parents("tr").attr("data-tr-id");
  TableEditForm(id, model, null, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
      initDropzone();
  });
})

// SAVE MODAL
$("body").on("submit", "#modal-table[data-action=edit] form", function(event){
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableEditeSave(data.id_collections, data, model);
})

// ADD
$('body').on("click", "#data-add", function() {
  TableAddForm(model, null, function() {
    // Инициализация Dropzone
    if ($("#modal-table div").is(".dropzone"))
    initDropzone();
  });
})

// SAVE ADD MODAL
$("body").on("submit", "#modal-table[data-action=add] form", function(event) {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableAddSave(data, model);
})

// SORT
// $("body").on("click", "#main_table th:not(.not-sort) i", function(){
//     var row = $(this).parents("th").attr("id");
//     var sort = $(this).attr("id");
//
//     TableSort(model, row, sort);
// })

/**
* ЗАГРУЗКА МОДУЛЕЙ
*/
$('body').on("click", "#data-get-module", function (event) {
  var link = prompt('Введите ссылку', '');
  var row = [];
  var id_collections = $(this).parents("tr").attr("data-tr-id");
  var self = $(this);

  if (!link.length) {
    return;
  }
  look_load($(self), "");

  link = "http://srv82925.ht-test.ru/index.php?url="+link;

  post("/admin/modules/getpage", {link:link}, function(data) {
    answerJson(data, {
      success: function () {
        data = JSON.parse(data);
        var modules = $(".module_block", data.html);
        $("div.item", modules).each(function(i, el) {
          var tr = {};
          tr.id_collections = id_collections;
          tr.image = ($(".photo img", el).attr("srca")).match(/\/([^.\/]*\.(jpg|gif))/i)[1];
          tr.image =  "http://srv82925.ht-test.ru/image.php?url=" + "http://lerom.ru/upload/iblock/" + tr.image.match(/^(.{3})/)[1] +"/"+ tr.image;
          tr.name = $(".property", el).text();
          tr.characteristics = $(".property2", el).text();
          tr.price = parseInt(($(".price", el).text()).replace(" ",""));

          row.push(tr);
        });
      }
    });

    look_load($(self), "");
    post("/admin/modules/addall", {data:row}, function(data) {
      answerJson(data);
    });
  });
})

/**
  *  Инициализация Dropzone
  */
function initDropzone(){
  var myDropzone = new Dropzone(".dropzone-in-form", {
    url: '/admin/imageupload/dropzone',
    maxFiles: 1,
    thumbnailMethod: 'crop',
    dictMaxFilesExceeded: 'Можно загрузить только один файл',
    init: function () {
      this.on("success", function (file) {
          answerJson(file.xhr.response, {
              success: function () {
                data = JSON.parse(file.xhr.response);
                // Запомним имя файла, чтобы потом его использовать
                file.filename = data.name;
              }
          });
      });
      this.on("error", function (file, text) {
          $.hulla.send(text, 'danger');
          myDropzone.removeFile(file);
      });
  }});
}
