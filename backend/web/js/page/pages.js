/*
 * GLOBAL options
 */
$("*[rel=tooltip]").tooltip();
/* tooltip huk */
$("body").on("mouseleave", '[data-toggle="tooltip"]', function () {
  $("*").tooltip('hide');
});
$.hulla = new hullabaloo();
$.hulla.options.fnEnd = function() {
    look_reset();
}

/**
  * Обработка ответа от PHP
  */
function answerJson(response, callback) {
  try {
    response = JSON.parse(response);
  } catch (e) {
    console.log("Ошибка обработки ответа: ");
    console.log(response);

    // Если не удалось декодировать JSON создадим свою ошибку
    response = {};
    response.text = "Произошла неизвестная ошибка.";
    response.status = "danger";
  }

  // Если есть дополнительные параметры
  /*
   * success - функция выполняется при успешном ответе от сервера
   * error - функция выполняется если от сервера пришел ответ с ошибкой
   * end - функция выполняется при любом ответе от сервера
   */
   if (typeof callback == "object") {
     if (typeof callback.success == "function" && response.status == "success")
       callback.success();
     if (typeof callback.error == "function" && response.status == "danger")
       callback.error();
     if (typeof callback.done == "function")
       callback.done();
   }

  // Если нет текста и статус SUCCESS то не выводим сообщение
  // а просто запустим функцию окончания скрипта
  if (response.status == "success" && !response.text) {
    $.hulla.options.fnEnd();
  }else{
    $.hulla.send(response.text, response.status);
    // Запустим функцию после закрытия сообщения
    if(typeof callback == "object" && typeof callback.end == "function")
        callback.end();
  }
}

/*
 *  LEFT MENU select
 */
$(document).ready(function () {
    var str = window.location.pathname;
    //str = str.substr(1);
    if (str == "")
        $(".sidebar-nav li:first").addClass("current_page_item");
    else {
        var link = $(".sidebar-nav li a[href='" + str + "']");
        link.addClass("active");
        if (link.hasClass("sub-menu"))
            link.parents("li:last").addClass("active");
    }
});

/*
 * MODAL DIALOG
 */
// Open modal dialog
function modal_dialog_open(url, data, callback) {
    data = data || "null";

    $("#modal-dialog .modal-content").html();

    post(url, {data: data}, function (answer) {
        answerJson(answer, {
            success: function () {
                answer = JSON.parse(answer);
                $("#modal-dialog .modal-content").html(answer.html);
                $("*[data-toogle=popover]").popover();
                if (callback !== undefined && callback !== null)
                    callback();
                $("#modal-dialog").modal('show');

                // Инициализация Chosen
                if ($("#modal-table div").is(".select-chosen"))
                  $('.select-chosen').chosen({width: "100%", search_contains: true});
            }
        });
    });
}
// Очищает содержимое модального окна и отображает загрузку
function modal_dialog_inload() {
    $("#modal-table .modal-body, #modal-dialog .modal-content").html("<div class='modal-body'><p class='text-center text-muted'><i class='fa fa-spinner fa-spin'></i> Загружаю </p></div>");
}
// После закрытия модального окна, очищаем его содержимое
$('#modal-table, #modal-dialog').on('hidden.bs.modal', function (e) {
    modal_dialog_inload();
})


/**
  * Добавление в адресную строку новых ГЕТ параметров
  */
function urlGet(row) {
    var url = window.location.pathname;
    var get_url = [];

    // Если не передали объект с параметрами или он пуст
    if (typeof row == "undefined" || !Object.keys(row).length)
      return url;

    // Проверяем есть ли уже параметры в адресной строке
    if (window.location.search) {
      // Сформируем массив с уже имеющимися параметрами GET
      var get = decodeURIComponent(location.search.substr(1)).split('&');
      get.forEach(function (item, i, arr) {
          var regexp = /^([^=]*)=(.*)$/i;
          var regexp = regexp.exec(item);

          // Если такой переменной нет в новом массиве, то запишем старое значение
          if (typeof row[regexp[1]] === 'undefined')
            get_url.push(regexp[1] + "=" + regexp[2]);
      });
    }

    // Добавим в массиву новые параметры GET
    $.each(row, function(index, value) {
      // Убираем пустые параметры !(Если тип параметра строка и длинна его равна 0 )
      if (!(typeof value === "string" && !value.length))
        get_url.push(index + "=" + value);
    });

    // Объеденим все параметры в строку если они есть
    if(get_url.length) {
      get_url = "?"+get_url.join('&');
    }

    return url+get_url;
}

/*
 * DATA TABLE
 */
// DELETE for table
function TableDelete(id, model, postURL) {
    postURL = postURL || "/admin/" + model + "/delete?id=" + id;
    post(postURL, null, function (data) {
        answerJson(data, {
            success: function () {
                $("tr[data-tr-id=" + id + "]").hide();
            }
        });
    })
}

/**
 * SEARCH for table
 * @param {type} search
 * @param {type} row
 * @param {type} type
 * @param {type} model
 * @returns {undefined}
 */
function TableSearch(search, row, type, model) {
    event.preventDefault();
    look_load("#form-search #data-search-button");


    if (!search.length || !row.length || !type.length){
      var get_url = urlGet({"s": "", "row": "", 't': ""});
      document.location.replace(get_url);
    } else {
      var get_url = urlGet({"s": search, "row": row, 't': type});
      document.location.replace(get_url);
    }
}
$(document).ready(function () {
    // SEARCH настройка формы после перезакгрузки страницы
    if ($("#form-search").attr('data-active') == "1") {
        var row = $("#form-search #data-search-rows #rows").text();
        var li = $("#form-search #data-search-rows-list li[id='" + row + "']");
        var row_ru = $(li, "a").text();
        var t = li.attr("data-type") || "i";

        $("#form-search #data-search-rows #rows").text(row_ru);
        $("#form-search #data-search-rows").attr("data-rows", row);
        $("#form-search #data-search-rows").attr("data-rows-type", t);
    }

    // Выбор столбца поиска
    $("body").on("click", ".input-group-btn #data-search-rows-list li", function () {
        var id = $(this).attr("id");
        var type = $(this).attr("data-type");
        var text = $(this).text();

        var button = $(this).parents(".input-group-btn").find("button#data-search-rows");
        button.attr("data-rows", id);
        button.attr("data-rows-type", type);
        button.find("span#rows").text(text);
    })
})

// SORT
// function hover
$(document).ready(function () {
    $("body").on("mouseenter", "#main_table th:not(.not-sort)", function () {
        var text = $(this).html();
        $(this).html(text + " <span id='sort'><i class='fa fa-sort-asc' id='asc'></i><i class='fa fa-sort-desc' id='desc'></i></span>");
    })
    $("body").on("mouseleave", "#main_table th", function () {
        $("#sort", $(this)).remove();
    })
})
// Action
function TableSort(model, row, type) {
    var get_url = "";

    if (!row.length)
        $.hulla.send("Нет имени колонки для сортировки", "warning");

    if (!type.length)
        $.hulla.send("Не задан тип сортировки", "warning");

    if (window.location.search) {
        var get_array = [];

        var get = decodeURIComponent(location.search.substr(1)).split('&');
        get.forEach(function (item, i, arr) {
            var regexp = /^([^=]*)=(.*)$/i;
            var regexp = regexp.exec(item);
            if (regexp[1] !== "so" && regexp[1] !== "sotype")
                get_array.push(regexp[1] + "=" + regexp[2]);
        });
        get_url = get_array.join('&');
    }

    if (get_url.length)
        get_url = get_url + "&so=" + row + "&sotype=" + type;
    else
        get_url = "so=" + row + "&sotype=" + type;

    var postURL = postURL || "/admin/" + model + "/index?" + get_url;
    document.location.replace(postURL);
}


// EDIT FORM for table
function TableEditForm(id, model, postURL, action, callback) {
    // Очистим содержимое от старых записей
    modal_dialog_inload();

    // Ссылка для получения содержимого окна
    postURL = postURL || "/admin/" + model + "/update?id=" + id;
    // актион окна для определения на какое событие обращаться add/edit и т.д.
    action = action || "edit";

    // Заполним содержимое по ссылке
    load($("#modal-table .modal-content"), postURL, null, function() {
      // Инициализация Chosen
      if ($("#modal-table select").is(".select-chosen"))
        $('.select-chosen').chosen({width: "100%", search_contains: true});

      if (callback !== undefined)
          callback();
    });
    // Выведем модальное окно и пометим его актионом
    $("#modal-table").modal('show').attr("data-action", action);
//  $("#modal-table #data-save").attr("form", "form-"+model);
}

// EDIT FORM SAVE for table
function TableEditeSave(id, tr, model, postURL, not_refresh) {
    not_refresh = not_refresh || 0; // Обновление таблицы. 1 - не обновлять
    postURL = postURL || "/admin/" + model + "/update?id=" + id;

    post(postURL, {data: tr}, function (data) {
        answerJson(data, {
            success: function () {
                data = JSON.parse(data);
                $("#modal-table").modal('hide');

                if (!not_refresh) {  // Если требуется обновление таблицы
                    $("tr[data-tr-id=" + id + "]").replaceWith(data.html);
                }
            }
        });
    })
}

// CHECKBOX EDIT SAVE for table
function TableCheckboxSave(id, checkbox, model, postURL) {
    postURL = postURL || "/admin/" + model + "/update?id=" + id;
    var tr = {}
    var checkbox = $("input[type=checkbox]", $(checkbox));
    var name = $(checkbox).attr("id");

    if($(checkbox).prop('checked'))
      tr[name] = 1; else tr[name] = 0;

    post(postURL, {data: tr}, function (data) {
      answerJson(data, {
        error: function () {
          $(checkbox).prop('checked', !$(checkbox).prop('checked'));
        }
      });
    })
}

// ADD FORM for table
function TableAddForm(model, data, callback) {
    // Данные для передачи при формировании формы
    data = data || null;
    // Очистим содержимое от старых записей
    modal_dialog_inload();

    // Заполним содержимое по ссылке
    load($("#modal-table .modal-content"), "/admin/" + model + "/add", {value: data}, function(){
      // Инициализация Dropzone
      if ($("#modal-table div").is(".dropzone"))
        initDropzone();
      // Инициализация Chosen
      if ($("#modal-table select").is(".select-chosen"))
        $('.select-chosen').chosen({width: "100%", search_contains: true});

      // Колбык функция, к примеру для инициализации каких то плагинов
      if (callback !== undefined)
          callback();
    });
    // Выведем модальное окно и пометим его актионом
    $("#modal-table").modal('show').attr("data-action", "add");

}

// ADD FORM SAVE for table
function TableAddSave(tr, model, postURL, not_refresh, callback) {
    not_refresh = not_refresh || 0; // Обновление таблицы. 1 - не обновлять
    postURL = postURL || "/admin/" + model + "/add";  // Адресс отправки данных

    post(postURL, {data: tr}, function (data) {
        answerJson(data, {
            success: function () {
                data = JSON.parse(data);
                $("#modal-table").modal('hide');

                if (!not_refresh) {  // Если требуется обновление таблицы
                    $("#main_table tbody").prepend(data.html);
                }

                if (callback !== undefined)
                    callback();
            }
        });
    })
}

// Auto-read form
function FormRead(form, event, load_button) {
    if ((event === undefined) || (event))
        event.preventDefault();

    if ((load_button === undefined) || (load_button))
        look_load($(load_button));


    if (!($(form).length) || ($(form).get(0).tagName !== "FORM")) {
        console.log("$(form).length = " + $(form).length);
        console.log("form.nodeName = " + $(form).get(0).tagName);

        look_send("SYSTEM: Нет формы для получения данных", "warning");
        look_reset();
    }

    var data = {};

    // read dropzone
    // Проверим есть ли на странице дропзон и есть ли там файлы
    if ($(form).has(".dropzone") && $(".dropzone .dz-image", $(form)).length) {
      // Получим имя
      var name = $(".dropzone", $(form)).attr("id");
      // Получим весь дропзон
      var files = Dropzone.forElement(".dropzone");
      var filesname = [];
      // Вытащим из дропзона все файлы
      if (files.files.length) {
        $(files.files).each(function (indx) {
          filesname.push($(this)[0].filename);
        });
        // Запомним массив с нашими файлами
        data[name] = filesname;
        // Если в настройках дропзоне указана максю число файлов - 1 (maxFiles: 1)
        if(files.options.maxFiles == 1)
          // то возьмем первый файл и запишем его в переменную уже не в виде массива
          data[name] = filesname[0];
      }
    }

    // read input
    $("input[type=text], input[type=password], input[type=number], input[type=email], input[type=hidden]", $(form)).each(function (indx, element) {
        var name = $(element).attr('id');
        data[name] = $(element).val();
    });

    // read select
    $("select", $(form)).each(function (indx, element) {
        var name = $(element).attr('id');
        data[name] = $(element).val();
    });

    // read textarea
    $("textarea", $(form)).each(function (indx, element) {
        var name = $(element).attr('id');
        data[name] = $(element).val();
    });

    // read checkbox
    $("input[type=checkbox]", $(form)).each(function (indx, element) {
        var name = $(element).attr('id');
        if ($(element).prop('checked'))
            data[name] = 1;
        else
            data[name] = 0;
    });

    return data;
}


/**
 * Обертка для функции Post с обработкой ошибок на стороне сервера
 */
function post(url, data, callback) {
    try {
        $.post(url, data, function (data) {
            if (callback !== undefined)
                callback(data);
        }).fail(function (Error) {
            console.log(Error.responseText);
            $.hulla.send("Произошла неизвестная ошибка. Попробуйте перезагрузить страницу и повторить операцию.", "danger");
            if (callback !== undefined)
                callback(data);
        })
    } catch (e) {
      console.log(e);
      $.hulla.send("Произошла неизвестная ошибка. Попробуйте перезагрузить страницу и повторить операцию.", "danger");
    }
}

/*
  * Обертка для функции $.load с обработкой ошибок на стороне сервера
  */
function load(el, url, data, callback) {
    $(el).load(url, data, function(data, textStatus, XMLHttpRequest) {
      if (textStatus == 'error') {
        console.log(data);
        $.hulla.send("Произошла неизвестная ошибка. Попробуйте перезагрузить страницу и повторить операцию.", "danger");
        return;
      }

      if (typeof callback == "function")
          callback();
    });
}

/**
 *  Индикатор загрузки
 */
function look_load(el, text) {
    if (typeof text === "undefined")
        text = " Думаю...";
    else
        text = "";

    look_element = el;
    look_element_text = $(el).html();

    $(el).html("<i class='fa fa-refresh fa-spin'></i>" + text).addClass("disabled");
    $("fieldset").attr("disabled", "");
}
function look_reset() {
    // Обнуляем кнопки загрузки
    if(typeof look_element !== 'undefined' || typeof look_element_text !== 'undefined'){
      $(look_element).html(look_element_text).removeClass('disabled');
      // Снимаем ограничение по записи
      $("fieldset").removeAttr("disabled");
      $(look_element).tooltip("destroy").tooltip();
    }
}

/*
 * RIGHT MENU
 */
