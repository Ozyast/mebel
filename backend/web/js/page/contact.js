var model = "contact";

/*
 * FURNITURES
 */

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});

// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  TableCheckboxSave(id, $(this), model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});
