var model = "items";

// DELETE BUTTON
$('body').on('click', "#data-delete",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  if (confirm("Вы действительно хотите удалить запись с ID - "+id)) {
    TableDelete(id, model);
  }
});


// CHECKBOX
$('body').on('change', ".data-checkbox",function() {
  var id = $(this).parents("tr").attr("data-tr-id");
  TableCheckboxSave(id, $(this), model, null, 1);
});

// SEARCH
$("body").on("submit", "#form-search", function(event) {
  var s = $("input[id=data-search]", $(this)).val();
  var r = $("button[id=data-search-rows]", $(this)).attr("data-rows");
  var t = $("button[id=data-search-rows]", $(this)).attr("data-rows-type");

  TableSearch(s, r, t, model);
});

// EDIT
$('body').on("click", "#data-edit", function(){
  var id = $(this).parents("tr").attr("data-tr-id");
  TableEditForm(id, model, null, null, function() {
    // Оставим только цифры в ценах
    $("#form-items #price").on('change', function(){
      $("#price").val(($("#price").val()).replace(/[^0-9]/g, ""));
    })
    $("#form-items #sale_price").on('change', function(){
      $("#sale_price").val(($("#sale_price").val()).replace(/[^0-9]/g, ""));
    })
  });
})

// SAVE MODAL
$("body").on("submit", "#modal-table[data-action=edit] form", function(event){
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));
  TableEditeSave(data.id_items, data, model);
})

// ADD
$('body').on("click", "#data-add", function() {
  TableAddForm(model, null, function() {
    // Оставим только цифры в ценах
    $("#form-items #price").on('change', function(){
      $("#price").val(($("#price").val()).replace(/[^0-9]/g, ""));
    })
    $("#form-items #sale_price").on('change', function(){
      $("#sale_price").val(($("#sale_price").val()).replace(/[^0-9]/g, ""));
    })
  });
})

// SAVE ADD MODAL
$("body").on("submit", "#modal-table[data-action=add] form", function(event) {
  var data = FormRead($(this), event, $("button[type=submit]", $(this)));

  TableAddSave(data, model);
})

// SORT
$("body").on("click", "#main_table th:not(.not-sort) i", function(){
    var row = $(this).parents("th").attr("id");
    var sort = $(this).attr("id");

    TableSort(model, row, sort);
})

// КОЛЛИЧЕСТВО (БЕСКОНЕЧНОЕ)
$("body").on("change", "#form-items #sold_out", function(){
  var sold = $("#form-items input[id=sold]");
  var instock = $("#form-items input[id=instock]");
  var sold_out = $("#form-items input[id=sold_out]");
  var instock_color = $("#form-items input[id=instock_color]");

  if (sold_out.prop("checked")) {
    instock.val(0).attr("disabled", "disabled");
    instock_color.prop("checked", false).attr("disabled", "disabled");
    sold.val(0);
  } else {
    instock.removeAttr("disabled");
    instock_color.removeAttr("disabled");
    sold.val(1);
  }
})
// КОЛЛИЧЕСТВО ШТУЧНО
$("body").on("change", "#form-items #instock", function(){
    var instock = $("#form-items input[id=instock]");
    var sold_out = $("#form-items input[id=sold_out]");
    var sold = $("#form-items input[id=sold]");

    if (!parseInt(instock.val())) {
      if (!sold_out.prop("checked") && !parseInt(sold.val()))
        sold.val(1);
    } else {
      if (parseInt(sold.val()))
        sold.val(0);
    }
})
// КОЛЛИЧЕСТВО ПО ЦВЕТАМ
$("body").on("change", "#form-items #instock_color", function(){
    var id = $(this).parents("form").attr("data-id");
    var instock_color = $("#form-items input[id=instock_color]");
    var instock = $("#form-items input[id=instock]");
    var sold_out = $("#form-items input[id=sold_out]");
    var sold = $("#form-items input[id=sold]");

    if (instock_color.prop("checked") && !id) {
      instock.val(0);
      sold.val(1);
      instock.attr("disabled", "disabled");
      sold_out.prop("checked", false).attr("disabled", "disabled");

    } else if (instock_color.prop("checked")) {
      // Получить кол-во товара по цветам
      post("/admin/"+model+"/countinstockcolor?id="+id, null, function (data) {
        answerJson(data, {
          success: function () {
            data = JSON.parse(data);

            instock.val(parseInt(data.count));
            if (data.count)
              sold.val(0);
            else
              sold.val(1);
          }
        });
      });

      instock.attr("disabled", "disabled");
      sold_out.prop("checked", false).attr("disabled", "disabled");
    } else {
      instock.removeAttr("disabled");
      sold_out.removeAttr("disabled");
      sold.val(1);
    }
})

/**
  * ОПИСАНИЕ
  */
  // Устанавливает флаг в положение true если было изменено поле описание
$("body").on("change", "#form-items #description", function(){
  $("#form-items #description_change").val(1);
});

/**
  * ФОТО
  */
// Добавление/Удаление фото
$('body').on("click", "#data-image", function () {
    look_load($(this), "");
    var id = $(this).parents("tr").attr("data-tr-id");

    modal_dialog_open("/admin/"+model+"/image?id="+id, null, function(data) {

      // Инициализация Dropzone
      if ($("#modal-dialog form").is(".dropzone"))
        initDropzone();
    });
})
// Установка фото на главное
$('body').on("click", "#data-image-main", function () {
    var id = $(this).parents(".modal-body").attr("data-tr-id");
    var idImg = $(this).parents("#images").attr("data-id");

    post("/admin/"+model+"/imagemain?id="+id+"&idImg="+idImg, null, function (data) {
        answerJson(data, {
            success: function () {
              data = JSON.parse(data);
              $("tr[data-tr-id=" + id + "]").replaceWith(data.tr);
              $("#modal-dialog #images[data-id="+idImg+"]").replaceWith(data.image);
            }
        });
    })
})
// Удаление фото
$('body').on('click', ".modal-dialog #data-delete-photo",function() {
  var id = $(this).parents("#images").attr("data-id");

  if (confirm("Вы действительно хотите удалить фото с ID - "+id)) {
    post("/admin/image/delete?id="+id, null, function (data) {
        answerJson(data, {
            success: function () {
                $("#modal-dialog #images[data-id=" + id + "]").hide();
            }
        });
    })
  }
});


/**
  * ПЕРЕСЧЕТ ЦЕНЫ
  */
$('body').on('click', "#data-price-recount",function() {
  var id = $(this).parents("tr").attr("data-tr-id");

  post("/admin/price/recount?id_items="+id, null, function (data) {
    answerJson(data, {
      success: function () {
        data = JSON.parse(data);
        $("tr[data-tr-id=" + id + "]").replaceWith(data.html);
      }
    });
  })
});

/**
  *  Инициализация Dropzone
  */
function initDropzone(){
  var myDropzone = new Dropzone("form.dropzone", {
    // url: '/admin/imageupload/dropzone',
    // maxFiles: 1,
    thumbnailMethod: 'crop',
    // dictMaxFilesExceeded: 'Можно загрузить только один файл',
    init: function () {
      this.on("complete", function (file) {
          answerJson(file.xhr.response, {
              success: function () {
                data = JSON.parse(file.xhr.response);
                // Запомним имя файла, чтобы потом его использовать
                file.filename = data.name;

                console.log($('#modal-dialog #images').length);
                if ($('#modal-dialog #images').length)
                  $("#modal-dialog .gallery").append(data.image);
                else
                  $("#modal-dialog .gallery").html(data.image);
                myDropzone.removeFile(file);
              },
              error: function () {
                data = JSON.parse(file.xhr.response);
                file.previewElement.classList.add("dz-error");
                $('.dz-error-message', $(file.previewElement)).text(data.text)
              }
          });
      });
      this.on("error", function (file, text) {
          $.hulla.send(text, 'danger');
          myDropzone.removeFile(file);
      });
  }});
}
