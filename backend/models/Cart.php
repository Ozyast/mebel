<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id_cart
 * @property string $hash
 * @property string $status
 * @property integer $price
 * @property integer $sale
 * @property integer $price_total
 * @property string $user_name
 * @property string $user_phone
 * @property integer $delivery
 * @property string $user_address
 * @property string $comment
 * @property integer $paid-up
 * @property string $created_at
 * @property string $updated_at
 * @property integer $done
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash', 'status', 'user_phone', 'price', 'price_total'], 'required'],
            [['price', 'sale', 'price_total', 'delivery', 'paid-up', 'done'], 'integer'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['hash', 'user_address'], 'string', 'max' => 250],
            [['status', 'user_name'], 'string', 'max' => 100],
            [['user_phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cart' => 'Id Cart',
            'hash' => 'Hash',
            'status' => 'Status',
            'price' => 'Price',
            'sale' => 'Sale',
            'price_total' => 'Price Total',
            'user_name' => 'User Name',
            'user_phone' => 'User Phone',
            'delivery' => 'Delivery',
            'user_address' => 'User Address',
            'comment' => 'Comment',
            'paid-up' => 'Paid Up',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'done' => 'Done',
        ];
    }

    /**
      * ПРОИЗВОДИТЕЛЬ
      * @return \yii\db\ActiveQuery
      */
    public function getCartitem()
    {
        return $this->hasMany(CartItem::className(), ['id_cart' => 'id_cart']);
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Cart::find();
    }

    /**
      * Отмечает все товары как проданные отнимает из общего кол-ва или кол-ва в цвете,
      * кол-во купленного товара
      */
    public function itemsSold()
    {
      $cartItem = CartItem::find()
        ->where(["id_cart" => $this->id_cart])
        ->with('items')
        ->all();

      if (!$cartItem) {
        return ['status'=>0, "text"=>'Товары из корзины не найдены'];
      }

      // Начало транзакции
      $transaction = Yii::$app->db->beginTransaction();

      foreach ($cartItem as $item) {
        // Если товара нет в наличии
        if (!$item->items->instock && $item->items->sold) {
          return ['status'=>0, "text"=>'Товар уже продан (ID: '.$item->id_items.', color: '.$item->color.')'];

        // Если товар есть в наличии
        } else {

          // Если у товара есть цвета
          if ($item->items->instock_color) {
            // Если товар этого цвета уже распродан
            if (!$item->colorsRelationship->visible) {
              return ['status'=>0, "text"=>'Товар этого цвета уже распродан (ID: '.$item->id_items.', color: '.$item->color.')'];
              // Если товар этого цвета есть в наличии
            } elseif ($item->colorsRelationship->instock) {
              // Если товара не хватает
              if ($item->colorsRelationship->instock < $item->count) {
                return ['status'=>0, "text"=>'Товара не достаточно (ID: '.$item->id_items.', color: '.$item->color.')'];
              }
              // Уменьшим кол-во товара на кол-во товара в заказе
              $item->colorsRelationship->instock -= $item->count;

              // Если товар закончился, пометим его как проданный
              if ($item->colorsRelationship->instock <= 0) {
                $item->colorsRelationship->visible = 0;
              }

              // Сохраним
              $item->colorsRelationship->save();
              if (!$item->colorsRelationship->save()) {
                $message = $item->colorsRelationship->getErrors();
                $message = array_shift($message);
                return ["status"=>0, "text"=>$message[0]];
              }
            }
          }

          // Отнимем из общего кол-ва купленый товар
          // Если товара не хватает
          if ($item->items->instock < $item->count)
            return ['status'=>0, "text"=>'Товара не достаточно (ID: '.$item->id_items.', color: '.$item->color.')'];


          // Отнимем кол-во из основной записи
          $item->items->instock -= $item->count;

          // Если товар закончился, пометим его как проданный
          if ($item->items->instock <= 0) {
            $item->items->sold = 1;
          }

          // Сохраним
          $item->items->save();
          if (!$item->items->save()) {
            $message = $model->getErrors();
            $message = array_shift($message);
            return ["status"=>0, "text"=>$message[0]];
          }
        }
      }

      // Сохраним изменения базы
      $transaction->commit();

      return ["status"=>1];
    }

    /**
      * Отменяет завершение корзины возвращает кол-во товара
      */
    public function itemsSoldRestore()
    {
      $cartItem = CartItem::find()
        ->where(["id_cart" => $this->id_cart])
        ->with('items')
        ->all();

      if (!$cartItem) {
        return ['status'=>0, "text"=>'Товар из корзины не найден'];
      }

      // Начало транзакции
      $transaction = Yii::$app->db->beginTransaction();

      foreach ($cartItem as $item) {
        // Если у товара есть цвета
        if ($item->color) {
            // Увеличим кол-во товара на кол-во товара в заказе
            $item->colorsRelationship->instock += $item->count;

            // Если товар закончился, пометим его как проданный
            if (!$item->colorsRelationship->visible) {
              $item->colorsRelationship->visible = 1;
            }

            // Сохраним
            if (!$item->colorsRelationship->save()) {
              $message = $item->colorsRelationship->getErrors();
              $message = array_shift($message);
              return ["status"=>0, "text"=>$message[0]];
            }
          }

          // Прибавим к общему кол-ву купленого товар
          $item->items->instock += $item->count;

          // Если товар отмечен как проданный, изменим значение
          if (!$item->items->sold) {
            $item->items->sold = 0;
          }
          // Сохраним
          if (!$item->items->save()) {
            $message = $model->getErrors();
            $message = array_shift($message);
            return ["status"=>0, "text"=>$message[0]];
          }
        }
        // Сохраним изменения базы
        $transaction->commit();

        return ["status"=>1];
      }

      /**
        * Пересчитывает корзину пользователя
        */
      public function recountCart(Cart $cart)
      {
        if (!$cart)
          return ["status"=>0, "text"=>"Нет корзины"];

        if (!count($cart->cartitem))
          return ["status"=>0, "text"=>"Нет товара в корзине"];

        // Обнулим корзину и пересчитаем все заново
        $cart["price_total"] = $cart["price"] = $cart["sale"] = 0;
        foreach ($cart->cartitem as $row) {
          $cart["price"] += $row["price"] * $row["count"];
          $cart["sale"] += $row["price_sale"] * $row["count"];
          $cart["price_total"] += ($row["price"] * $row["count"]) - ($row["price_sale"] * $row["count"]);
        }

        if(!$cart->save()){
          $message = $model->getErrors();
          $message = array_shift($message);
          return ["status"=>0, "text"=>$message[0]];
        }

        return ["status"=>1];
      }

}
