<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_question".
 *
 * @property integer $id_items_question
 * @property string $user_name
 * @property string $user_email
 * @property string $user_ip
 * @property string $text
 * @property integer $id_items
 * @property integer $id_answer
 * @property integer $answer_check
 * @property string $created_at
 * @property string $updated_at
 * @property integer $visible
 */
class Question extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'items_question';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_name', 'text', 'id_items'], 'required'],
      [['text'], 'string'],
      [['id_items', 'id_answer', 'answer_check', 'visible'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['user_name', 'user_email'], 'string', 'max' => 100],
      [['user_ip'], 'string', 'max' => 17],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_items_question' => 'Id Items Question',
      'user_name' => 'User Name',
      'user_email' => 'User Email',
      'user_ip' => 'User Ip',
      'text' => 'Text',
      'id_items' => 'Id Items',
      'id_answer' => 'Id Answer',
      'answer_check' => 'Answer Check',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
      'visible' => 'Visible',
    ];
  }

  /**
    * Критерии поиска для главной таблицы
    */
  public function criteria()
  {
    return Question::find();
  }
}
