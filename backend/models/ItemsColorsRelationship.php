<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_colors_relationship".
 *
 * @property integer $id
 * @property integer $id_items
 * @property integer $id_colors
 * @property integer $visible
 * @property integer $instock
 *
 * @property Colors $idColors
 * @property Items $idItems
 */
class ItemsColorsRelationship extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items_colors_relationship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_items', 'id_colors', 'visible', 'instock'], 'integer'],
            [['id_colors'], 'exist', 'skipOnError' => true, 'targetClass' => Colors::className(), 'targetAttribute' => ['id_colors' => 'id_colors']],
            [['id_items'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['id_items' => 'id_items']],
            [['instock', 'visible'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_items' => 'Id Items',
            'id_colors' => 'Id Colors',
            'visible' => 'Visible',
            'instock' => 'Instock',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColors()
    {
        return $this->hasOne(Colors::className(), ['id_colors' => 'id_colors']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasOne(Items::className(), ['id_items' => 'id_items']);
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return ItemsColorsRelationship::find()->with('colors', 'items');
    }
}
