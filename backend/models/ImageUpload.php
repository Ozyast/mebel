<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/*
 * Загрузка изображений на сервер
 */
class ImageUpload
{

   /**
     * Загружает изображение
     * $w, $h = Входные параметры к которым будет подгоняться изображение
     *        ширина и высота, соответсвенно
     * $src = путь до файла
     * $store_dir = куда сохрать файл (img/dress/)
     * $image = Задать имя для файла
     * $answer = выводить данные о добавленном файле
     * $answer = HelperPlugin::upload($_FILES['file']['tmp_name'], 'image/tmp/', 3000, 3000);
     */
    public static function upload($src, $store_dir, $w = 3000, $h = 3000, $image=0, $answer=1) {
      /* ============================ Получаем данные с форм =============================== */

      if(!strlen($src)){
         return array("status"=>0, "text"=>"Изображение не найдено.", "code"=>"ImageUpload::upload");
      }

      $size_img = getimagesize($src);

      /* ============================Переименовываем и сохраняем картинку=============================== */
//      $store_dir = "img/dress/";  # Дерриктория картинки по категориям

      if(!$image)
         $image = uniqid("img", False).".jpg";     # Генерируем название для картинки ( img********** )

      if (($size_img[0] > $w) || ($size_img[1] > $h)) {  # Проверяем больше изображение или нет если да то сжимаем
         # определим коэффициент сжатия изображения, которое будем генерить
         $ratio = $w / $h;
         # получим размеры исходного изображения
         $size_img = getimagesize($src);
         # получим коэффициент сжатия исходного изображения
         $src_ratio = $size_img[0] / $size_img[1];
         # здесь вычисляем размеры, чтобы при масштабировании сохранились
         # 1. Пропорции исходного изображения
         # 2. Исходное изображение полностью помещалось на маленькой копии
         # (не обрезалось)
         if ($src_ratio > $ratio) {
            $old_h = $size_img[1];
            $size_img[1] = floor($size_img[0] / $ratio);
            $new_h = floor($old_h * $h / $size_img[1]);
            # создадим пустое изображение по заданным размерам
            $dest_img = imagecreatetruecolor($w, $new_h);
         } else {
            $old_w = $size_img[0];
            $size_img[0] = floor($size_img[1] * $ratio);
            $new_w = floor($old_w * $w / $size_img[0]);
            # создадим пустое изображение по заданным размерам
            $dest_img = imagecreatetruecolor($new_w, $h);
         }

         # зальём его белым цветом
         imagefill($dest_img, 0, 0, 0xc4c8ca);

         # выбираем функцию создания
         # создаем jpeg из файла
         switch ($size_img['mime']) {
            case 'image/jpeg':
               $src_img = imagecreatefromjpeg($src);
               break;
            case 'image/png':
               $src_img = imagecreatefrompng($src);
               break;
         }

         # масштабируем изображение    функцией imagecopyresampled()
         # $dest_img - уменьшенная копия
         # $src_img - исходной изображение
         # $w - ширина уменьшенной копии
         # $h - высота уменьшенной копии
         # $size_img[0] - ширина исходного изображения
         # $size_img[1] - высота исходного изображения
         imagecopyresampled($dest_img, $src_img, 0, 0, 0, 0, $w, $h, $size_img[0], $size_img[1]);

         # сохраняем в файл .jpg
         switch ($size_img['mime']) {
            case 'image/jpeg':
               imagejpeg($dest_img, $store_dir.$image);
               break;
            case 'image/pjpeg':
               imagejpeg($dest_img, $store_dir.$image);
               break;
            case 'image/png':
               imagepng($dest_img, $store_dir.$image);
               break;
            case 'image/x-png':
               imagepng($dest_img, $store_dir.$image);
               break;
         }
         imagejpeg($dest_img, $store_dir.$image);

         # чистим память от созданных изображений
         imagedestroy($dest_img);
         imagedestroy($src_img);
      } else {
         if (is_uploaded_file($src)) {
            move_uploaded_file($src, $store_dir . $image);   # Загружаем файл
         } else {
//            throw new CHttpException(400, 'Ошибка загрузки файла');
            return array("status"=>0, "text"=>"Ошибка загрузки файла.", "code"=>"ImageUpload::upload");
         }
      }

      // если стоит флаг не отвечать
      if(!$answer)
         return 1;

      return array("status" => 1, "name" => $image);
   }

   /**
    * Копирует файл $src в каталог $store_dir
    *
    * @param type $src - Путь до файла оригинала
    * @param type $store_dir - Путь куда копировать файл
    * @param type $store_dir_create - указывать деррикторию создания, если ее нужно создать
    * $answer = HelperPlugin::download("image/tmp/img5a40ef9a0dac0.jpg", "image/photo/1/img5a40ef9a0dac0.jpg", "image/photo/1/");
    */
   public static function download($src, $store_dir, $store_dir_create=0) {
     // Если необходимо создать дерикторию, то создадим ее
     if(($store_dir_create) && !is_dir($store_dir_create)){
       if(!@mkdir($store_dir_create, 0777)){
         return array("status" => 0, "text" => "Не удалось создать деррикторию", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
       }
     }

    // Копируем файл в каталог
    if (!@copy($src, $store_dir)) {
      // Проверим, существует ли каталог, куда мы собираемся копировать файл
      if ($store_dir_create && !is_dir($store_dir_create)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: указанная папка не найдена ($store_dir)", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      // Проверим файл который надо скопировать
      } elseif (!file_get_contents($src)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: файл изображения не найден", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      } elseif (!is_file($store_dir)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: не является файлом", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      } else{
        return array("status" => 0, "text" => "Не удалось скопировать файл", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      }
    } else {
      return array("status" => 1);
    }
  }


   /**
    * Сохраняем бинарный файл полученный с помощью file_get_contents
    *
    * @param type $src - Путь до файла оригинала
    * @param type $store_dir - Путь куда копировать файл
    * @param type $store_dir_create - указывать деррикторию создания, если ее нужно создать
    * $answer = HelperPlugin::download("image/tmp/img5a40ef9a0dac0.jpg", "image/photo/1/img5a40ef9a0dac0.jpg", "image/photo/1/");
    */
   public static function saveBinFile($fileBin, $store_dir, $store_dir_create=0) {
     // Если необходимо создать дерикторию, то создадим ее
     if(($store_dir_create) && !is_dir($store_dir_create)){
       if(!@mkdir($store_dir_create, 0777)){
         return array("status" => 0, "text" => "Не удалось создать деррикторию", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
       }
     }

    // запишем наш бинарник в файл
    if (!@file_put_contents($store_dir, $fileBin)) {
      // Проверим, существует ли каталог, куда мы собираемся копировать файл
      if ($store_dir_create && !is_dir($store_dir_create)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: указанная папка не найдена ($store_dir)", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      // Проверим файл который надо скопировать
    } elseif (!strlen($fileBin)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: файл изображения не найден", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      } else{
        return array("status" => 0, "text" => "Не удалось скопировать файл", "code" => __FILE__.":".__LINE__." (".__METHOD__.")");
      }
    } else {
      return array("status" => 1);
    }
  }


  /**
   * Переносит файл $src в каталог $store_dir
   *
   * @param type $src - Путь до файла оригинала
   * @param type $store_dir - Путь куда копировать файл
   * @param type $store_dir_create - указывать деррикторию создания, если ее нужно создать
   * $answer = HelperPlugin::remove("image/tmp/img5a40ef9a0dac0.jpg", "image/photo/1/img5a40ef9a0dac0.jpg", "image/photo/1/");
   */
  public static function move($src, $store_dir, $store_dir_create=0)
  {
    // Если необходимо создать дерикторию, то создадим ее
    if(($store_dir_create) && !is_dir($store_dir)){
      @mkdir($store_dir_create, 0777);
    }

    // Копируем файл в каталог
    if (!@rename($src, $store_dir)) {

      // Проверим, существует ли каталог, куда мы собираемся копировать файл
      if (!is_dir($store_dir)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: указанная папка не найдена", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        // Проверим файл который надо скопировать

      } elseif (!file_get_contents($src)) {
        return array("status" => 0, "text" => "Не удалось скопировать файл: файл изображения не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");

      } else {
        return array("status" => 0, "text" => "Не удалось скопировать файл", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }
    } else
      return array("status" => 1);
  }


  public static function remove($src, $dir = 0)
  {
    // Проверим, что нужно удалить каталог или файл
    if ($dir) {
      if (!is_dir($src)){
        return array("status" => 0, "text" => "Ошибка удаления: файл не является каталогом", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }
      // Удаляем каталог
      $result = rmdir($src);
    } else {
      if (!@file_get_contents($src)) {
        return array("status" => 0, "text" => "Ошибка удаления: файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }
      // удаляем файл
      $result = unlink($src);
    }

    // Проверим на ошибки
    if (!$result) {
      return array("status" => 0, "text" => "Ошибка удаления: Не удалось удалить файл", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
    } else {
      return array("status" => 1);
    }
  }


  function cutToCenter($dir, $image, $w, $h)
  {
    // Узнаем исходный размер изображения
    $size_img = getimagesize($dir.$image);

    if ( ($size_img[0] < $w) && ($size_img[1] < $h))
      return ['status'=>1];
    // Уменьшим изображене до нужного нам размера.
    // Самую маленькую часть подгоним под нужный нам размер
    // Тем самым большая часть будет превышать наш размера
    // Именно ее мы потом обрежем если размер исходный 1800х1200
    // а нам нужен 500х500 значит мы получим $w = 1800, $h = 500
    // После уменьшения получим изображение 750х500
    if($size_img[0] > $size_img[1]){
      // Если ширина больше высоты, уменьшим изображние по ширине
      $w_new = $size_img[0];
      $h_new = $h;
    } else {
      // Если высота больше ширины, уменьшим изображние по высоте
      $w_new = $w;
      $h_new = $size_img[1];
    }

    // Уменьшаем изображения учитывая наши подсчеты.
    $answer = ImageUpload::upload($dir.$image, $dir, $w_new, $h_new, '@'.$image);
    if(!$answer['status'])
      return $answer;

    // Получаем новые размеры
    $size_img = getimagesize($dir.$answer['name']);

    // Проверим какая сторона не помещается в наш квадрат
    // Если это ширина то разделим оставшееся пространство на 2 и получим координату начала окна
    if ($size_img[0] > $size_img[1]) {
      $x = round(($size_img[0] - $w) / 2);
      $y = 0;
    } else {
      $x = 0;
      $y = round(($size_img[1] - $h) / 2);
    }

    // Обрезаем изображение
    $answer = ImageUpload::crop($dir.$answer['name'], $x, $y, $w, $h);
    if(!$answer['status'])
      return $answer;

    return array("status" => 1);
  }

  /*
  $x_o и $y_o - координаты левого верхнего угла выходного изображения на исходном
  $w_o и h_o - ширина и высота выходного изображения
  */
  function crop($image, $x_o, $y_o, $w_o, $h_o)
  {
    if (($x_o < 0) || ($y_o < 0) || ($w_o < 0) || ($h_o < 0)) {
      return array("status" => 0, "text" => "Некорректные входные параметры", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
    }

    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
      $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
      $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
      // Выводим ошибку, если формат изображения недопустимый
      return array("status" => 0, "text" => "Некорректное изображение", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
    }
    if ($x_o + $w_o > $w_i) $w_o = $w_i - $x_o; // Если ширина выходного изображения больше исходного (с учётом x_o), то уменьшаем её
    if ($y_o + $h_o > $h_i) $h_o = $h_i - $y_o; // Если высота выходного изображения больше исходного (с учётом y_o), то уменьшаем её
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
    imagecopy($img_o, $img_i, 0, 0, $x_o, $y_o, $w_o, $h_o); // Переносим часть изображения из исходного в выходное
    $func = 'image'.$ext; // Получаем функция для сохранения результата

    // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
    if (!$func($img_o, $image)) {
      return array("status" => 0, "text" => "Ошибка при создании обрезанного изображения", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
    }

    return array("status" => 1);
  }
}
