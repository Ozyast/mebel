<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "collections".
*
* @property integer $id_collections
* @property string $image
* @property string $name
* @property string $created_at
* @property string $updated_at
* @property integer $visible
*/
class Collections extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'collections';
  }

  /**
    * Папка с фото
    */
  public static function imageDir()
  {
    return Yii::getAlias('@frontend/web').'/img/collections/';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['visible'], 'integer'],
      [['name'], 'string', 'max' => 100],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_collections' => 'Id Collections',
      'name' => 'Name',
      'visible' => 'Visible',
      ];
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Collections::find();
    }

    /**
      * Модули
      * @return \yii\db\ActiveQuery
      */
    public function getModules()
    {
        return $this->hasMany(Modules::className(), ['id_collections' => 'id_collections']);
    }

    /**
      * Модули
      * @return \yii\db\ActiveQuery
      */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['id_collections' => 'id_collections']);
    }

    /**
      * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
      * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
      */
    public function uploadImage($image_name)
    {
      $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
      $dir = $this->imageDir().$this->id_collections."/";

      $answer = ImageUpload::move($image_src, $dir.$image_name, $dir);
      if (!$answer['status']) {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }

      $this->image = $this->id_collections."/".$image_name;

      return ["status" => 1];
    }

    /**
      * Удаляет фото
      * @param $image_name - Имя фото с ID товара в начале (img5bacc5921e140.jpg)
      */
    public function deleteImage()
    {
      // Проверим есть ли фото
      if(!strlen($this->image)){
        return array("status" => 2, "text" => "Фото нет", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }

      // Удалим фото
      $image_src = $this->imageDir().$this->image;
      $answer = ImageUpload::remove($image_src);
      if (!$answer['status']) {
        // Если фото не найдено, ошибку не выводим
        if (!@file_get_contents($image_src)) {
          return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        } else {
          return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
        }
      }

      if ($del_catalog) {
        $dir = $this->imageDir().$this->id_collections;
        $answer = ImageUpload::remove($dir, 1);
        if (!$answer['status']) {
          // Если фото не найдено, ошибку не выводим
          if (!@file_get_contents($dir)) {
            return array("status" => 2, "text" => "Каталог не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
          } else {
            return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
          }
        }
      }

      $this->image = "";

      return ["status" => 1];
    }

    /**
      * Удаляет каталог
      * @param $image_name - Имя фото с ID товара в начале (img5bacc5921e140.jpg)
      */
    public function deleteCatalog()
    {
      $dir = $this->imageDir().$this->id_collections;

      $answer = ImageUpload::remove($dir, 1);
      if (!$answer['status']) {
        // Если фото не найдено, ошибку не выводим
        if (!@file_get_contents($dir)) {
          return array("status" => 2, "text" => "Каталог не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        } else {
          return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
        }
      }

      return ["status" => 1];
    }
}
