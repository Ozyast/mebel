<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id_articles
 * @property string $menu_title
 * @property string $menu_url
 * @property string $file_name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $visible
 */
class Articles extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'articles';
  }

  /**
  * Папка с фото
  */
  public static function imageDir()
  {
    return Yii::getAlias('@frontend/web').'/img/articles/';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['menu_title', 'menu_url'], 'required'],
      [['created_at', 'updated_at'], 'safe'],
      [['visible'], 'integer'],
      [['menu_title'], 'string', 'max' => 250],
      [['menu_url', 'file_name'], 'string', 'max' => 100],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_articles' => 'Id Articles',
      'menu_title' => 'Menu Title',
      'menu_url' => 'Menu Url',
      'file_name' => 'File Name',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
      'visible' => 'Visible',
    ];
  }

  /**
  * Критерии поиска для главной таблицы
  */
  public function criteria()
  {
    return Articles::find()->orderBy("id_articles DESC");
  }

  /**
    * Удаляем фото
    * @param $image_name - Имя фото с ID товара в начале (32/img5bacc5921e140.jpg)
    */
  public static function deleteImage($src)
  {
    // Удалим главное фото
    $image_src = Articles::imageDir().$src;
    $answer = ImageUpload::remove($image_src);
    if (!$answer['status']) {
      // Если фото не найдено, ошибку не выводим
      if (!@file_get_contents($image_src)) {
        return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      } else {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }
    }

    return ["status" => 1];
  }
}
