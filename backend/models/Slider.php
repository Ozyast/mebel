<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "slider".
*
* @property integer $id_slider
* @property string $image
* @property string $title
* @property string $sub_title
* @property string $text
* @property string $button_name
* @property string $button_url
* @property integer $visible
*/
class Slider extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'slider';
  }

  /**
    * Папка с фото
    */
  public static function imageDir()
  {
    return Yii::getAlias('@frontend/web').'/img/slider/';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      // [['title', 'sub_title', 'button_name', 'button_url'], 'required'],
      [['text'], 'string'],
      [['visible'], 'integer'],
      [['image', 'title', 'sub_title', 'button_name', 'button_url'], 'string', 'max' => 100],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_slider' => 'Id Slider',
      'image' => 'Image',
      'title' => 'Title',
      'sub_title' => 'Sub Title',
      'text' => 'Text',
      'button_name' => 'Button Name',
      'button_url' => 'Button Url',
      'visible' => 'Visible',
    ];
  }

  /**
    * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
    * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
    */
  public function uploadImage($image_name)
  {
    $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
    $dir = $this->imageDir();

    $answer = ImageUpload::move($image_src, $dir.$image_name, $dir);
    if (!$answer['status']) {
      return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
    }

    return ["status" => 1, 'src' => $image_name];
  }

  /**
    * Удаляет фото
    * @param $image_name - Имя фото с ID товара в начале (32/img5bacc5921e140.jpg)
    */
  public function deleteImage()
  {
    // Проверим есть ли фото
    if(!strlen($this->image)){
      return array("status" => 2, "text" => "Фото нет", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
    }

    // Удалим фото
    $image_src = $this->imageDir().$this->image;
    $answer = ImageUpload::remove($image_src);
    if (!$answer['status']) {
      // Если фото не найдено, ошибку не выводим
      if (!@file_get_contents($image_src)) {
        return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      } else {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }
    }

    return ["status" => 1];
  }
}
