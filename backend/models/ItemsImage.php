<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "items_image".
*
* @property integer $id_items_image
* @property string $src
* @property integer $id_items
* @property integer $color
* @property string $created_at
* @property string $updated_at
* @property integer $visible
*/
class ItemsImage extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'items_image';
  }

  /**
    * Папка с фото
    */
  public static function imageDir()
  {
    return Yii::getAlias('@frontend/web').'/img/items/';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id_items'], 'required'],
      [['id_items', 'color', 'visible'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['src'], 'string', 'max' => 100],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_items_image' => 'Id Items Image',
      'src' => 'Src',
      'id_items' => 'Id Items',
      'color' => 'Color',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
      'visible' => 'Visible',
    ];
  }

  /**
    * Критерии поиска для главной таблицы
    */
  public function criteria()
  {
    return ItemsImage::find();
  }

  /**
    * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
    * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
    */
  public function uploadImage($image_name)
  {
    $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
    $dir = $this->imageDir().$this->id_items.'/';
    $name = uniqid("img", False).'.jpg';

    $answer = ImageUpload::download($image_src, $dir.$name, $dir);
    if (!$answer['status']) {
      return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
    }

    $answer = ImageUpload::cutToCenter($dir, $name, 500, 500);
    if (!$answer['status']) {
      return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
    }

    return ["status" => 1, 'src' => $this->id_items.'/'.$name];
  }

  /**
    * Удаляет 2 фото
    * @param $image_name - Имя фото с ID товара в начале (32/img5bacc5921e140.jpg)
    */
  public function deleteImage()
  {
    // Удалим главное фото
    $image_src = $this->imageDir().$this->src;
    $answer = ImageUpload::remove($image_src);
    if (!$answer['status']) {
      // Если фото не найдено, ошибку не выводим
      if (!@file_get_contents($image_src)) {
        return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      } else {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }
    }

    // Уддалим уменьшенную копию
    $image_src = $this->imageDir().str_replace("/", "/@", $this->src);
    $answer = ImageUpload::remove($image_src);
    if (!$answer['status']) {
      // Если фото не найдено, ошибку не выводим
      if (!@file_get_contents($image_src)) {
        return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      } else {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }
    }

    return ["status" => 1];
  }
}
