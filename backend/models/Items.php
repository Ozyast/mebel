<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property integer $id_items
 * @property string $title
 * @property string $image
 * @property integer $id_manufacturer
 * @property integer $price
 * @property integer $sale
 * @property integer $sale_price
 * @property integer $id_collections
 * @property string $additions
 * @property string $services
 * @property integer $instock
 * @property integer $total_sold
 * @property string $created_at
 * @property string $updated_at
 * @property integer $visible_catalog
 * @property integer $visible
 *
 * @property ItemsCategoryRelationship[] $itemsCategoryRelationships
 * @property ItemsColorsRelationship[] $itemsColorsRelationships
 * @property ItemsSubcategoryRelationship[] $itemsInteriorRelationships
 * @property ItemsModulesRelationship[] $itemsModulesRelationships
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['id_manufacturer', 'price', 'sale', 'sale_price', 'instock', 'sold', 'visible_catalog', 'visible', 'instock_color', 'id_collections'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 250],
            [['image'], 'string', 'max' => 100],
            [['sale_price', 'price'], 'default', 'value' => 0],
            [['sold', 'instock', 'instock_color'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_items' => 'Id Items',
            'title' => 'Title',
            'image' => 'Image',
            'id_manufacturer' => 'Id Manufacturer',
            'price' => 'Price',
            'sale' => 'Sale',
            'sale_price' => 'Sale Price',
            'id_collections' => 'Коллекция',
            'instock_color' => 'Кол-во товара по цветам',
            'instock' => 'Instock',
            'total_sold' => 'Total Sold',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'visible_catalog' => 'Visible Catalog',
            'visible' => 'Visible',
        ];
    }

    /**
     * КАТЕГОРИИ мебели
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasMany(Category::className(), ['id_category' => 'id_category'])
                ->viaTable('items_category_relationship', ['id_items' => 'id_items']);
    }

    /**
      * ЦВЕТА мебели
      * @return \yii\db\ActiveQuery
      */
    public function getColors()
    {
      return $this->hasMany(Colors::className(), ['id_colors' => 'id_colors'])
              ->viaTable('items_colors_relationship', ['id_items' => 'id_items']);
    }

    /**
      * ИНТЕРЬЕРЫ
      */
    public function getSubcategory()
    {
      return $this->hasMany(Subcategory::className(), ['id_subcategory' => 'id_subcategory'])
              ->viaTable('items_subcategory_relationship', ['id_items' => 'id_items']);
    }

    /**
      * МОДУЛИ
      * @return \yii\db\ActiveQuery
      */
    public function getModules()
    {
      return $this->hasMany(Modules::className(), ['id_modules' => 'id_modules'])
      ->viaTable('items_modules_relationship', ['id_items' => 'id_items']);
    }

    /**
      * КОЛЛЕКЦИЯ
      * @return \yii\db\ActiveQuery
      */
    public function getCollections()
    {
      return $this->hasOne(Collections::className(), ['id_collections' => 'id_collections']);
    }

    /**
      * ПРОИЗВОДИТЕЛЬ
      * @return \yii\db\ActiveQuery
      */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id_manufacturer' => 'id_manufacturer']);
    }

    /**
      * ОПИСАНИЕ
      * @return \yii\db\ActiveQuery
      */
    public function getDescription()
    {
        return $this->hasOne(ItemsDescription::className(), ['id_items' => 'id_items']);
    }

    /**
      * ФОТО
      * @return \yii\db\ActiveQuery
      */
    public function getImages()
    {
        return $this->hasMany(ItemsImage::className(), ['id_items' => 'id_items']);
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Items::find()->orderBy("id_items DESC")->with('category', 'colors', 'subcategory', 'modules', 'manufacturer');
    }


    /**
      * Закрепляет/открепляет связь между таблицами
      * Входные:
      * $data - Массив с id из chosen: [$name] => Array([0] => 1,[1] => 6,[2] => 2)
      */
    public function toggleLink($data, $name, $id_name, $oposite)
    {
      // Проверим есть ли данные для связи от пользователя и от БД
      if(count($data) && count($this->$name)){
        // Пройдем по всем связям из БД
        foreach ($this->$name as $row) {
          // Если связь из БД не найдена в пользовательских данных
          if (!in_array($row[$id_name], $data)){
            // Значит удалим связь
            $this->unlink($name, $row, true);
          }
          // Удалим этот ID из пользовательского массива, если он есть, т.к. мы его уже проверили
          $key = array_search($row[$id_name], $data);
          unset($data[$key]);
        }
      }

      // Если после проверки у нас остались данные, значит это новые связи
      if(is_array($data) && count($data)){
        // Пройдем по пользовательским данным
        foreach ($data as $value) {
          // Добавим новые связи по этим данным
          $this->link($name, $oposite::findOne($value));
        }
      }
    }
}
