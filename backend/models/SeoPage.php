<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seo_page".
 *
 * @property integer $id_page
 * @property string $url
 * @property string $title
 * @property string $description
 * @property integer $visible
 */
class SeoPage extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'seo_page';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['url', 'title'], 'required'],
      [['visible'], 'integer'],
      ['text', 'safe'],
      [['url'], 'string', 'max' => 250],
      [['title', 'description', 'h1'], 'string', 'max' => 220],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_page' => 'Id Page',
      'url' => 'Url',
      'title' => 'Title',
      'description' => 'Description',
      'visible' => 'Visible',
    ];
  }

  /**
    * Критерии поиска для главной таблицы
    */
  public function criteria()
  {
    return SeoPage::find()->orderBy("id_page DESC");
  }

  /**
    * Перебирает полученный УРЛ и убирает лишние параметры
    */
  public static function getSeoPage($page)
  {
    $page = parse_url($page);
    $url = $page['path'];

    // Если есть параметры
    if (isset($page['query'])) {
      $params = explode("&", $page['query']);
      $param = [];
      // переберм параметры и оставим только важные
      foreach ($params as $value) {
        preg_match('/^([^=]*)=(.*)/i', $value, $new);
        if ($new[1] == 'page')
        $param[0] = $new[1].'='.$new[2];

        if ($new[1] == 'id')
        $param[1] = $new[1].'='.$new[2];
      }

      // Если есть параметры сформируем новый УРЛ
      if (!empty($param))
      $url = $url.'?'.implode("&", $param);
    }

    return $url;
  }
}
