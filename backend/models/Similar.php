<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "similar".
 *
 * @property int $id_similar
 * @property int $id_items_master
 * @property int $id_items_slave
 *
 * @property Items $itemsMaster
 * @property Items $itemsSlave
 */
class Similar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'similar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_items_master', 'id_items_slave'], 'integer'],
            [['id_items_master'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['id_items_master' => 'id_items']],
            [['id_items_slave'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['id_items_slave' => 'id_items']],
            ['id_items_master', 'compare', 'compareAttribute' => 'id_items_slave', 'operator' => '!=', 'message' => 'Нельзя сравнивать один и тот же товар'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_similar' => 'Id Similar',
            'id_items_master' => 'Id Items Master',
            'id_items_slave' => 'Id Items Slave',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaster()
    {
        return $this->hasOne(Items::className(), ['id_items' => 'id_items_master']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlave()
    {
        return $this->hasOne(Items::className(), ['id_items' => 'id_items_slave']);
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Similar::find()->with('slave', 'master');
    }

    /**
      * Проверяет есть ли одинаковые связи в таблице
      * Одинаковыми связями счиатются (1,2 = 2,1)
      */
    public function checkSimilarLink($id1, $id2)
    {
      // проверка одинаковых связей
      $similar = (new \yii\db\Query())
            ->from("similar s")
            ->where("(s.id_items_master = :id1 AND s.id_items_slave = :id2) OR (s.id_items_master = :id2 AND s.id_items_slave = :id1)", [":id1" => $id1, ":id2" => $id2])
            ->all();

      if ($similar) {
        return 0;
      }

      return 1;
    }
}
