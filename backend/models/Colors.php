<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "colors".
 *
 * @property integer $id_colors
 * @property string $image
 * @property string $name
 * @property integer $main_color
 */
class Colors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'colors';
    }

    /**
      * Папка с фото
      */
    public static function imageDir()
    {
      return Yii::getAlias('@frontend/web').'/img/colors/';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['main_color'], 'integer'],
            [['main_color'], 'default', 'value' => 0 ],
            [['image', 'name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_colors' => 'Id Colors',
            'image' => 'Image',
            'name' => 'Name',
            'main_color' => 'Main Color',
        ];
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Colors::find();
    }

    /**
      * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
      * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
      */
    public function uploadImage($image_name)
    {
      $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
      $dir = $this->imageDir();

      $answer = ImageUpload::move($image_src, $dir.$image_name, $dir);
      if (!$answer['status']) {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }

      return ["status" => 1, 'src' => $image_name];
    }

    /**
      * Удаляет 2 фото
      * @param $image_name - Имя фото с ID товара в начале (img5bacc5921e140.jpg)
      */
    public function deleteImage()
    {
      // Проверим есть ли фото
      if(!strlen($this->image)){
        return array("status" => 2, "text" => "Фото нет", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }

      // Удалим фото
      $image_src = $this->imageDir().$this->image;
      $answer = ImageUpload::remove($image_src);
      if (!$answer['status']) {
        // Если фото не найдено, ошибку не выводим
        if (!@file_get_contents($image_src)) {
          return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        } else {
          return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
        }
      }

      return ["status" => 1];
    }
}
