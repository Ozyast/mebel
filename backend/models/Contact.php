<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id_contact
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $ip
 * @property integer $reply
 * @property integer $read
 * @property string $created_at
 */
class Contact extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'contact';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['name', 'email', 'text', 'ip'], 'required'],
      [['text'], 'string'],
      [['reply', 'read'], 'integer'],
      [['created_at'], 'safe'],
      [['name', 'email'], 'string', 'max' => 250],
      [['ip'], 'string', 'max' => 15],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_contact' => 'Id Contact',
      'name' => 'Name',
      'email' => 'Email',
      'text' => 'Text',
      'ip' => 'Ip',
      'reply' => 'Reply',
      'read' => 'Read',
      'created_at' => 'Created At',
    ];
  }

  /**
    * Критерии поиска для главной таблицы
    */
  public function criteria()
  {
    return Contact::find();
  }
}
