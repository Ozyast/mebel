<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart_item".
 *
 * @property integer $id_cart_item
 * @property integer $id_items
 * @property integer $id_cart
 * @property string $title
 * @property string $image
 * @property integer $color
 * @property integer $count
 * @property integer $price
 * @property integer $price_sale
 * @property string $created_at
 * @property string $updated_at
 */
class CartItem extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'cart_item';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id_items', 'id_cart', 'title', 'image', 'color', 'count'], 'required'],
      [['id_items', 'id_cart', 'color', 'count', 'price', 'price_sale'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['title'], 'string', 'max' => 250],
      [['image'], 'string', 'max' => 100],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_cart_item' => 'Id Cart Item',
      'id_items' => 'Id Items',
      'id_cart' => 'Id Cart',
      'title' => 'Title',
      'image' => 'Image',
      'color' => 'Color',
      'count' => 'Count',
      'price' => 'Price',
      'price_sale' => 'Price Sale',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
    ];
  }

  /**
  * Товар
  * @return \yii\db\ActiveQuery
  */
  public function getItems()
  {
    return $this->hasOne(Items::className(), ['id_items' => 'id_items']);
  }

  /**
  * ЦВЕТ
  * @return \yii\db\ActiveQuery
  */
  public function getColors()
  {
    return $this->hasOne(Colors::className(), ['id_colors' => 'color']);
  }

  /**
  * ЦВЕТ
  * @return \yii\db\ActiveQuery
  */
  public function getColorsRelationship()
  {
    return $this->hasOne(ItemsColorsRelationship::className(), ['id_colors' => 'color', 'id_items' => 'id_items']);
  }

  /**
  * КОРЗИНА
  * @return \yii\db\ActiveQuery
  */
  public function getCart()
  {
    return $this->hasOne(Cart::className(), ['id_cart' => 'id_cart']);
  }

  /**
    * Критерии поиска для главной таблицы
    */
  public function criteria()
  {
    return CartItem::find();
  }
}
