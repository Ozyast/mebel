<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manufacturer".
 *
 * @property integer $id_manufacturer
 * @property string $image
 * @property string $name
 * @property integer $visible
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    /**
      * Папка с фото
      */
    public static function imageDir()
    {
      return Yii::getAlias('@frontend/web').'/img/manufacturer/';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['visible'], 'integer'],
            [['image', 'name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_manufacturer' => 'Id Manufacturer',
            'image' => 'Image',
            'name' => 'Name',
            'visible' => 'Visible',
        ];
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Manufacturer::find();
    }

    /**
      * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
      * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
      */
    public function uploadImage($image_name)
    {
      $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
      $dir = $this->imageDir();
      $name = Helper::ruEnTranslate($this->name).".png";

      $answer = ImageUpload::move($image_src, $dir.$name, $dir);
      if (!$answer['status']) {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }

      $this->image = $name;

      return ["status" => 1];
    }

    /**
      * Удаляет 2 фото
      * @param $image_name - Имя фото с ID товара в начале (img5bacc5921e140.jpg)
      */
    public function deleteImage()
    {
      // Проверим есть ли фото
      if(!strlen($this->image)){
        return array("status" => 2, "text" => "Фото нет", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }

      // Удалим фото
      $image_src = $this->imageDir().$this->image;
      $answer = ImageUpload::remove($image_src);
      if (!$answer['status']) {
        // Если фото не найдено, ошибку не выводим
        if (!@file_get_contents($image_src)) {
          return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        } else {
          return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
        }
      }

      return ["status" => 1];
    }
}
