<?php

namespace app\models;

use Yii;

class Helper
{

  /**
    * Добавление фильтра к запросу для поиска в главной таблице
    * - row, имя столбца таблицы, по которому осуществлять поиск.
    *     <имя столбца> - поиск в столбе значения 's'. Массив вида ['id_items_master' => 1]
    *     <имя столбца 1>;<имя столбца 2> - поиск одного значения в нескольких столбцах c ИЛИ.
    *                                       формирует массив ['or', ['id_items_master' => 1], ['id_items_slave' => 1]]
    *     <имя столбца 1>,<имя столбца 2> - поиск одного значения в нескольких столбцах c И.
    *                                       формирует массив ['and', ['id_items_master' => 1], ['id_items_slave' => 1]]
    */
  public function setFilter($criteria)
  {
    // ПОИСК
    if (isset($_GET['s']) && isset($_GET['row']) && isset($_GET['t'])) {

      // Проверим сколько столбцов будем использовать для поиска
      // Если в столбце есть ";" значит столбцов несколько и соеденены они опирацией 'ИЛИ'
      if (strpos($_GET['row'], ";")) {
        $rows = explode(";", $_GET['row']);
        $crit = ['or'];
      // Если в столбце есть "," значит столбцов несколько и соеденены они опирацией 'И'
      } elseif (strpos($_GET['row'], ",")) {
        $rows = explode(",", $_GET['row']);
        $crit = ['and'];
      // Иначе столбец всего один
      } else {
        $rows = [$_GET['row']];
        $crit = ['and'];
      }

      // Пройдем по всем столбцам или по обному
      foreach ($rows as $key => $row) {
        // Если поиск числового значения
        if ($_GET['t'] == "i") {
          $value = (int) $_GET['s'];
          $crit[] = [$row => $value];
        // Если поиск строкового значения
        } else {
          $value = $_GET['s'];
          $crit[] = ['LIKE', $row, $value];
        }
      }

      // Применим полученный массив
      $criteria->andWhere($crit);
    }

    return $criteria;
  }

  /**
   * Подготовит данные для постраничной разбивки
   *
   * Входные данные:
   * $count_data - кол-во всех записей.
   * $count_data_str - кол-во записей на страницу.
   * $str - страница на которой сейчас находимся
   */
  public static function pages($count_data, $count_data_str, $str = 1)
  {
//      $controller = new CController('context');

     // Узнаем кол-во страниц (всего)
     $pages['length'] = ceil($count_data / $count_data_str);
     $pages['str'] = $str;
     $pages['count_data'] = $count_data;

     //Определяем URL страницы для доступа к следующим и предыдущим новостям
     $baseUrl = Yii::$app->getRequest();
     // Получим GET параметр (var=val) из http://site.ru/forum/index?var=val
     $getQueryUrl = $baseUrl->getQueryString();
     $getQueryUrl = preg_replace("/&*page=[0-9]+/i", '', $getQueryUrl);
//      $getQueryUrl = preg_replace("/\?*/i", '', $getQueryUrl);

     $pages['url'] = "/admin/".Yii::$app->controller->id . "/" . Yii::$app->controller->action->id . "?page={{page}}";
     //$pages['url'] = Yii::$app->urlManager->createUrl($pages['url']);

     // Если есть GET параметры, запиишем их к URL
     if (strlen($getQueryUrl))
        $pages['url'] = $pages['url'] . $getQueryUrl;

     return $pages;
  }

  /**
    * Переводит русский текст в транслит
    */
  static public function ruEnTranslate($text)
  {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($text, $converter);
  }

  /**
    * Принимает массив из БД
    * Возвращает масссив индексами которого является значение $key
    */
  public static function indexByKey($array, $key)
  {
    $new_array = [];
    foreach ($array as $row) {
      if (!is_array($row)) {
        $new_array[$row->$key] = $row;
      } else {
        $new_array[$row[$key]] = $row;
      }
    }

    return $new_array;
  }


  /**
   * Использование cURL запроса
   * @param string $url - Ссылка
   * @param bool $privat - Приватный заврос через прокси ( 0/1 )
   * @param bool $browser - Имитация брайзера ( 0/1 )
   * @return html | array
   */
  public static function curl($url, $privat = 0, $browser = 0)
  {
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     // Получать ошибки
     curl_setopt($ch,CURLOPT_FAILONERROR,true);

     // proxy
     if ($privat) {
       $proxy_list = [
           "46.16.226.10:8080",
       ];

       $proxy = $proxy_list[rand(0,count($proxy_list))];
       curl_setopt($ch, CURLOPT_PROXY, $proxy);
     }

     // browser
     if ($browser) {
       $uagent = $_SERVER['HTTP_USER_AGENT'];
       $cookie = tmpfile();

       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
       curl_setopt($ch, CURLOPT_HEADER, 0);
       curl_setopt($ch, CURLOPT_USERAGENT, $uagent);


       curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
       curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
     }

     $output = curl_exec($ch);

     if ($output === false) {
       return ["status"=>0, "text"=>"Ошибка при выполнении cURL (".curl_error($ch).") по URL: ".$url];
     }

     curl_close($ch);
     return ["status"=>1, "output"=>$output];
  }

   // Отправляет сообщение в скрипт alert.js
   // public static function send($message, $type)
   // {
   //    $message = CJavaScript::encode(array(
   //                "message" => $message,
   //                "type" => $type,
   //    ));
   //    Yii::app()->clientScript->registerScript(
   //            'appConfig', "var message = " . $message . ";", CClientScript::POS_HEAD);
   // }

   // public static function getTimeAgo($date_time)
   // {
   //    $timeAgo = time() - (int) $date_time;
   //    $timePer = array(
   //        'day' => array(3600 * 24, 'дн.'),
   //        'hour' => array(3600, ''),
   //        'min' => array(60, 'мин.'),
   //        'sek' => array(1, 'сек.'),
   //    );
   //
   //    foreach ($timePer as $type => $tp) {
   //       $tpn = floor($timeAgo / $tp[0]);
   //       if ($tpn) {
   //          switch ($type) {
   //             case 'hour':
   //                if (in_array($tpn, array(1, 21))) {
   //                   $tp[1] = 'час';
   //                } elseif (in_array($tpn, array(2, 3, 4, 22, 23))) {
   //                   $tp[1] = 'часa';
   //                } else {
   //                   $tp[1] = 'часов';
   //                }
   //                break;
   //          }
   //          return $tpn . ' ' . $tp[1] . ' назад';
   //       }
   //    }
   // }

   // Отправляем письмо на почту пользователя
   // public static function sendMail($email, $title, $template, $data = null)
   // {
   //    $from = "Servios <support@servios.ru>";
   //
   //    // Указываем правильный MIME-тип сообщения:
   //    $headers = 'MIME-Version: 1.0' . "\r\n";
   //    $headers .= 'Content-type: text/html;
   //      charset=utf-8' . "\r\n";
   //    // Добавляем необходимые заголовки
   //    $headers .='To: ' . $email . "\r\n";
   //    $headers .='From: ' . $from . "\r\n";
   //
   //    $controller = new CController('context');
   //    $template = $controller->renderPartial($template, array(
   //        'title' => $title,
   //        'data' => $data,
   //            ), true);
   //
   //    $return = mail($email, $title, $template, $headers);
   //    return $return;
   // }


   /*
    * filter
    */
//    static public function html($value)
//    {
//      $value = \yii\helpers\HtmlPurifier::process($value, [
//           'Attr.EnableID' => true,
//       ]);
//
//      return $value;
// //      $newValue = strip_tags($value);
// //
// //      $purifier = new CHtmlPurifier();
// //      $newValue = $purifier->purify($newValue);
// //
// //      return $newValue;
//    }


   /**
    *
    * @param type $answer
    * @param int $return
    * @param type $bd
    * @param int $echo
    * @param type $data
    * @return string
    * Helper::look($model->open_ssh("10.50.70.7"), 1, function($answer, $data){
    *    echo "<pre>";
    *    print_r($data);
    *    echo "</pre>";
    * }, 0, ["model"=>$model]);
    */
  //  static public function look($answer, $echo, $bd, $return, $data = null)
  //  {
  //   if (!$answer["status"]) {
  //     if ($echo && !is_callable($echo)) {
  //       echo json_encode($answer);
  //     } elseif (is_callable($echo)) {
  //       $echo($answer, $data);
  //     }
  //
  //     if ($bd && !is_callable($bd)) {
  //       Status::add($data->id_equipment, $answer['text'], "danger", 1);
  //     } elseif (is_callable($bd)) {
  //       $bd($answer, $data);
  //     }
  //
  //     if ($return && !is_callable($return)) {
  //       if ($answer['status'] == 0)
  //         $answer['status'] = "danger";
  //       elseif ($answer['status'] == 1)
  //         $answer['status'] = "success";
  //
  //       //      if (!strlen($answer['text']) && $answer['status'] == "success")
  //       echo json_encode($answer);
  //       exit(0);
  //     }elseif (is_callable($return)) {
  //       $return($answer, $data);
  //     }
  //   }
  //   return $answer;
  // }


  // Выводим ошибку
   // Helper::wtf(array("status"=>"danger", "text"=>$message[0]), array("echo", "bd", "return"));
//   static public function wtf($wtf, $action){
//
//     if ((is_string($action) && $action == "echo") || (is_array($action) && array_search("echo", $action) !== false)){
//       if (!isset($wtf['status']))
//         $wtf["status"] = "danger";
//
//       echo json_encode($wtf);
//       exit(0);
//     }
//
//     if ((is_string($action) && $action == "bd") || (is_array($action) && array_search("bd", $action) !== false))
//      Error::add($wtf['message'], $wtf['code'], $wtf['text']);
//
//     if ((is_string($action) && $action == "return") || (is_array($action) && array_search("return", $action) !== false))
//      return array("status"=>0, "text"=>$wtf['message'], "code"=>$wtf['code'], "data"=>$wtf['text']);
//   }


   /*
    * Получает данные по таблицы
    *
    * $criteria - Критерии для поиска данных и выборки колличества (массив):
    *  $criteria = array(
    *    'from' => 'parts_base parts',
    *     'join'=> 'LEFT JOIN parts_base_name name ON name.id_parts_name = parts.id_parts_name',
    *     'order' => 'id_parts',
    *     'limit' => "100",
    *  )
    *
    * $page - Страница на которой находиьмся 1, 2 или 3
    */
//    public static function getDataForTable($model, $criteria, $page)
//    {
//       // Обезапасим запрос с помошью CDbCriteria
// //      $crit = new \yii\db\Query();
//
//       // Получим колличество записей на страницу
//       if (strlen($criteria->limit)) {
// //         $crit->limit = $criteria->limit;
//          $criteria->offset = ($criteria->limit * $page - $criteria->limit);
//       }
//
// //      // Узнаем имя таблицы(алиас) для подсчета колличества при поиску
// //        $crit->from($criteria['from']);
// //      $alias = explode(" ",$criteria['from']);
// //      // Зададим критерии наш алиас
// //      $crit->alias = count($alias)>1 ? $alias[1] : $alias[0];
//
//       // Поиск
//       // Если находим в запросе данные для поиска то ищем
//       if (isset($_GET['s']) && isset($_GET['row'])) {
// //         // Если ищем текст подготовим запрос с LIKE и экранированием % %
//          if (isset($_GET['t']) && $_GET['t'] == "s")
//             $criteria->andWhere(["LIKE", $_GET['row'], $_GET['s']]);
// //         // Если ищем ID убирем экраны и установим =
//          else
//             $criteria->andWhere([$_GET['row'] => $_GET['s']]);
//       }
//
//       // Сортировка
//       // Если находим в запросе данные для сортировки то сортируем
//       if (isset($_GET['so']) && isset($_GET['sotype'])) {
//         $criteria->orderBY($_GET['so']." ".$_GET['sotype']);
//       }
//          // Подключаем остальные таблицы для подсчета кол-ва записей в поиске
// //         if (isset($criteria['join']) && is_array($criteria['join']))
// //            $crit->join = implode(" ",$criteria['join']);
// //         elseif (isset($criteria['join']))
// //            $crit->join = $criteria['join'];
//
//
// //         // Запишем поиск в массив для запроса
// //         $criteria['where'] = $crit->condition;
// //         $criteria['params'] = $crit->params;
// //      // Если нет запроса на поиск подготовим запрос для подсчета кол-ва записей
// //      } else {
// //         if (isset($criteria['where']))
// //         $crit->condition = $criteria['where'];
// //      }
//
//       $data['data'] = $criteria->All();
//       // Получим общее количество записей
//       $count_data = $criteria->count();
//       // Подготовим данные для навигации по страницам
//       $data['pages'] = Helper::pages($count_data, $criteria->limit, $page);
//
//       return $data;
//    }

  /**
   * HTML DOM Parser - из html кода выбирает код селектора заданных пользователем
   *
   * @param type $html - HTML страница или код
   * @param type $tag - Какой тег нужно найти: div
   * @param type $attribute - Аттрибут по которому будет осуществляться поиск: class
   * @param type $attribute_name - текст аттрибута, указывать полностью: story
   * @return type array("status"=>1, "html"=>$html, "test"=>$test_html);
   */
//   static public function htmlFind($html, $tag, $attribute, $attribute_name)
//   {
//     $begin_time = time() - 1272000000 + floatval(microtime());
//     // Определим сколько таких селекторов есть в коде
//     $count_tag = substr_count($html, "$attribute=\"$attribute_name\"");
//     $html_return = array();
//
//     for ($i = 0; $i < $count_tag; $i++) {
//       if ($pos!== 0) {
//         $html = substr($html, $pos);
//       }
//
//       // Найдем строку кода нашего селектора
//       $pos1 = strpos($html, "$attribute=\"$attribute_name\"");
//       // Отрежем все что находится после нашего селектора
//       $temp = substr($html, 0, $pos1);
//
//       // находим начало нашего селектора
//       $pos2 = strrpos($temp, "<$tag");
//       // Вырезаем начало страницы по наш селектор
//       $html = substr($html, $pos2);
//
//       // Счетчик кол-во вложенных однотипных тегов в нашем искомом
//       $tags = 0;
//       // Счетчик позиции на котором находится цикл, чтобы при завершении отрезать от html2 по этот счетчик
//       $pos = 1;
//       // Флаг завершения цикла
//       $done = 0;
//       // Переменная с кодом страницы для прохождения по нему циклом
//       $html_temp = $html;
//       // Переменная для тестового вывода всех найденных тегов
//       $test_html = "";
//
//       // Запускаем цикл и по очередно отбрасываем теги, которые нам не подходят или закрывающиеся
//       while ($done == 0) {
//         // Находим следующий открытый однотипный тег
//         $pos1 = strpos($html_temp, "<$tag", 1);
//         // Находим следующий закрывающийся однотипный тег
//         $pos2 = strpos($html_temp, "</$tag>", 1);
//
//         // Если включить режим теста покажет какие теги он отбирает
// //        for ($i = 0; $i < $tags; $i++)
// //          $test_html = $test_html . "&nbsp;&nbsp;&nbsp;";
// //        // Формируем ответ для теста вида: <div class="story__left"> (36 - 190:94)
// //        $test_html = $test_html . htmlspecialchars(substr($html_temp, 0, strpos($html_temp, ">") + 1)) . " (" . $pos1 . " - " . $pos2 . ":" . $pos . ")</br>";
//
//         // Если следующий в коде идет открывающийся однотипный тег
//         if ($pos1 < $pos2) {
//           // пометим что в нашем теге есть еще один подобный тег (чтобы сначала закрыть его)
//           $tags++;
//           // К общей позиции добавим найденный тег
//           $pos = $pos + $pos1;
//           // Обрежем временный код по найденный элемент
//           $html_temp = substr($html_temp, $pos1);
//
//         // Или если следующим идет закрывающийся тег и счетчик кол-ва вложений НЕ равен нулю
//         } elseif (($pos1 > $pos2) && ($tags > 0)) {
//           // Уменьшим счетчик, покажем что один из вложенных подобных тегов закрылся
//           $tags--;
//           // К общей позиции добавим найденный тег
//           $pos = $pos + $pos2;
//           // Обрежем временный код по найденный элемент
//           $html_temp = substr($html_temp, $pos2);
//
//         // Или если следующим идет закрывающийся тег и счетчик кол-ва вложений РАВЕН нулю
//         } elseif (($pos1 > $pos2) && ($tags == 0)) {
//           // К общей позиции добавим найденный тег + сам закрывающийся тег
//           $pos = $pos + $pos2 + 3 + strlen($tag);
//           // Вырезаем из оригинального кода кусок до найденной позиции
//           $html_return[] = substr($html, 0, $pos);
//           // ставим флаг завершения цикла
//           $done = 1;
//         }
//       }
//     }
//
//     $end_time = time() - 1272000000 + floatval(microtime()) - $begin_time;
//     echo "</br></br>Time: $end_time</br>";
//     return array("status"=>1, "html"=>$html_return, "test"=>$test_html);
//   }


//   static public function htmlFind2($html, $tag, $attribute, $attribute_name)
//   {
//     $begin_time = time() - 1272000000 + floatval(microtime());
//     // Определим сколько таких селекторов есть в коде
//     $count_tag = substr_count($html, "$attribute=\"$attribute_name\"");
//     $html_return = array();
//
//     for ($i = 0; $i < $count_tag; $i++) {
//       // Найдем строку кода нашего селектора и если уже был такой селектор то делаем отступ
//       $pos1 = strpos($html, "$attribute=\"$attribute_name\"", $pos);
//       // Отрежем все что находится после нашего селектора
//       $temp = substr($html, 0, $pos1);
//
//       // находим начало нашего селектора
//       $pos2 = strrpos($temp, "<$tag");
//       // Вырезаем начало страницы по наш селектор
//       $html = substr($html, $pos2);
//
//       // Счетчик кол-во вложенных однотипных тегов в нашем искомом
//       $tags = 0;
//       // Счетчик позиции на котором находится цикл, чтобы при завершении отрезать от html2 по этот счетчик
//       $pos = 1;
//       // Флаг завершения цикла
//       $done = 0;
//       // Переменная с кодом страницы для прохождения по нему циклом
//       $html_temp = $html;
//       // Переменная для тестового вывода всех найденных тегов
//       $test_html = "";
//
//       // Запускаем цикл и по очередно отбрасываем теги, которые нам не подходят или закрывающиеся
//       while ($done == 0) {
//         // Находим следующий открытый однотипный тег
//         $pos1 = strpos($html, "<$tag", $pos);
//         // Находим следующий закрывающийся однотипный тег
//         $pos2 = strpos($html, "</$tag>", $pos);
//
// //          // Если включить режим теста покажет какие теги он отбирает
// //          for ($i = 0; $i < $tags; $i++)
// //            $test_html = $test_html . "&nbsp;&nbsp;&nbsp;";
// //          // Формируем ответ для теста вида: <div class="story__left"> (36 - 190:94)
// //          $test_html = $test_html . htmlspecialchars(substr($html, $pos-1, strpos(substr($html, $pos), ">")+2)) . " (" . $pos1 . " - " . $pos2 . ":" . $pos . ") ($tags)</br>";
//
//         // Если следующий в коде идет открывающийся однотипный тег
//         if ($pos1 < $pos2) {
//           // пометим что в нашем теге есть еще один подобный тег (чтобы сначала закрыть его)
//           $tags++;
//           // К общей позиции добавим найденный тег
//           $pos = $pos1+1;
//
//         // Или если следующим идет закрывающийся тег и счетчик кол-ва вложений НЕ равен нулю
//         } elseif (($pos1 > $pos2) && ($tags > 0)) {
//           // Уменьшим счетчик, покажем что один из вложенных подобных тегов закрылся
//           $tags--;
//           // К общей позиции добавим найденный тег
//           $pos = $pos2+1;
//
//         // Или если следующим идет закрывающийся тег и счетчик кол-ва вложений РАВЕН нулю
//         } elseif (($pos1 > $pos2) && ($tags == 0)) {
//           // К общей позиции добавим найденный тег + сам закрывающийся тег
//           $pos = $pos2;
//           // Вырезаем из оригинального кода кусок до найденной позиции
//           $html_return[] = substr($html, 0, $pos);
//           // ставим флаг завершения цикла
//           $done = 1;
//         }
//       }
//     }
//
//     $end_time = time() - 1272000000 + floatval(microtime()) - $begin_time;
//     echo "</br></br>Time: $end_time</br>";
//     return array("status"=>1, "html"=>$html_return, "test"=>$test_html);
//   }
}
