<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_modules_relationship".
 *
 * @property integer $id
 * @property integer $id_items
 * @property integer $id_modules
 * @property integer $visible
 *
 * @property Items $idItems
 * @property Modules $idModules
 */
class ItemsModulesRelationship extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items_modules_relationship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_items', 'id_modules', 'visible'], 'integer'],
            [['id_items'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['id_items' => 'id_items']],
            [['id_modules'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['id_modules' => 'id_modules']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_items' => 'Id Items',
            'id_modules' => 'Id Modules',
            'visible' => 'Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasOne(Items::className(), ['id_items' => 'id_items']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasOne(Modules::className(), ['id_modules' => 'id_modules']);
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return ItemsModulesRelationship::find()->with();
    }

    /**
      * Добавление новой связи
      */
    public function add($data)
    {
      $this->attributes = $data;

      // Проверяем правильно ли введены данные
      if (!$this->validate()) {
        $message = $this->getErrors();
        $message = array_shift($message);
        return ["status"=>0, "text"=>$message[0]];
      }

      // заливаем фото в нужную папку
      if (strlen($this->image)) {
        $answer = $this->uploadImage($this->image);
        if (!$answer['status'])
          return ["status" => 0, "text" => $answer['text'], "code" => $answer['code']];
      }

      if (!$this->save()) {
        $message = $this->getErrors();
        $message = array_shift($message);
        return ["status"=>0, "text"=>$message[0]];
      }

      return ['status'=>1];
    }
}
