<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modules".
 *
 * @property integer $id_modules
 * @property string $name
 * @property string $characteristics
 * @property string $image
 * @property integer $price
 * @property integer $id_collections
 * @property string $created_at
 * @property string $updated_at
 */
class Modules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules';
    }

    /**
      * Папка с фото
      */
    public static function imageDir()
    {
      return Yii::getAlias('@frontend/web').'/img/collections/';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'characteristics', 'price', 'id_collections'], 'required'],
            [['price', 'id_collections'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 200],
            [['characteristics'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_modules' => 'Id Modules',
            'name' => 'Имя',
            'characteristics' => 'Характеристики',
            'image' => 'Фото',
            'price' => 'Цена',
            'id_collections' => 'Id Коллекции',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
      * Модули
      * @return \yii\db\ActiveQuery
      */
    public function getCollections()
    {
        return $this->hasMany(Collections::className(), ['id_collections' => 'id_collections']);
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Modules::find()->with();
    }

    /**
      * Добавление нового модуля
      */
    public function add($data)
    {
      $this->attributes = $data;

      // Проверяем правильно ли введены данные
      if (!$this->validate()) {
        $message = $this->getErrors();
        $message = array_shift($message);
        return ["status"=>0, "text"=>$message[0]];
      }

      // заливаем фото в нужную папку
      if (strlen($this->image)) {
        $answer = $this->uploadImage($this->image);
        if (!$answer['status'])
          return ["status" => 0, "text" => $answer['text'], "code" => $answer['code']];
      }

      if (!$this->save()) {
        $message = $this->getErrors();
        $message = array_shift($message);
        return ["status"=>0, "text"=>$message[0]];
      }

      return ['status'=>1];
    }

    /**
      * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
      * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
      */
    public function uploadImage($image_name)
    {
      $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
      $dir = $this->imageDir().$this->id_collections."/";

      // Проверим переданна ссылка с другого сайта или нашего
      if (!parse_url($image_name)['host']) {
        $answer = ImageUpload::move($image_src, $dir.$image_name, $dir);
      } else {
        $answer = Helper::curl($image_name);
        $imageBinFile = $answer['output'];

        $image_src = $this->image;
        $image_name = uniqid("img", False).".jpg";
        $answer = ImageUpload::saveBinFile($imageBinFile, $dir.$image_name, $dir);
      }

      if (!$answer['status']) {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }

      $this->image = $this->id_collections."/".$image_name;

      return ["status" => 1];
    }


    /**
      * Переносит фото из папки при изменении id товара
      * @param $image_name - Имя фото из БД (22/img5bacc5921e140.jpg)
      */
    public function moveImage($image_src)
    {
      $image_name = $image_src;
      $image_src = Yii::getAlias('@frontend/web')."/img/modules/".$image_src;
      $image_name = substr($image_name, strpos($image_name, "/")+1);

      $dir = $this->imageDir().$this->id_collections."/";

      $answer = ImageUpload::move($image_src, $dir.$image_name, $dir);
      if (!$answer['status']) {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }

      $this->image = $this->id_collections."/".$image_name;

      return ["status" => 1];
    }

    /**
      * Удаляет 2 фото
      * @param $image_name - Имя фото с ID товара в начале (img5bacc5921e140.jpg)
      */
    public function deleteImage()
    {
      // Проверим есть ли фото
      if(!strlen($this->image)){
        return array("status" => 2, "text" => "Фото нет", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }

      // Удалим фото
      $image_src = $this->imageDir().$this->image;
      $answer = ImageUpload::remove($image_src);
      if (!$answer['status']) {
        // Если фото не найдено, ошибку не выводим
        if (!@file_get_contents($image_src)) {
          return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        } else {
          return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
        }
      }

      return ["status" => 1];
    }
}
