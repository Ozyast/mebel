<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sale".
 *
 * @property integer $id_sale
 * @property string $image
 * @property string $title
 * @property string $sub_title
 * @property string $text
 * @property string $button_p_name
 * @property string $button_p_url
 * @property string $button_s_name
 * @property string $button_s_url
 * @property integer $visible
 */
class Sale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sale';
    }

    /**
      * Папка с фото
      */
    public static function imageDir()
    {
      return Yii::getAlias('@frontend/web').'/img/sale/';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'sub_title', 'button_p_name', 'button_p_url', 'button_s_name', 'button_s_url'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['text'], 'string'],
            [['visible', 'done'], 'integer'],
            [['image', 'title', 'sub_title', 'button_p_name', 'button_p_url', 'button_s_name', 'button_s_url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_sale' => 'Id Sale',
            'image' => 'Image',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'text' => 'Text',
            'button_p_name' => 'Button P Name',
            'button_p_url' => 'Button P Url',
            'button_s_name' => 'Button S Name',
            'button_s_url' => 'Button S Url',
            'visible' => 'Visible',
        ];
    }

    /**
      * Критерии поиска для главной таблицы
      */
    public function criteria()
    {
      return Sale::find();
    }

    /**
      * Переносит фото из временной папки и делает его обрезанную квадратную копию 500х500
      * @param $image_name - Имя фото из темпа (img5bacc5921e140.jpg)
      */
    public function uploadImage($image_name)
    {
      $image_src = Yii::getAlias('@frontend/web')."/img/temp/".$image_name;
      $dir = $this->imageDir();

      $answer = ImageUpload::move($image_src, $dir.$image_name, $dir);
      if (!$answer['status']) {
        return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
      }

      return ["status" => 1, 'src' => $image_name];
    }

    /**
      * Удаляет 2 фото
      * @param $image_name - Имя фото с ID товара в начале (32/img5bacc5921e140.jpg)
      */
    public function deleteImage()
    {
      // Проверим есть ли фото
      if(!strlen($this->image)){
        return array("status" => 2, "text" => "Фото нет", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
      }

      // Удалим фото
      $image_src = $this->imageDir().$this->image;
      $answer = ImageUpload::remove($image_src);
      if (!$answer['status']) {
        // Если фото не найдено, ошибку не выводим
        if (!@file_get_contents($image_src)) {
          return array("status" => 2, "text" => "Файл не найден", "code" => __FILE__ . ":" . __LINE__ . " (" . __METHOD__ . ")");
        } else {
          return ["status" => 0, "text" => $answer['text'], "code" => __FILE__.":".__LINE__." (".__METHOD__.")"];
        }
      }

      return ["status" => 1];
    }
}
