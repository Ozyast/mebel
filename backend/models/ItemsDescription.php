<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items_description".
 *
 * @property integer $id_items_description
 * @property string $description
 * @property integer $id_items
 */
class ItemsDescription extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return 'items_description';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['description'], 'string'],
      [['id_items'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id_items_description' => 'Id Items Description',
      'description' => 'Description',
      'id_items' => 'Id Items',
    ];
  }

  /**
    * Добавляет новое описание к товару
    * Принимает: Идентификатор товара и описание (в формате ключ:значение)
    * Выводит: массив информацией о статусе выполнения и сообщение об ошибки если оно есть
    */
  public function add($id_items, $description)
  {
    if (!strlen($description) || !$id_items)
      return ["status"=>0, "text"=>"Переданны не все обязательные параметры", 'description'=>$description, 'id_items'=>$id_items];

    $model = ItemsDescription::findOne(['id_items'=>$id_items]);
    if(!$model) {
      $model = new ItemsDescription;
      $model->id_items = $id_items;
    }

    $model->description = ItemsDescription::encodeJSON($description);
    if (!$model->save()) {
      $message = $model->getErrors();
      $message = array_shift($message);
      return ['status'=>0, 'text'=>$message[0]];
    }

    return ['status'=>1];
  }

  public function encodeJSON($description){
    // preg_match_all("/(.*)$/i", $description, $desc);
    $description_new = [];
    $description = explode("\n", $description);
    foreach ($description as $value) {
      preg_match_all("/([^:]*):(.*)/i", $value, $row);

      if (strlen($row[1][0]) && strlen($row[2][0])){
        $key = trim($row[1][0]);
        $description_new[$key] = trim($row[2][0]);

        // Если текст заглючен в кавычки, уберем их
        if (preg_match("/^\"(.*)\"$/i", $description_new[$key], $row)) {
          $description_new[$key] = trim($row[1]);
        }
      }
    }

    return json_encode($description_new);
  }
}
