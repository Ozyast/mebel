<?php

namespace backend\controllers;

use Yii;
use app\models\Items;
use app\models\Modules;
use app\models\ItemsModulesRelationship;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* FurnituresController implements the CRUD actions for Furnitures model.
*/
class ModulesrelationController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Furnitures models.
  * @return mixed
  */
  public function actionIndex($id, $page = 1)
  {
    $criteria = ItemsModulesRelationship::criteria()->where(['id_items'=>$id]);
    $criteria = Helper::setFilter($criteria);
    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    $items = Items::findOne($id);

    return $this->render('index', [
      'id_items' => $id,
      'table_data' => $data,
      'pages' => $pages,
      'items' => $items,
    ]);
  }

  /**
  * Добавление новой записи
  */
  public function actionAdd()
  {
    $model = new ItemsModulesRelationship;
    $model->id_items = $_POST['value']['id_items'];
    $items = Items::findOne($model->id_items);

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/modulesrelation/_view.php', [
        'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    $modules = (new \yii\db\Query())
            ->from("modules")
            ->where(["id_collections" => $items->id_collections])
            ->orderBy("name ASC")
            ->All();

    return $this->renderFile('@app/views/modulesrelation/_form.php', [
      'form' => $model,
      'modules' => $modules,
    ]);
  }

  /**
  *  Редактирование записи
  */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $items = Items::findOne($model->id_items);

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];

      // Проверяем правильно ли введены данные
      if (!$model->validate()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
      }

      // Если кол-во изменилось изменим и в главном товаре
      if (($model->oldAttributes['instock'] != $model->attributes['instock'])) {
        $model->items->instock = $model->items->instock - ($model->oldAttributes['instock'] - $model->attributes['instock']);

        if (!$model->items->save()) {
          $message = $model->items->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '"}';
        }
      }

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);

        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/modulesrelation/_view.php', [
        'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    $modules = (new \yii\db\Query())
            ->from("modules")
            ->where(["id_collections" => $items->id_collections])
            ->orderBy("name ASC")
            ->All();

    return $this->renderFile('@app/views/modulesrelation/_form.php', [
      'form' => $model,
      'modules' => $modules,
    ]);
  }


  /**
  * Добавление массива с новыми записями
  */
  public function actionAddall($id_items)
  {
    $text = "";
    $items = Items::findOne($id_items);
    if (!$items)
      throw new NotFoundHttpException('The requested page does not exist.');

    // Если получили данные для добавления
    if (empty($_POST['data']) || !is_array($_POST['data'])) {
      return '{"status":"danger", "text":"Нет данных для добавления"}';
    }

    foreach ($_POST['data'] as $row) {
      $modules = Modules::find()
        ->where([
          "id_collections" => $items['id_collections'],
          "name" => $row['name'],
        ])
        ->one();
      if (!$modules) {
        $text .= $row['name'].": Не найден в базе модулей<br/>";
        continue;
      }


      $items->link('modules', $modules);
      // if (!$answer['status'])
      //   $text .= $row['name'].": Ошибка добавления (".$answer[text].")<br/>";
    }

    // Подготовим вывод данных
    $data['status'] = "info";
    $data['text'] = strlen($text) ? $text : "Готово";
    $data['html'] = $this->renderFile('@app/views/modules/_view.php', [
      'table_data' => [$model],
    ]);
    return json_encode($data);
  }

  /**
  *  Удалить запись
  */
  public function actionDelete($id)
  {
    $model = $this->findModel($id);

    // Удалим запись
    $model->delete();

    return '{"status":"success", "text":"Запись удалена!"}';
  }

  /**
  *
  */
  protected function findModel($id)
  {
    if (($model = ItemsModulesRelationship::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
