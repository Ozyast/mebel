<?php

namespace backend\controllers;

use Yii;
use app\models\Similar;
use app\models\ImageUpload;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* ModulesController implements the CRUD actions for Modules model.
*/
class SimilarController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Modules models.
  * @return mixed
  */
  public function actionIndex($page = 1)
  {
    $criteria = Similar::criteria();
    $criteria = Helper::setFilter($criteria);
    // $criteria = $criteria->andWhere( ['and', ['or', [['id_items_master'=>1], ['id_items_slave'=>1]]]] );
    // $criteria->andWhere(['or', ['id_items_master' => 1], ['id_items_slave' => 1]]);
    // $criteria->orWhere(['id_items_slave' => 1,'id_items_slave' => 1]);

    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

        // echo "<pre>";
        // print_r($criteria);
        // print_r($data);
        // echo "</pre>";
        // return;

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    return $this->render('index', [
      'table_data' => $data,
      'pages' => $pages,
      ]);
    }


    /**
    * Добавление новой записи
    */
    public function actionAdd()
    {
      $model = new Similar;

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        if (!$model->validate()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
        }

        // проверка одинаковых связей
        if (!Similar::checkSimilarLink($model->id_items_master, $model->id_items_slave)) {
          return '{"status":"warning","text":"Связь этих товаров уже есть"}';
        }

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/similar/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/similar/_form.php', [
      'form' => $model,
      ]);
    }

    /**
    *  Редактирование записи
    */
    public function actionUpdate($id)
    {
      $model = $this->findModel($id);

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        // проверка одинаковых связей
        if (!Similar::checkSimilarLink($model->id_items_master, $model->id_items_slave)) {
          return '{"status":"warning","text":"Связь этих товаров уже есть"}';
        }

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/similar/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/similar/_form.php', [
      'form' => $model,
      'collections' => $collections,
      ]);
    }


    /**
    *  Удалить запись
    */
    public function actionDelete($id)
    {
      $model = $this->findModel($id);

      // Удалим запись
      $model->delete();

      return '{"status":"success", "text":"Запись удалена!"}';
    }

    /**
    * Finds the Modules model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Modules the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
      if (($model = Similar::findOne($id)) !== null) {
        return $model;
      } else {
        throw new NotFoundHttpException('The requested page does not exist.');
      }
    }
  }
