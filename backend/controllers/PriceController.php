<?php

namespace backend\controllers;

use Yii;
use app\models\Modules;
use app\models\Items;
use app\models\ItemsModulesRelationship;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FurnituresController implements the CRUD actions for Furnitures model.
 */
class PriceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Furnitures models.
     * @return mixed
     */
    public function actionIndex()
    {
      $url = Yii::getAlias('@backend/views').'/price/lerom.csv';
      $file = file_get_contents($url);

      return $this->render('index', [
        'table_data' => $data,
        'file' => $file,
      ]);
    }

    /**
      * Добавить новый файл
      */
    public function actionFile($update = 0)
    {
      // Если получили данные для добавления
      if ($update) {
        $url = Yii::getAlias('@backend/views').'/price/lerom.csv';

        if (!copy($_FILES['file']['tmp_name'], $url))
          return '{"status":"danger","text":"Не удалось скопировать файл"}';

        return '{"status":"success","text":"Файл изменен"}';
      }

      return $this->render('file', [
        'table_data' => $data,
        'file' => $file,
      ]);
    }

    /**
    * Добавление новой записи
    */
    public function actionAdd()
    {
      if (!strlen($_POST['data'][0]))
        return '{"status":"danger","text":"Неверное имя товара"}';

      $model = Modules::find()->where(['regexp', 'name', ' ?'.$_POST['data'][0].'($| .*$)'])->all();
      if (!$model)
        return '{"status":"danger","text":"Товар не найден в БД"}';

      $data['description'] = "";

      foreach ($model as $key => $row) {
        $price = (int) (str_replace(" ", "", $_POST['data'][1]));
        if (!$price) {
          $data['description'] .= "id: ".$row->id_modules." Ошибка: ( Неверная цена )<br/>";
          continue;
        }

        if ($price == $row->price) {
          $data['description'] .= "id: ".$row->id_modules." ( Цена одинаковая )<br/>";
          continue;
        }

        $oldPrice = $row->price;
        $row->price = $price;
        // Сохраним наши данные
        if (!$row->save()) {
          $message = $row->getErrors();
          $message = array_shift($message);
          $data['description'] .= "id: ".$row->id_modules." Ошибка: ( ".$message[0]." )<br/>";
          continue;
        }

        $data['status'] = "success";
        $data['text'] = "Готово (name: ".$row->name.")";
        $data['description'] .= "id: ".$row->id_modules." (Цена: ".$row->price." (".$oldPrice."), name: ".$row->name.")<br/>";
      }

      return json_encode($data);
    }

    /**
    * Пересчет цены товара
    */
    public function actionRecount($id_items)
    {
      $items = Items::findOne($id_items);
      if (!$items)
        throw new NotFoundHttpException('The requested page does not exist.');

      if (!count($items->modules))
        return '{"status":"warning","text":"Нет модулей для товара"}';

      $price = 0;

      // Получим все связи, если напрямую делать то перезаписывает одинаковые модули
      $relation = ItemsModulesRelationship::find()
          ->where(['id_items' => $id_items])
          ->with('modules')
          ->all();

      foreach ($relation as $key => $row) {
        $price += $row->modules->price;
      }

      if ($items->price === $price)
        return '{"status":"info","text":"Цены одинаковые"}';

      $items->price = $price;
      if (!$items->save()) {
        $message = $items->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/items/_view.php', [
          'table_data' => [$items],
      ]);

      return json_encode($data);
    }

    /**
    *  Редактирование записи
    */
    // public function actionUpdate($id)
    // {
    //   $model = $this->findModel($id);
    //
    //   // Если получили данные для добавления
    //   if (isset($_POST['data'])) {
    //     $model->attributes = $_POST['data'];
    //
    //     // Проверяем правильно ли введены данные
    //     if (!$model->validate()) {
    //       $message = $model->getErrors();
    //       $message = array_shift($message);
    //       return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
    //     }
    //
    //     // заливаем фото в нужную папку
    //     if(strlen($_POST['data']['image'])){
    //       $answer = $model->uploadImage($model->image);
    //       if (!$answer['status']) {
    //         return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
    //       }
    //     }
    //
    //     if (!$model->save()) {
    //       $message = $model->getErrors();
    //       $message = array_shift($message);
    //
    //       return '{"status":"danger","text":"' . $message[0] . '"}';
    //     }
    //
    //     // Подготовим вывод данных
    //     $data['status'] = "success";
    //     $data['html'] = $this->renderFile('@app/views/slider/_view.php', [
    //     'table_data' => [$model],
    //     ]);
    //
    //     return json_encode($data);
    //   }
    //
    //   return $this->renderFile('@app/views/slider/_form.php', [
    //   'form' => $model,
    //   ]);
    // }
    //
    // /**
    // *  Удалить фото
    // */
    // public function actionImageremove($id)
    // {
    //   $model = $this->findModel($id);
    //
    //   // Удалим фото
    //   $answer = $model->deleteImage();
    //   if (!$answer['status']) {
    //     return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
    //   }
    //
    //   $model->image = "";
    //   if (!$model->save()) {
    //     $message = $model->getErrors();
    //     $message = array_shift($message);
    //     return '{"status":"danger","text":"' . $message[0] . '"}';
    //   }
    //
    //   // Подготовим вывод данных
    //   $data['status'] = "success";
    //   $data['html'] = $this->renderFile('@app/views/slider/_view.php', [
    //   'table_data' => [$model],
    //   ]);
    //
    //   return json_encode($data);
    // }
    //
    // /**
    // *  Удалить запись
    // */
    // public function actionDelete($id)
    // {
    //   $model = $this->findModel($id);
    //
    //   // Удалим фото
    //   $answer = $model->deleteImage();
    //   if (!$answer['status']) {
    //     return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
    //   }
    //
    //   // Удалим запись
    //   $model->delete();
    //
    //   return '{"status":"success", "text":"Запись удалена!"}';
    // }

    /**
     *
     */
    // protected function findModel($id)
    // {
    //     if (($model = Slider::findOne($id)) !== null) {
    //         return $model;
    //     } else {
    //         throw new NotFoundHttpException('The requested page does not exist.');
    //     }
    // }
}
