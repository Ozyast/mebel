<?php

namespace backend\controllers;

use Yii;
use app\models\CartItem;
use app\models\Cart;
use app\models\Items;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemsController implements the CRUD actions for Items model.
 */
class CartitemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Items models.
     * @return mixed
     */
    public function actionIndex($page = 1)
    {
      $criteria = CartItem::criteria();
      $criteria = Helper::setFilter($criteria);
      $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
      $data = $criteria->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();

      $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

      return $this->render('index', [
        'table_data' => $data,
        'pages' => $pages,
      ]);
    }

    /**
    * Добавление новой записи
    */
    // public function actionAdd()
    // {
    //   $model = new CartItem;
    //
    //   // Если получили данные для добавления
    //   if (isset($_POST['data'])) {
    //     $model->attributes = $_POST['data'];
    //
    //     // Проверяем правильно ли введены данные
    //     if (!$model->validate()) {
    //       $message = $model->getErrors();
    //       $message = array_shift($message);
    //       return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
    //     }
    //
    //     // заливаем фото в нужную папку
    //     if(strlen($model->image)){
    //       $answer = $model->uploadImage($model->image);
    //       if (!$answer['status']) {
    //         return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
    //       }
    //     }
    //
    //     // Сохраним наши данные
    //     if (!$model->save()) {
    //       $message = $model->getErrors();
    //       $message = array_shift($message);
    //       return '{"status":"danger","text":"' . $message[0] . '"}';
    //     }
    //
    //     // Подготовим вывод данных
    //     $data['status'] = "success";
    //     $data['html'] = $this->renderFile('@app/views/cartitem/_view.php', [
    //     'table_data' => [$model],
    //     ]);
    //
    //     return json_encode($data);
    //   }
    //
    //   return $this->renderFile('@app/views/cartitem/_form.php', [
    //   'form' => $model,
    //   ]);
    // }

    /**
    *  Редактирование записи
    */
    public function actionUpdate($id)
    {
      $model = $this->findModel($id);

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        if ($model->cart->done)
          return '{"status":"danger", "text":"Корзина оформелна, нельзя изменить описание това"}';

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);

          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Пересчитаем корзину
        $answer = Cart::recountCart($model->cart);

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/cartitem/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      $items = Items::findOne($model->id_items);
      $colors = $items->colors;

      return $this->renderFile('@app/views/cartitem/_form.php', [
      'form' => $model,
      'colors' => $colors,
      ]);
    }

    /**
    *  Удалить запись
    */
    public function actionDelete($id)
    {
      // Удалим запись
      $model = $this->findModel($id);
      $cart = $model->cart;
      if ($cart->done)
        return '{"status":"danger", "text":"Корзина оформелна, нельзя удалить товар"}';


      $model->delete();

      $answer = Cart::recountCart($cart);
      if(!$answer['status'])
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';

      return '{"status":"success", "text":"Запись удалена!"}';
    }

    /**
     *
     */
    protected function findModel($id)
    {
        if (($model = CartItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
