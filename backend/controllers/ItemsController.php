<?php

namespace backend\controllers;

use Yii;
use app\models\Items;
use app\models\Category;
use app\models\Subcategory;
use app\models\ItemsImage;
use app\models\ItemsDescription;
use app\models\Colors;
use app\models\Helper;
use app\models\ImageUpload;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * itemsController implements the CRUD actions for items model.
 */
class ItemsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all items models.
     * @return mixed
     */
    public function actionIndex($page = 1)
    {
      $criteria = Items::criteria();
      $criteria = Helper::setFilter($criteria);
      $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
      $data = $criteria->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();

      $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

      return $this->render('index', [
        'table_data' => $data,
        'pages' => $pages,
      ]);
    }


    /**
      * Добавление новой записи
      */
    public function actionAdd()
    {
      $model = new Items;

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $post = $_POST['data'];
        $model->attributes = $post;

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);

          return '{"status":"danger","text":"' . $message[0] . '"}';
        }
        // СВЯЗИ ДЛЯ КАТЕГОРИЙ
        if(count($post['category'])){
          foreach ($post['category'] as $value) {
            $category = Category::findOne($value);
            $model->link('category', $category);
          }
        }

        // СВЯЗИ ДЛЯ Интерьера
        if (Yii::$app->params['siteSubCategory']) {
          if(count($post['subcategory'])){
            foreach ($post['subcategory'] as $value) {
              $subcategory = Subcategory::findOne($value);
              $model->link('subcategory', $subcategory);
            }
          }
        }

        // СВЯЗИ ДЛЯ ЦВЕТОВ
        if(count($post['colors']) && $post['colors']){
          foreach ($post['colors'] as $value) {
            $colors = Colors::findOne($value);
            $model->link('colors', $colors);
          }
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/items/_view.php', [
            'table_data' => [$model],
        ]);

        return json_encode($data);
      }
      $itemsLast = Items::find()
                  ->orderBy('id_items DESC')
                  ->one();

      $category = (new \yii\db\Query())
              ->from("category")
              ->where(["visible" => "1"])
              ->orderBy("name_ru ASC")
              ->All();

      $collections = (new \yii\db\Query())
              ->from("collections")
              ->where(["visible" => "1"])
              ->orderBy("name ASC")
              ->All();

      if (Yii::$app->params['siteSubCategory']) {
        $subcategory = (new \yii\db\Query())
                ->from("subcategory")
                ->where(["visible" => "1"])
                ->orderBy("name_ru ASC")
                ->All();
      }
      $manufacturer = (new \yii\db\Query())
              ->from("manufacturer")
              ->where(["visible" => "1"])
              ->orderBy("name ASC")
              ->All();

      $colors = (new \yii\db\Query())
              ->from("colors")
              ->orderBy("name ASC")
              ->All();

      return $this->renderFile('@app/views/items/_form.php', [
          'form' => $model,
          'category' => $category,
          'subcategory' => $subcategory,
          'manufacturer' => $manufacturer,
          'colors' => $colors,
          'image' => $image,
          'itemsLast' => $itemsLast,
          'collections' => $collections,
      ]);
    }

    /**
     *  Редактирование записи
     */
     public function actionUpdate($id)
     {
       $model = Items::findOne($id);
       // Если получили данные для добавления
       if (isset($_POST['data'])) {
         // https://nix-tips.ru/yii2-api-guides/yii-base-model.html#load()-detail
         // $model->load($_POST['data']);
         $model->attributes = $_POST['data'];

         if (is_array($_POST['data']['image']))
           $model->image = implode(",", $_POST['data']['image']);

         $model->sale = $model->sale_price ? 1 : 0;

         // Изменим связи по цветам
         if (count($_POST['data']['colors']) && $_POST['data']['colors'])
           $model->toggleLink($_POST['data']['colors'], 'colors', 'id_colors', new Colors);
         // Изменим связи по категориям
         $model->toggleLink($_POST['data']['category'], 'category', 'id_category', new Category);
         // Изменим связи по интерьеру
         if ($this->module->params->siteSubCategory)
           $model->toggleLink($_POST['data']['subcategory'], 'subcategory', 'id_subcategory', new Subcategory);

         // Добавим описание если оно изменилось
         if ($_POST['data']['description_change'])
           ItemsDescription::add($model->id_items, $_POST['data']['description']);

         if (!$model->save()) {
           $message = $model->getErrors();
           $message = array_shift($message);
           return '{"status":"danger","text":"' . $message[0] . '"}';
         }

         // Подготовим вывод данных
         $data['status'] = "success";
         $data['html'] = $this->renderFile('@app/views/items/_view.php', [
             'table_data' => [$model],
         ]);

         return json_encode($data);
       }

       if (Yii::$app->params['siteSubCategory']) {
         // Сменим ключи для массива
         $sub_active = Helper::indexByKey($model->subcategory, 'id_subcategory');
       }

       // Сменим ключи для массива
       $cat_active = Helper::indexByKey($model->category, 'id_category');

       // Сменим ключи для массива
       $col_active = Helper::indexByKey($model->colors, 'id_colors');

       $category = (new \yii\db\Query())
               ->from("category")
               ->where(["visible" => "1"])
               ->orderBy("name_ru ASC")
               ->All();

       $collections = (new \yii\db\Query())
               ->from("collections")
               ->where(["visible" => "1"])
               ->orderBy("name ASC")
               ->All();

       if (Yii::$app->params['siteSubCategory']) {
         $subcategory = (new \yii\db\Query())
               ->from("subcategory")
               ->where(["visible" => "1"])
               ->orderBy("name_ru ASC")
               ->All();
       }
       $manufacturer = (new \yii\db\Query())
               ->from("manufacturer")
               ->where(["visible" => "1"])
               ->orderBy("name ASC")
               ->All();
       $colors = (new \yii\db\Query())
               ->from("colors")
               ->orderBy("name ASC")
               ->All();
       $image = (new \yii\db\Query())
               ->from("items_image")
               ->where(["id_items" => $id])
               ->All();

       return $this->renderFile('@app/views/items/_form.php', [
           'form' => $model,
           'category' => $category,
           'subcategory' => $subcategory,
           'manufacturer' => $manufacturer,
           'colors' => $colors,
           'image' => $image,
           'sub_active' => $sub_active,
           'cat_active' => $cat_active,
           'col_active' => $col_active,
           'collections' => $collections,
       ]);
     }


    /**
     *  Установим фото на главное изображение товара
     */
     public function actionImagemain($id, $idImg)
     {
       $modelImage = [];
       $model = Items::findOne($id);
       $image = ItemsImage::findOne($idImg);
       if (!$model || !$image) {
         return '{"status":"danger","text":"Не найден товар или фото"}';
       }

       if (strlen($model->image)) {
         $modelImage = explode(",", $model->image);
       }

       // Если это фото указано как главное, удалим его с главного
       if (in_array($image->src, $modelImage)) {
         $key = array_search($image->src, $modelImage);
         unset($modelImage[$key]);
       } else {
         if (count($modelImage) > 1) {
           return '{"status":"danger","text":"Все фото уже заданы"}';
         }

         $modelImage[] = $image->src;
       }

       $model->image = implode(",", $modelImage);

       if (!$model->save()) {
         $message = $model->getErrors();
         $message = array_shift($message);
         return '{"status":"danger","text":"' . $message[0] . '"}';
       }

       // Подготовим вывод данных
       $data['status'] = "success";
       $data['tr'] = $this->renderFile('@app/views/items/_view.php', [
           'table_data' => [$model],
       ]);
       $data['image'] = $this->renderFile('@app/views/items/_image_item.php', [
           'items' => $model,
           'image' => [$image],
       ]);

       return json_encode($data);
     }

    /**
     *  Выводит форму добавления фото и сами фото для управления
     */
     public function actionImage($id)
     {
       $items = Items::findOne($id);
       $image = $items->getImages()->all();

       $data['status'] = "success";
       $data['html'] = $this->renderFile('@app/views/items/_image.php', [
           'items' => $items,
           'image' => $image,
       ]);

       return json_encode($data);
     }

    /**
     *  Загружает изображения из формы ДРОПЯОНЕ (формы добавления фото)
     */
     public function actionImageupload($id)
     {
       $items = Items::findOne($id);
       $dir = Yii::getAlias('@frontend/web').'/img/items/'.$items->id_items.'/';
       $name = uniqid("img", False).'.jpg';

       $answer = ImageUpload::download($_FILES['file']['tmp_name'], $dir.$name, $dir);
       if (!$answer['status']) {
         return '{"status":"danger", "text":"'.$answer['text'].'", "code":"1"}';
       }

       $answer = ImageUpload::cutToCenter($dir, $name, 500, 500);
       if (!$answer['status']) {
         return '{"status":"danger", "text":"'.$answer['text'].'", "code":"2"}';
       }

       $image = new ItemsImage;
       $image->id_items = $id;
       $image->src = $id.'/'.$name;
       if (!$image->save()) {
         $message = $image->getErrors();
         $message = array_shift($message);

         return '{"status":"danger","text":"' . $message[0] . '", "code":"3"}';
       }

       $data['status'] = "success";
       $data['text'] = "Изображение добавленно";
       $data['image'] = $this->renderFile('@app/views/items/_image_item.php', [
           'items' => $items,
           'image' => [$image],
       ]);
       return json_encode($data);
     }

    /**
     *  Удалить запись
     */
     public function actionDelete($id)
     {
       $model = $this->findModel($id);
       if (!$model)
        return '{"status":"danger", "text":"Запись не найдена!"}';

       $model->delete();
       return '{"status":"success", "text":"Запись удалена!"}';
     }


    /**
     *  Получить кол-во товара по цветам
     */
     public function actionCountinstockcolor($id)
     {
       $model = $this->findModel($id);

       $count = (new \yii\db\Query())
             ->select("SUM(instock) as count")
             ->from("items_colors_relationship")
             ->where(["id_items" => $id])
             ->One();

        $count['count'] = (int) $count['count'];

        return '{"status":"success", "count":"'.$count['count'].'"}';
     }

    /**
     *
     */
    protected function findModel($id)
    {
        if (($model = Items::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
