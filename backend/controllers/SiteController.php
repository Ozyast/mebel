<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'cacheclear' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
      $count = (new \yii\db\Query())
        ->select([
          "COUNT(*) as contact",
          "(SELECT COUNT(*) FROM  cart WHERE done = 0) as cart",
          "(SELECT COUNT(*) FROM  cart WHERE status = 'Ожидает проверки менеджером') as cart_on_check",
          "(SELECT COUNT(*) FROM  items WHERE visible = 1) as items",
        ])
        ->from("contact")
        ->where(["read"=>0])
        ->One();

        $cart = \app\models\Cart::find()->where(["done"=>0])->all();

        return $this->render('index', [
          'count' => $count,
          'cart' => $cart,
        ]);
    }

    /**
    * Очистка КЭША
    */
    public function actionCacheclear()
    {
      if (Yii::$app->cache->flush()) {
          return '{"status":"success","text":"Кэш очищен"}';
      } else {
        return '{"status":"danger","text":"Ошибка при очистки кэша"}';
      }
    }


    /**
    * Запуск миграции без использования консоли
    */
    public function actionMigration() {
      $stdout_url = 'stdout';

      // ДОСТУП НА СТРАНИЦУ
      if (!$_POST['pass'])
        return $this->render('@app/views/sservices/access', ['url'=>'site/migration']);
      if (md5(md5($_POST['pass'])) !== '2fa91e9e84de456c9ce60c932d3d9544')
        return $this->render('@app/views/sservices/access', ['url'=>'site/migration', 'message'=>'Ключ доступа введен не верно']);

      if (!defined('STDOUT'))
        define('STDOUT', fopen($stdout_url, 'w'));

      $migration = new \yii\console\controllers\MigrateController('migrate', Yii::$app);
      $migration->runAction('up', ['migrationPath' => '@console/migrations/', 'interactive' => false]);

      $handle = fopen($stdout_url, 'r');
      $message = '';
      while (($buffer = fgets($handle, 4096)) !== false) {
        $message.=$buffer . "<br>";
      }
      fclose($handle);

      return $this->render('@app/views/sservices/migrate', ['message' => $message]);
    }


    /**
    * Лог бэкэнда
    */
    public function actionLog() {
      // ДОСТУП НА СТРАНИЦУ
      if (!$_POST['pass'])
        return $this->render('@app/views/sservices/access', ['url'=>'site/log']);
      if (md5(md5($_POST['pass'])) !== '2fa91e9e84de456c9ce60c932d3d9544')
        return $this->render('@app/views/sservices/access', ['url'=>'site/log', 'message'=>'Ключ доступа введен не верно']);

      $flog = fopen("../runtime/logs/app.log", 'r');
      if (!$flog)
        return "Ошибка при открытии файла";

      while (!feof($flog)) {
        $message .= fgets($flog, 999);
      }

      fclose($flog);

      return $this->render('@app/views/sservices/migrate', ['message' => $message]);
    }
}
