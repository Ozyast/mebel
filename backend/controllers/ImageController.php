<?php

namespace backend\controllers;

use Yii;
use app\models\ItemsImage;
use app\models\ImageUpload;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemsController implements the CRUD actions for Items model.
 */
class ImageController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Items models.
  * @return mixed
  */
  public function actionIndex($page = 1)
  {
    $criteria = ItemsImage::criteria();
    $criteria = Helper::setFilter($criteria);
    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    return $this->render('index', [
      'table_data' => $data,
      'pages' => $pages,
    ]);
  }

  /**
  * Добавление новой записи
  */
  public function actionAdd()
  {
    $model = new ItemsImage;

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];
      if (!$model->validate()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
      }

      // заливаем фото в нужную папку и делаем маленькую копию
      $answer = $model->uploadImage($model->src);
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }

      // Сохраним новы путь к фото
      $model->src = $answer['src'];

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);

        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/image/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    $colors = (new \yii\db\Query())
      ->from("colors")
      ->orderBy("name ASC")
      ->All();

    return $this->renderFile('@app/views/image/_form.php', [
    'form' => $model,
    'colors' => $colors,
    ]);
  }

  /**
  *  Редактирование записи
  */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];
      if (!$model->validate()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
      }

      // Если есть новый путь до фото
      if (strlen($_POST['data']['src'])) {
        // заливаем фото в нужную папку и делаем маленькую копию
        $answer = $model->uploadImage($model->src);
        if (!$answer['status']) {
          return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
        }
        // Сохраним новы путь к фото
        $model->src = $answer['src'];
      }

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);

        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/image/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    $colors = (new \yii\db\Query())
      ->select([
        "icr.id_colors",
        "(SELECT name FROM colors c WHERE c.id_colors = icr.id_colors) as name",
      ])
      ->from("items_colors_relationship icr")
      ->where(["id_items" => $model->id_items])
      ->orderBy("name ASC")
      ->All();

    return $this->renderFile('@app/views/image/_form.php', [
    'form' => $model,
    'colors' => $colors,
    ]);
  }

  /**
  *  Удалить фото
  */
  public function actionImageremove($id)
  {
    $model = $this->findModel($id);

    // Удалим фото
    $answer = $model->deleteImage();
    if (!$answer['status']) {
      return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
    }

    $model->src = "";

    if (!$model->save()) {
      $message = $model->getErrors();
      $message = array_shift($message);

      return '{"status":"danger","text":"' . $message[0] . '"}';
    }

    // Подготовим вывод данных
    $data['status'] = "success";
    $data['html'] = $this->renderFile('@app/views/image/_view.php', [
    'table_data' => [$model],
    ]);

    return json_encode($data);
  }


  /**
  *  Удалить запись
  */
  public function actionDelete($id)
  {
    $model = $this->findModel($id);

    // Удалим фото
    if (strlen($model->src)) {
      $answer = $model->deleteImage();
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }
    }

    // Удалим запись
    $model->delete();

    return '{"status":"success", "text":"Запись удалена!"}';
  }

  /**
  *
  */
  protected function findModel($id)
  {
    if (($model = ItemsImage::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
