<?php

namespace backend\controllers;

use Yii;
use app\models\Articles;
use app\models\ImageUpload;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FurnituresController implements the CRUD actions for Furnitures model.
 */
class ArticlesController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Furnitures models.
  * @return mixed
  */
  public function actionIndex($page = 1)
  {
    $criteria = Articles::criteria();
    $criteria = Helper::setFilter($criteria);
    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
    ->limit($pagination->limit)
    ->all();

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    return $this->render('index', [
      'table_data' => $data,
      'pages' => $pages,
    ]);
  }

  /**
  * Добавление новой записи
  */
  public function actionAdd()
  {
    $model = new Articles;

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/articles/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    return $this->renderFile('@app/views/articles/_form.php', [
    'form' => $model,
    ]);
  }

  /**
  *  Редактирование записи
  */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/articles/_view.php', [
        'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    return $this->renderFile('@app/views/articles/_form.php', [
    'form' => $model,
    ]);
  }

  /**
  *  Выводит форму добавления фото и сами фото для управления
  */
  public function actionImage($id)
  {
    $articles = Articles::findOne($id);

    $image = $dir = [];
    // Парверим есть ли папка
    if (file_exists($articles->imageDir().$articles->id_articles))
      $dir = scandir($articles->imageDir().$articles->id_articles);

    if (!empty($dir))
      foreach ($dir as $value) {
        if (preg_match("(\.jpg$|\.png$|\.gif$)", $value))
          $image[] = $value;
      }

    $data['status'] = "success";
    $data['html'] = $this->renderFile('@app/views/articles/_image.php', [
    'articles' => $articles,
    'image' => $image,
    ]);

    return json_encode($data);
  }

  /**
  *  Выводит форму добавления фото и сами фото для управления
  */
  public function actionFile($id)
  {
    $articles = Articles::findOne($id);

    $dir = Yii::getAlias('@frontend/views').'/articles/';
    $dir_file = scandir($dir);

    foreach ($dir_file as $value) {
      if (preg_match("/^_.*\.php$/", $value) && $value == $articles->file_name) {
        $file = $value;
        $text = file_get_contents($dir.$file);
        break;
      }
    }

    $data['status'] = "success";
    $data['html'] = $this->renderFile('@app/views/articles/_file.php', [
      'articles' => $articles,
      'text' => $text,
      'file_src' => $dir.$file,
    ]);

    return json_encode($data);
  }

  /**
  *  Выводит форму добавления фото и сами фото для управления
  */
  public function actionFilesave($id)
  {
    $articles = Articles::findOne($id);

    $dir = Yii::getAlias('@frontend/views').'/articles/';
    // Если текста нет
    if (!strlen($_POST['text'])) {
      // И файлсуществует, то удалим его
      if (file_exists($dir.$articles->file_name)) {
        unlink($dir.$articles['file_name']);
        return '{"status":"success", "text":"Файл изменен"}';
      }
    }

    if (!strlen($articles->file_name))
      $articles->file_name = uniqid("_", False).".php";
    $answer = @file_put_contents($dir.$articles->file_name, $_POST['text']);
    if (!$answer) {
      return '{"status":"danger", "text":"Ошибка при сохранении файла"}';
    }

    if (!$articles->save()) {
      $message = $articles->getErrors();
      $message = array_shift($message);
      return '{"status":"danger","text":"' . $message[0] . '"}';
    }

    $data['status'] = "success";
    $data['html'] = $this->renderFile('@app/views/articles/_view.php', [
      'table_data' => [$articles],
    ]);

    return json_encode($data);
  }

  /**
  *  Загружает изображения из формы ДРОПЯОНЕ (формы добавления фото)
  */
  public function actionImageupload($id)
  {
    $articles = Articles::findOne($id);
    $dir = Yii::getAlias('@frontend/web').'/img/articles/'.$articles->id_articles.'/';
    $name = uniqid("img", False).'.jpg';

    $answer = ImageUpload::download($_FILES['file']['tmp_name'], $dir.$name, $dir);
    if (!$answer['status']) {
      return '{"status":"danger", "text":"'.$answer['text'].'", "code":"1"}';
    }
    $name = $articles->id_articles."/".$name;

    return '{"status":"success", "text":"Изображение добавленно", "name":"'.$name.'"}';
  }

  /**
  *  Удалить запись
  */
  public function actionImagedelete($src)
  {
    // Удалим фото
    $answer = Articles::deleteImage($src);
    if (!$answer['status']) {
      return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
    }

    return '{"status":"success", "text":"Запись удалена!"}';
  }

  /**
  *  Удалить запись
  */
  public function actionDelete($id)
  {
    // Удалим запись
    $this->findModel($id)->delete();
    return '{"status":"success", "text":"Запись удалена!"}';
  }

  /**
  *
  */
  protected function findModel($id)
  {
    if (($model = Articles::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
