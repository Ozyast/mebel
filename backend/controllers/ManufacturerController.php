<?php

namespace backend\controllers;

use Yii;
use app\models\Manufacturer;
use app\models\ImageUpload;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* FurnituresController implements the CRUD actions for Furnitures model.
*/
class ManufacturerController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Furnitures models.
  * @return mixed
  */
  public function actionIndex($page = 1)
  {
    $criteria = Manufacturer::criteria();
    $criteria = Helper::setFilter($criteria);
    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    return $this->render('index', [
      'table_data' => $data,
      'pages' => $pages,
      ]);
    }


    /**
    * Добавление новой записи
    */
    public function actionAdd()
    {
      $model = new Manufacturer;

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        // Проверяем правильно ли введены данные
        if (!$model->validate()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
        }

        // заливаем фото в нужную папку
        if (strlen($model->image)) {
          $answer = $model->uploadImage($model->image);
          if (!$answer['status']) {
            return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
          }
        }

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);

          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/manufacturer/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/manufacturer/_form.php', [
      'form' => $model,
      ]);
    }


    /**
    *  Редактирование записи
    */
    public function actionUpdate($id)
    {
      $model = $this->findModel($id);

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        // Проверяем правильно ли введены данные
        if (!$model->validate()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
        }

        // заливаем фото в нужную папку
        if ((strlen($model->image)) && ($model->oldAttributes['image'] != $model->image)) {
          $answer = $model->uploadImage($model->image);
          if (!$answer['status']) {
            return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
          }
        }

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);

          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/manufacturer/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/manufacturer/_form.php', [
      'form' => $model,
      ]);
    }

    /**
    *  Удалить фото
    */
    public function actionImageremove($id)
    {
      $model = $this->findModel($id);

      // Удалим фото
      $answer = $model->deleteImage();
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }

      $model->image = "";

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);

        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/manufacturer/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    /**
    *  Удалить запись
    */
    public function actionDelete($id)
    {
      $model = $this->findModel($id);

      // Удалим фото
      $answer = $model->deleteImage();
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }

      // Удалим запись
      $model->delete();

      return '{"status":"success", "text":"Запись удалена!"}';
    }

    /**
    *
    */
    protected function findModel($id)
    {
      if (($model = Manufacturer::findOne($id)) !== null) {
        return $model;
      } else {
        throw new NotFoundHttpException('The requested page does not exist.');
      }
    }
  }
