<?php

namespace backend\controllers;

use Yii;
use app\models\Modules;
use app\models\ImageUpload;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* ModulesController implements the CRUD actions for Modules model.
*/
class ModulesController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Modules models.
  * @return mixed
  */
  public function actionIndex($page = 1)
  {
    $criteria = Modules::criteria();
    $criteria = Helper::setFilter($criteria);
    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    return $this->render('index', [
      'table_data' => $data,
      'pages' => $pages,
      ]);
    }


    /**
    * Добавление новой записи
    */
    public function actionAdd()
    {
      $model = new Modules;

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $answer = $model->add($_POST['data']);
        if (!$answer['status'])
          return '{"status":"danger","text":"' . $answer['text'] . '", "code":"'.$answer['code'].'"}';

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/modules/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/modules/_form.php', [
      'form' => $model,
      ]);
    }

    /**
    *  Редактирование записи
    */
    public function actionUpdate($id)
    {
      $model = $this->findModel($id);

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        // Проверяем правильно ли введены данные
        if (!$model->validate()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
        }

        // заливаем фото в нужную папку
        if(strlen($model->image)){
          // Если изменился id товара
          if ($model->oldAttributes->id_items != $model->id_items) {
            $answer = $model->moveImage($model->image);
          } else {
            $answer = $model->uploadImage($model->image);
            if (!$answer['status']) {
              return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
            }
          }
        }

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/modules/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/modules/_form.php', [
      'form' => $model,
      'collections' => $collections,
      ]);
    }

    /**
    *  Удалить фото
    */
    public function actionImageremove($id)
    {
      $model = $this->findModel($id);

      // Удалим фото
      $answer = $model->deleteImage();
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }

      $model->image = "";

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);

        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/modules/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    /**
    *  Удалить запись
    */
    public function actionDelete($id)
    {
      $model = $this->findModel($id);

      // Удалим фото
      $answer = $model->deleteImage();
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }

      // Удалим запись
      $model->delete();

      return '{"status":"success", "text":"Запись удалена!"}';
    }


    /**
    * Получение модулей с удаленной страницы
    */
    public function actionGetpage()
    {
      $link = $_POST['link'];

      $answer = Helper::curl($link);
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'"}';
      }
      $page = $answer['output'];

      preg_match("/<body[^>]*>(.*)<\/body>/ims", $page, $page);

      $page = preg_replace("/<style>.*<\/style>/i", "", $page[0]);
      $page = preg_replace("/<script type=\"text\/javascript\">.*<\/script>/i", "", $page);
      $page = preg_replace("/src=\"/i", "srca=\"", $page);

      return json_encode(["status"=>"success", "html"=>$page]);
    }

    /**
    * Добавление массива с новыми записями
    */
    public function actionAddall()
    {
      $text = "";

      // Если получили данные для добавления
      if (empty($_POST['data']) || !is_array($_POST['data'])) {
        return '{"status":"danger", "text":"Нет данных для добавления"}';
      }

      foreach ($_POST['data'] as $row) {
        $modules = (new \yii\db\Query())
                ->from("modules")
                ->where([
                  "id_collections" => $row['id_collections'],
                  "name" => $row['name'],
                ])
                ->count();
        if ($modules) {
          $text .= $row['name'].": Уже есть в базе<br/>";
          continue;
        }

        $model = new Modules;
        $answer = $model->add($row);
        if (!$answer['status'])
          $text .= $row['name'].": Ошибка добавления (".$answer[text].")<br/>";
      }

      // Подготовим вывод данных
      $data['status'] = "info";
      $data['text'] = strlen($text) ? $text : "Готово";
      $data['html'] = $this->renderFile('@app/views/modules/_view.php', [
        'table_data' => [$model],
      ]);
      return json_encode($data);
    }

    /**
    * Finds the Modules model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Modules the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
      if (($model = Modules::findOne($id)) !== null) {
        return $model;
      } else {
        throw new NotFoundHttpException('The requested page does not exist.');
      }
    }
  }
