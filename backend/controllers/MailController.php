<?php

namespace backend\controllers;

use Yii;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FurnituresController implements the CRUD actions for Furnitures model.
 */
class MailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Furnitures models.
     * @return mixed
     */
    public function actionNeworder()
    {
      $data = [
        "cart" => [
          "hash" => '725a5e747b4857b70832150537486590',
          "status" => 'Ожидает проверки менеджером',
          "user_name" => '123123',
          "user_phone" => '3123',
          "user_address" => '',
          "comment" => '123123',
          "delivery" => 0,
          "price" => 48260,
          "sale" => 4260,
          "price_total" => 44000,
          "id_cart" => 48,
        ],
        "cartItem" => [
          "recount" => 0,
          "cartItem" => [
            "2_4" => [
              "id_items" => 2,
              "color" => 4,
              "count" => 1,
              "title" => 'Шкаф-купе КП-232',
              "price" => 24130,
              "sale" => 2130,
              "image" => '2/@Shkaf_1_sn_2_логотип.jpg',
            ],
            "2_12" => [
              "id_items" => 2,
              "color" => 12,
              "count" => 1,
              "title" => 'Шкаф-купе КП-232',
              "price" => 24130,
              "sale" => 2130,
              "image" => '2/@Shkaf_1_sl-k_2_логотип.jpg',
            ]
          ],
          "cart" => [
            "sale" => 4260,
            "price" => 48260,
            "total" => 44000,
          ],
          "items" => [
            "2" => [
              "id_items" => 2,
              "title" => 'Шкаф-купе КП-232',
              "image" => '2/Shkaf_1_bd_2_Р»РѕРіРѕС‚РёРї.jpg,2/Shkaf_1_sl_2_Р»РѕРіРѕС‚РёРї.jpg',
              "id_manufacturer" => 1,
              "price" => 24130,
              "sale" => 1,
              "sale_price" => 22000,
              "additions" => '',
              "services" => '',
              "instock" => 0,
              "total_sold" => 4,
              "created_at" => '2018-08-31 12:33:30',
              "updated_at" => '2018-10-29 14:13:57',
              "visible_catalog" => 1,
              "visible" => 1,
            ]
          ]
        ]
      ];

      $this->layout = "@common/mail/layouts/html";
      return $this->render('@common/mail/mails/new_order.php', $data);
    }
}
