<?php

namespace backend\controllers;

use Yii;
use app\models\Question;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemsController implements the CRUD actions for Items model.
 */
class QuestionController extends Controller
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Items models.
  * @return mixed
  */
  public function actionIndex($page = 1)
  {
    $criteria = Question::criteria();
    $criteria = Helper::setFilter($criteria);
    $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
    $data = $criteria->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

    $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

    return $this->render('index', [
      'table_data' => $data,
      'pages' => $pages,
    ]);
  }

  /**
  * Добавление новой записи
  */
  public function actionAdd()
  {
    $model = new Question;

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/question/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    return $this->renderFile('@app/views/question/_form.php', [
    'form' => $model,
    ]);
  }

  /**
  *  Редактирование записи
  */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    // Если получили данные для добавления
    if (isset($_POST['data'])) {
      $model->attributes = $_POST['data'];

      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);

        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/question/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    return $this->renderFile('@app/views/question/_form.php', [
    'form' => $model,
    ]);
  }

  /**
  *  Удалить запись
  */
  public function actionDelete($id)
  {
    // Удалим запись
    $this->findModel($id)->delete();
    return '{"status":"success", "text":"Запись удалена!"}';
  }

  /**
  *
  */
  protected function findModel($id)
  {
    if (($model = Question::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
