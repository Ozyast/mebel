<?php

namespace backend\controllers;

use Yii;
use app\models\ImageUpload;
use app\models\Helper;
// use yii\data\Pagination;
// use yii\data\ActiveDataProvider;
use yii\web\Controller;
// use yii\web\NotFoundHttpException;
// use yii\filters\VerbFilter;

/**
 * FurnituresController implements the CRUD actions for Furnitures model.
 */
class ImageuploadController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Загрузка изображения в папку темп
     */
    public function actionDropzone()
    {
      $dir = Yii::getAlias('@frontend/web').'/img/temp/';
      $name = uniqid("img", False).'.jpg';

      $answer = ImageUpload::download($_FILES['file']['tmp_name'], $dir.$name, $dir);
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"1"}';
      }

      return '{"status":"success","name":"'.$name.'","text":"Изображение добавленно"}';
    }

    /**
     *
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
      *
      */
    // public function actionAdd()
    // {
    //   $class = "app\models\Equipment".$_POST['data']['type'];
    //   $model = new $class($model);
    //
    //   // Если получили данные для добавления
    //   if (isset($_POST['data'])) {
    //     $model->attributes = $_POST['data'];
    //
    //     if (!$model->save()) {
    //       $message = $model->getErrors();
    //       $message = array_shift($message);
    //
    //       return '{"status":"danger","text":"' . $message[0] . '"}';
    //     }
    //
    //     // Подготовим вывод данных
    //     $data['status'] = "success";
    //     $data['html'] = $this->renderFile('@app/views/equipment/_view.php', [
    //         'table_data' => [$model],
    //     ]);
    //
    //     return json_encode($data);
    //   }
    //
    //   return $this->renderFile('@app/views/equipment/_form.php', [
    //       'form' => $model,
    //   ]);
    // }

    /**
     *
     */
     // public function actionUpdate($id)
     // {
     //   $model = $this->findModel($id);
     //
     //   // Если получили данные для добавления
     //   if (isset($_POST['data'])) {
     //     // $model->attributes = $_POST['data'];
     //     // https://nix-tips.ru/yii2-api-guides/yii-base-model.html#load()-detail
     //     $model->load($_POST['data']);
     //
     //     if (!$model->save()) {
     //       $message = $model->getErrors();
     //       $message = array_shift($message);
     //
     //       return '{"status":"danger","text":"' . $message[0] . '"}';
     //     }
     //
     //     $model = new Equipment;
     //     $data = Helper::getDataForTable($model, $model->criteria()->where(["id_equipment" => $id]), 1);
     //
     //     // Подготовим вывод данных
     //     $data['status'] = "success";
     //     $data['html'] = $this->renderFile('@app/views/equipment/_view.php', [
     //         'table_data' => $data['data'],
     //     ]);
     //
     //     return json_encode($data);
     //   }
     //
     //   return $this->renderFile('@app/views/equipment/_form.php', [
     //       'form' => $model,
     //   ]);
     // }

    /**
     *
     */
     // public function actionDelete($id)
     // {
     //   $this->findModel($id)->delete();
     //
     //   return '{"status":"success", "text":"Запись удалена!"}';
     // }
}
