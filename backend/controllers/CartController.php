<?php

namespace backend\controllers;

use Yii;
use app\models\Cart;
use app\models\ImageUpload;
use app\models\Helper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FurnituresController implements the CRUD actions for Furnitures model.
 */
class CartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Furnitures models.
     * @return mixed
     */
    public function actionIndex($page = 1)
    {
      $criteria = Cart::criteria();
      $criteria = Helper::setFilter($criteria);
      $pagination = new Pagination(['totalCount' => $criteria->count(), 'pageSize' => 20]);
      $data = $criteria->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->orderBy("done ASC")
                    ->all();

      $pages = Helper::pages($pagination->totalCount, $pagination->pageSize, $page);

      return $this->render('index', [
        'table_data' => $data,
        'pages' => $pages,
      ]);
    }

    /**
    * Добавление новой записи
    */
    public function actionAdd()
    {
      $model = new Slider;

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        // Проверяем правильно ли введены данные
        if (!$model->validate()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '", "code":"'.$answer['code'].'"}';
        }

        // заливаем фото в нужную папку
        if(strlen($model->image)){
          $answer = $model->uploadImage($model->image);
          if (!$answer['status']) {
            return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
          }
        }

        // Сохраним наши данные
        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);
          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/cart/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/cart/_form.php', [
      'form' => $model,
      ]);
    }

    /**
    *  Редактирование записи
    */
    public function actionUpdate($id)
    {
      $model = $this->findModel($id);

      // Если получили данные для добавления
      if (isset($_POST['data'])) {
        $model->attributes = $_POST['data'];

        if ($model->oldAttributes['done'] != $model->attributes['done']) {
          if ($model->done) {
            $answer = $model->itemsSold();
          } else {
            $answer = $model->itemsSoldRestore();
          }

          if (!$answer['status'])
            return '{"status":"danger","text":"' . $answer['text'] . '"}';
        }

        if (!$model->save()) {
          $message = $model->getErrors();
          $message = array_shift($message);

          return '{"status":"danger","text":"' . $message[0] . '"}';
        }

        // Подготовим вывод данных
        $data['status'] = "success";
        $data['html'] = $this->renderFile('@app/views/cart/_view.php', [
        'table_data' => [$model],
        ]);

        return json_encode($data);
      }

      return $this->renderFile('@app/views/cart/_form.php', [
      'form' => $model,
      ]);
    }

    /**
    *  Удалить фото
    */
    public function actionImageremove($id)
    {
      $model = $this->findModel($id);

      // Удалим фото
      $answer = $model->deleteImage();
      if (!$answer['status']) {
        return '{"status":"danger", "text":"'.$answer['text'].'", "code":"'.$answer['code'].'"}';
      }

      $model->image = "";
      if (!$model->save()) {
        $message = $model->getErrors();
        $message = array_shift($message);
        return '{"status":"danger","text":"' . $message[0] . '"}';
      }

      // Подготовим вывод данных
      $data['status'] = "success";
      $data['html'] = $this->renderFile('@app/views/cart/_view.php', [
      'table_data' => [$model],
      ]);

      return json_encode($data);
    }

    /**
    *  Удалить запись
    */
    public function actionDelete($id)
    {
      // Получим запись
      $model = $this->findModel($id);

      // Удалим все товары корзины
      Yii::$app->db->createCommand()->delete('cart_item', 'id_cart = '.$model->id_cart)->execute();

      // Удалим запись
      $this->findModel($id)->delete();
      return '{"status":"success", "text":"Запись удалена!"}';
    }

    /**
     *
     */
    protected function findModel($id)
    {
        if (($model = Cart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
