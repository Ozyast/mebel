<?php
// Имя формы
$table_name = "form-similar";
// ID редактируемой записи
$id_table_name = 'id_similar';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
                <label class="col-md-3 control-label" for="id_items_master">ID Товар 1</label>
                <div class="col-md-8">
                    <input type="text" id="id_items_master" name="id_items_master" class="form-control"  placeholder="ID Товар 1" value="<?= $form['id_items_master'] ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="id_items_slave">ID Товар 2</label>
                <div class="col-md-8">
                    <input type="text" id="id_items_slave" name="id_items_slave" class="form-control"  placeholder="ID Товар 2" value="<?= $form['id_items_slave'] ?>">
                </div>
            </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
