<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;
use yii\helpers\Html;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}



foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_page'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="seo">
    <td>
      <?= $id ?>
    </td>

    <td>
      <?= $row['url'] ?>
    </td>

    <td>
      <?= $row['title'] ?>
    </td>

    <td>
      <?= $row['description'] ?>
    </td>

    <td>
      <?= $row['h1'] ?>
    </td>

    <td>
      <?= Html::encode($row['text']) ?>
    </td>

    <td>
      <label class='switch switch-success data-checkbox' id='visible'>
        <input type='checkbox' id='visible' <?= $row["visible"] ? "checked" : "" ?>>
        <span></span>
      </label>
    </td>

    <td>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>

      <!-- <div class="border-bottom" style="margin:5px 0;"></div>-->
      <!-- <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
