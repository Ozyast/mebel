<?php
// Имя формы
$table_name = "form-seopage";
// ID редактируемой записи
$id_table_name = 'id_page';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
                <label class="col-md-3 control-label" for="url">Url</label>
                <div class="col-md-8">
                    <input type="text" id="url" name="url" class="form-control"  placeholder="Url" value="<?= $form['url'] ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="title">Title</label>
                <div class="col-md-8">
                    <input type="text" id="title" name="title" class="form-control"  placeholder="Title" value="<?= $form['title'] ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="description">Description</label>
                <div class="col-md-8">
                    <input type="text" id="description" name="description" class="form-control"  placeholder="Description" value="<?= $form['description'] ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="h1">H1</label>
                <div class="col-md-8">
                    <input type="text" id="h1" name="h1" class="form-control"  placeholder="H1" value="<?= $form['h1'] ?>">
                </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="text">Текст</label>
              <div class="col-md-9">
                <textarea id="text" name="text" class="form-control" rows="5" placeholder="Текст"><?= $form['text'] ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="visible">Включить</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>


        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
