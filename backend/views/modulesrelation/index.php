<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Модули товара \"".$items->title."\"";
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['colors/index']];
$this->params['breadcrumbs'][] = ['label' => $items->id_items, 'url' => ['items/index', "s"=>$items->id_items, 'row'=>'id_items', 't'=>'i']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::$app->request->baseUrl ?>/js/page/modulesrelation.js"></script>

<div class="block">
    <!-- Form search -->
    <div class="row push-bit-2">
        <div class="col-sm-6 col-xs-6">
            <form id='form-search' class="form-horizontal" data-active="<?= isset($_GET['s']) ? 1 : 0 ?>">
                <fieldset>
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button type="submit" id="data-search-button" class="btn btn-effect-ripple btn-primary"></span><i class='fa fa-search'></i> Поиск </button>
                        </div>
                        <input type="text" id="data-search" name="data-search" class="form-control" placeholder="Поиск..." value="<?= isset($_GET['s']) ? $_GET['s'] : "" ?>">
                        <div class="input-group-btn">
                            <button type="button" id="data-search-rows" data-rows="id_colors" data-rows-type="i" class="btn btn-effect-ripple btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span> <span id='rows'><?= isset($_GET['row']) ? $_GET['row'] : "ID" ?></span>
                            </button>
                            <ul class="dropdown-menu" id="data-search-rows-list">
                                <li id='id' data-type="i"><a>ID</a></li>
                                <li id='id_colors' data-type="i"><a>ID Цвета</a></li>
                                <li id='id_items' data-type="i"><a>ID Товара</a></li>
                                <!-- <li id='ip' data-type="s"><a>IP</a></li>
                                <li id='file_name' data-type="s"><a>Имя файла</a></li> -->
                                <!--<li class="divider"></li>-->
                            </ul>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>

        <div class='cos-sm-6 col-xs-6 text-right'>
          <button id='data-add-url' data-toggle='tooltip' title='Добавить через URL' type="button" class="btn btn-effect-ripple btn-info"><i class="fa fa-plus"></i> Добавить URL</button>
          <button id='data-add' data-toggle='tooltip' title='Добавить' type="button" class="btn btn-effect-ripple btn-success"><i class="fa fa-plus"></i> Добавить</button>
        </div>
    </div>
    <!-- END Form search -->


    <table id="main_table" class="table table-vcenter table-bordered table-striped table-hover" data-main-id="<?= $this->context->actionParams['id'] ?>">
        <thead>
            <tr>
                <th id="id_equipment" class="text-center" style="width: 50px;">ID</th>
                <th id="name" class="text-center" style="width: 150px;">Фото</th>
                <th id="ip">Модуль</th>
                <th id="ip">Товар</th>
                <th id="ip">Коллекция</th>
                <th id="ip">Цена</th>
                <th id="visible" class="text-center"><i class="gi gi-eye_open"></i></th>
                <th class="text-center not-sort" style="width: 115px;"><i class="fa fa-flash"></i></th>
            </tr>
        </thead>
        <tbody>

            <?php
            echo $this->render("_view", array("table_data" => $table_data));
            ?>

        </tbody>
    </table>
    <p class='pull-right'>Записей: <?= count($table_data) ?> из <?= $pages['count_data'] ?></p>

    <div class='pagination'>
        <?php
        echo $this->render("@app/views/site/_str", array(
            "pages" => $pages,
        ));
        ?>
    </div>

</div>
<!-- END Example Block -->
