<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Обратная связь";
// $this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::$app->request->baseUrl ?>/js/page/contact.js"></script>

<div class="block">
    <!-- Form search -->
    <div class="row push-bit-2">
      <div class="col-sm-6 col-xs-6">
        <form id='form-search' class="form-horizontal" data-active="<?= isset($_GET['s']) ? 1 : 0 ?>">
          <fieldset>
            <div class="input-group">
              <div class="input-group-btn">
                <button type="submit" id="data-search-button" class="btn btn-effect-ripple btn-primary"></span><i class='fa fa-search'></i> Поиск </button>
              </div>
              <input type="text" id="data-search" name="data-search" class="form-control" placeholder="Поиск..." value="<?= isset($_GET['s']) ? $_GET['s'] : "" ?>">
              <div class="input-group-btn">
                <button type="button" id="data-search-rows" data-rows="id_contact" data-rows-type="i" class="btn btn-effect-ripple btn-info dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span> <span id='rows'><?= isset($_GET['row']) ? $_GET['row'] : "ID" ?></span>
                </button>
                <ul class="dropdown-menu" id="data-search-rows-list">
                  <li id='id_contact' data-type="i"><a>ID</a></li>
                  <li id='name' data-type="s"><a>Имя пользователя</a></li>
                  <li id='email' data-type="s"><a>Email пользователя</a></li>
                  <li id='ip' data-type="s"><a>IP</a></li>
                  <!--<li class="divider"></li>-->
                </ul>
              </div>
            </div>

          </fieldset>
        </form>
      </div>

      <!-- <div class='cos-sm-6 col-xs-6 text-right'>
        <button id='data-add' data-toggle='tooltip' title='Добавить' type="button" class="btn btn-effect-ripple btn-success"><i class="fa fa-plus"></i> Добавить</button>
      </div> -->
    </div>
    <!-- END Form search -->


    <table id="main_table" class="table table-vcenter table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center" style="width: 50px;" id="id_contact">ID</th>
                <th id="name" style="width: 200px;">Пользователь</th>
                <th id="name" class="text-center">Текст</th>
                <th id="name" class="text-center">IP</th>
                <th id="name" class="text-center">Дата</th>
                <th id="visible" class="text-center" style="width: 60px;"><i class="gi gi-message_full"></i></th>
                <th class="text-center not-sort" style="width: 115px;"><i class="fa fa-flash"></i></th>
            </tr>
        </thead>
        <tbody>

            <?php
            echo $this->render("_view", array("table_data" => $table_data));
            ?>

        </tbody>
    </table>
    <p class='pull-right'>Записей: <?= count($table_data) ?> из <?= $pages['count_data'] ?></p>

    <div class='pagination'>
        <?php
        echo $this->render("@app/views/site/_str", array(
            "pages" => $pages,
        ));
        ?>
    </div>

</div>
<!-- END Example Block -->
