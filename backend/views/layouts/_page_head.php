<?php
use yii\widgets\Breadcrumbs;
?>

<div class="content-header">
  <div class="row">
    <div class="col-sm-6">
      <div class="header-section">
        <h1><?= $this->title ?></h1>
      </div>
    </div>

    <div class="col-sm-6 hidden-xs">
      <div class="header-section">
        <?=
        Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          'options' => ["class"=>"breadcrumb breadcrumb-top"],
        ])
        ?>
      </div>
    </div>
  </div>
</div>
