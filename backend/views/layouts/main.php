<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\AppAsset;
use yii\helpers\Url;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

            <!-- Icons -->
            <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
            <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl ?>/img/favicon.png">
            <!-- END Icons -->

            <!-- Stylesheets -->
            <!-- Bootstrap is included in its original form, unaltered -->
            <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl ?>/css/bootstrap.min.css">

            <!-- Related styles of various icon packs and plugins -->
            <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl ?>/css/plugins.css">

            <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
            <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl ?>/css/main.css">

            <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

            <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
            <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl ?>/css/themes.css">
            <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl ?>/css/themes/social.css">
            <!-- END Stylesheets -->

            <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl ?>/css/my.css">

            <!-- Modernizr (browser feature detection library) -->
            <script src="<?= Yii::$app->request->baseUrl ?>/js/vendor/modernizr-3.3.1.min.js"></script>

            <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
            <script src="<?= Yii::$app->request->baseUrl ?>/js/vendor/jquery-2.2.4.min.js"></script>
            <script src="<?= Yii::$app->request->baseUrl ?>/js/vendor/bootstrap.min.js"></script>
            <script src="<?= Yii::$app->request->baseUrl ?>/js/plugins.js"></script>
            <script src="<?= Yii::$app->request->baseUrl ?>/js/app.js"></script>

            <script src="<?= Yii::$app->request->baseUrl ?>/js/vendor/hullabaloo.min.js"></script>
            <!-- <script src="/js/page/alertmy.js"></script> -->
            <script src="<?= Yii::$app->request->baseUrl ?>/js/page/pages.js"></script>

            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?php // $this->head()  ?>
        </head>
        <body>
            <?php $this->beginBody() ?>

            <div id="page-wrapper" class="page-loading">
                <div class="preloader">
                    <div class="inner">
                        <div class="preloader-spinner themed-background hidden-lt-ie10"></div>
                        <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                    </div>
                </div>

                <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                    <div id="sidebar">
                      <?php
                      echo Yii::$app->view->renderFile('@app/views/layouts/_menu.php', [
                        // "category" => $this->context->pageMenu["category"],
                      ]);
                      ?>
                    </div>

                    <div id="main-container">
                      <?php
                      echo Yii::$app->view->renderFile('@app/views/layouts/_head.php');
                      ?>

                        <div id="page-content">
                          <?php
                          echo Yii::$app->view->renderFile('@app/views/layouts/_page_head.php');
                          ?>

                          <?= $content ?>
                        </div>
                    </div>

                    <div id="modal-table" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p class='text-center text-muted'><i class='fa fa-spinner fa-spin'></i> Загружаю ... </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="modal-dialog" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </body>
    </html>
    <?php $this->endPage() ?>
