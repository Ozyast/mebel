
<?php
use yii\helpers\Url;
?>

<!-- Sidebar Brand -->
<div id="sidebar-brand" class="themed-background">
  <a href="/" class="sidebar-title text-center">
    <img src="<?= Yii::$app->request->baseUrl ?>/img/logo.png" alt="" width="150">
  </a>
</div>
<!-- END Sidebar Brand -->

<!-- Wrapper for scrolling functionality -->
<div id="sidebar-scroll">
  <!-- Sidebar Content -->
  <div class="sidebar-content">
    <!-- Sidebar Navigation -->
    <ul class="sidebar-nav">
      <li>
        <a href="<?= Url::to(["/"]) ?>"><i class="hi hi-home sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Главная</span></a>
      </li>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="<?= Url::to(["items/index"]) ?>"><i class="gi gi-stop sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Товар</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["collections/index"]) ?>"><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Коллекции</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["modules/index"]) ?>"><i class="fa fa-th sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Модули</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["similar/index"]) ?>"><i class="gi gi-random sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Похожие товары</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["manufacturer/index"]) ?>"><i class="fa fa-industry sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Производители</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["colors/index"]) ?>"><i class="fa fa-paint-brush sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Цвета</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["image/index"]) ?>"><i class="fa fa-image sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Фото</span></a>
      </li>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="<?= Url::to(["cart/index"]) ?>"><i class="hi hi-shopping-cart sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Корзина</span></a>
      </li>

      <li>
        <a href="<?= Url::to(["cartitem/index"]) ?>"><i class="gi gi-cart_in sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Товары</span></a>
      </li>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="<?= Url::to(["review/index"]) ?>"><i class="gi gi-comments sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Отзывы</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["question/index"]) ?>"><i class="gi gi-life_preserver sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Вопросы</span></a>
      </li>
      <li>
        <a href="<?= Url::to(["contact/index"]) ?>"><i class="fa fa-comment sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Контакты</span></a>
      </li>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="<?= Url::to(["category/index"]) ?>"><i class="fa fa-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Категории</span></a>
      </li>
      <?php if (Yii::$app->params['siteSubCategory']): ?>
        <li>
          <a href="<?= Url::to(["subcategory/index"]) ?>"><i class="fa fa-bars sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Подкатегории</span></a>
        </li>
      <?php endif; ?>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="<?= Url::to(["slider/index"]) ?>"><i class="fa fa-sliders sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Слайдер</span></a>
      </li>

      <li>
        <a href="<?= Url::to(["sale/index"]) ?>"><i class="fa fa-percent sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Акции</span></a>
      </li>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="<?= Url::to(["seo/index"]) ?>"><i class="fa fa-rocket sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">SEO Страницы</span></a>
      </li>

      <li>
        <a href="<?= Url::to(["articles/index"]) ?>"><i class="fa fa-file-text-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Статьи</span></a>
      </li>

      <li class="sidebar-separator">
        <i class="fa fa-ellipsis-h"></i>
      </li>

      <li>
        <a href="javascript:void(0);" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-fire sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Служебные</span></a>
        <ul>
          <li>
            <a href="<?= Url::to(["price/index"]) ?>"><i class="fa fa-rouble sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Цены</span></a>
          </li>

          <li>
            <a href="<?= Url::to(["site/log"]) ?>"><i class="fa fa-file-text-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Лог</span></a>
          </li>

          <li>
            <a href="<?= Url::to(["site/migration"]) ?>"><i class="fa fa-database sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Миграции</span></a>
          </li>

          <li>
            <a href="javascript:void(0);" class="sidebar-nav-submenu"><i class="gi gi-message_full sidebar-nav-icon"></i><i class="fa fa-chevron-left sidebar-nav-indicator"></i>Почта</a>
            <ul>
              <li>
                <a href="<?= Url::to(["mail/neworder"]) ?>">Новый заказ</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>

      <!--                                                                    <li>
      <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dropdown</span></a>
      <ul>
      <li>
      <a href="javascript:void(0)">Link #1</a>
    </li>
    <li>
    <a href="javascript:void(0)">Link #2</a>
  </li>
</ul>
</li>-->
</ul>
<!-- END Sidebar Navigation -->

<!-- Color Themes -->
<!-- Preview a theme on a page functionality can be found in js/app.js - colorThemePreview() -->
<div class="sidebar-section sidebar-nav-mini-hide">
  <div class="sidebar-separator push">
    <i class="fa fa-ellipsis-h"></i>
  </div>
</div>
<!-- END Color Themes -->
</div>
<!-- END Sidebar Content -->
</div>
<!-- END Wrapper for scrolling functionality -->
