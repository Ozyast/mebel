<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Доступ закрыт";
?>

<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="block">

      <form action="<?= Url::to([$url]) ?>" method="post" class="form-horizontal form-bordered">
        <div class="form-group <?= isset($message) ? 'has-error' : '' ?>">
          <label class="col-md-4 control-label" for="pass">Ключ доступа</label>
          <div class="col-md-8">
            <input type="password" id="pass" name="pass" class="form-control">
            <?php if (isset($message)): ?>
              <span id="pass" class="help-block animation-slideDown"><?= $message ?></span>
            <?php else: ?>
              <span class="help-block">Введите ключ доступа к странице</span>
            <?php endif; ?>
          </div>
        </div>
        <div class="form-group form-actions">
          <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-effect-ripple btn-primary">Отправить</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>
