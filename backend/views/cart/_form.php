<?php
// Имя формы
$table_name = "form-cart";
// ID редактируемой записи
$id_table_name = 'id_cart';
$id_table = $form[$id_table_name];

$status = [
  "Ожидает проверки менеджером",
];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
              <label class="col-md-3 control-label" for="category">Категории</label>
              <div class="col-md-9">
                <select id="category" name="category" class="select-chosen" data-placeholder="Выберите категорию">
                  <?php foreach ($status as $sta): ?>
                    <option value="<?= $sta ?>" <?= $form['status']==$sta ? "selected":"" ?>><?= $sta ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="price">Цена</label>
                <div class="col-md-3">
                    <input type="text" id="price" name="price" class="form-control"  placeholder="Цена" value="<?= $form['price'] ?>">
                </div>
                <label class="col-md-3 control-label" for="sale">Скидка</label>
                <div class="col-md-3">
                    <input type="text" id="sale" name="sale" class="form-control"  placeholder="Скидка" value="<?= $form['sale'] ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="price_total">Сумма</label>
                <div class="col-md-9">
                    <input type="text" id="price_total" name="price_total" class="form-control"  placeholder="Сумма" value="<?= $form['price_total'] ?>">
                </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="user_name">Имя</label>
              <div class="col-md-3">
                <input type="text" id="user_name" name="user_name" class="form-control"  placeholder="Имя пользователя" value="<?= $form['user_name'] ?>">
              </div>
              <label class="col-md-2 control-label" for="user_phone">Телефон</label>
              <div class="col-md-4">
                <input type="text" id="user_phone" name="user_phone" class="form-control"  placeholder="Телефон пользователя" value="<?= $form['user_phone'] ?>">
              </div>
            </div>


            <div class="form-group">
              <label class="col-md-3 control-label" for="user_address">Адрес</label>
              <div class="col-md-9">
                <input type="text" id="user_address" name="user_address" class="form-control"  placeholder="Адрес пользователя" value="<?= $form['user_address'] ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="comment">Комментарий</label>
              <div class="col-md-9">
                <textarea id="comment" name="comment" class="form-control"  placeholder="Комментарий"><?= $form['comment'] ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="delivery">Доставка</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="delivery"><input id="delivery" type='checkbox' <?= $form['delivery'] ? "checked" : "" ?>><span></span></label>
              </div>
              <label class="col-md-3 control-label" for="paid-up">Оплачен</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="paid-up"><input id="paid-up" type='checkbox' <?= $form['paid-up'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="done">Завершен</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="done"><input id="done" type='checkbox' <?= $form['done'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
