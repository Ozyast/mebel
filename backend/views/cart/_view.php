<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}

foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_cart'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="sale">
    <td class='text-center'>
      <?= $id ?>
    </td>

    <td>
      <?= $row['status'] ?>
    </td>

    <td>
      <?= $row['price'] ?>
      <br/>
      <span class="text-danger"><?= $row['sale'] ?><span>
    </td>

    <td>
      <?= $row['price_total'] ?>
      <a href="<?= Url::to(["cartitem/index", 's'=>$id, 'row'=>'id_cart', 't'=>'i']) ?>">
        (<?= $row->getCartitem()->count() ?>)
      </a>
    </td>

    <td>
      <?= $row['user_name'] ?><br/>
      <?= $row['user_phone'] ?><br/>
      <span class="text-info"><?= $row['user_address'] ?><span><br/>
    </td>

    <td>
      <?= $row['comment'] ?><br/>
    </td>

    <td class='text-muted'>
      <small rel="tooltip" title="Создан: <?= date_format(date_create($row['created_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['created_at']), "d.m.y") ?>
      </small> <br/>
      <small rel="tooltip" title="Изменен: <?= date_format(date_create($row['updated_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['updated_at']), "d.m.y") ?>
      </small>
    </td>

    <td class='text-center'>
      <label class='switch switch-success data-checkbox' id='done'>
        <input type='checkbox' id="done" <?= $row["done"] ? "checked" : "" ?>>
        <span></span>
      </label>
    </td>

    <td class='text-center'>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <?php if (!$row['done']): ?>
        <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>
      <?php endif; ?>
      <!-- <div class="border-bottom" style="margin:5px 0;"></div>
      <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button>
      <button id='data-refresh' title='Проверить настройки' class='btn btn-effect-ripple btn-xs btn-info'><i class='fa fa-refresh'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
