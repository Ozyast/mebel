<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}

foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_cart'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="sale">
    <td class='text-center'>
      <a href="<?= Url::to(["cart/index", 's'=>$id, 'row'=>'id_cart', 't'=>'i']) ?>">
        <?= $id ?>
      </a>
    </td>

    <td>
      <?= $row['status'] ?>
    </td>

    <td>
      <?= $row['price'] ?>
      <br/>
      <span class="text-danger"><?= $row['sale'] ?><span>
    </td>

    <td>
      <?= $row['price_total'] ?>
      <a href="<?= Url::to(["cartitem/index", 's'=>$id, 'row'=>'id_cart', 't'=>'i']) ?>">
        (<?= $row->getCartitem()->count() ?>)
      </a>
    </td>

    <td>
      <?= $row['user_name'] ?><br/>
      <?= $row['user_phone'] ?><br/>
      <span class="text-info"><?= $row['user_address'] ?><span><br/>
    </td>

    <td>
      <?= $row['comment'] ?><br/>
    </td>

    <td class='text-muted'>
      <small rel="tooltip" title="Создан: <?= date_format(date_create($row['created_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['created_at']), "d.m.y") ?>
      </small> <br/>
      <small rel="tooltip" title="Изменен: <?= date_format(date_create($row['updated_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['updated_at']), "d.m.y") ?>
      </small>
    </td>

  </tr>

<?php } ?>
