<?php
// Имя формы
$table_name = "form-colors";
// ID редактируемой записи
$id_table_name = 'id_colors';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
                <label class="col-md-3 control-label" for="name">Имя</label>
                <div class="col-md-8">
                    <input type="text" id="name" name="name" class="form-control"  placeholder="Имя" value="<?= $form['name'] ?>">
                </div>
            </div>

            <?php if (!strlen($form['image'])): ?>
              <div class="form-group">
                <label class="col-md-3 control-label" for="image">Фото</label>
                <div class="col-md-8">
                  <div class="dropzone dz-clickable dropzone-in-form" id="image">
                    <div class="dz-default dz-message">
                      <span>Перенесите фото сюда для загрузки</span>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

            <div class="form-group">
              <label class="col-md-3 control-label" for="main_color">Основной цвет</label>
              <div class="col-md-8">
                <select id="main_color" name="main_color" class="select-chosen" data-placeholder="Выберите основной цвет">
                  <option value='0'>Нет основного цвета</option>
                  <?php if (count($colors) >= 1 && is_array($colors)): ?>
                    <?php foreach ($colors as $col): ?>
                      <option value="<?= $col['id_colors'] ?>" <?= $col['id_colors']==$form['main_color'] ? "selected":"" ?>><?= $col['name'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
