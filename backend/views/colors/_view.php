<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}

foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_colors'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="furnitures">
    <td class='text-center'>
      <?= $id ?>
    </td>

    <td>
      <?php if (strlen($row['image'])): ?>
        <img src="/img/colors/<?= $row['image'] ?>" class="img-responsive">
      <?php else: ?>
        <i class="fa fa-file-image-o push-bit-top text-muted"></i>
      <?php endif; ?>
    </td>

    <td>
      <?= $row['name'] ?>
    </td>

    <td class='text-center'>
      <?= $row['main_color'] ?>
    </td>

    <td class='text-center'>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>
      <?php if (strlen($row['image'])): ?>
        <button id='data-delete-image' title='Удалить изображение' class='btn btn-effect-ripple btn-xs btn-warning'><i class='fa fa-file-image-o'></i></button>
      <?php endif; ?>
      <!-- <div class="border-bottom" style="margin:5px 0;"></div>
      <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button>
      <button id='data-refresh' title='Проверить настройки' class='btn btn-effect-ripple btn-xs btn-info'><i class='fa fa-refresh'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
