<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">

    <form id='form_articles_file' data-id="<?= $articles->id_articles ?>" class="form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

          <div class="form-group">
            <label class="control-label" for="text">Текст</label>
            <div class="">
              <textarea id="text" name="text" class="form-control" rows="20" placeholder="Текст"><?= $text ?></textarea>
              <span class="help-block"><?= $file_src ?></span>
            </div>

          </div>

        </fieldset>
    </form>

</div>
<div class="modal-footer">
    <button form="form_articles_file" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
