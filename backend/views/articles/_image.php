<?php
use yii\helpers\Url;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form action="/admin/articles/imageupload?id=<?= $articles->id_articles ?>" class="dropzone dz-clickable">
      <div class="dz-default dz-message">
        <span>Перенесите фото сюда для загрузки</span>
      </div>
    </form>

    <?php if (count($image)): ?>
      <div class="row gallery push-top">

        <?php foreach ($image as $src): ?>
          <div class="col-sm-4">
            <span>/img/articles/<?= $articles->id_articles."/".$src ?></span>
            <div class="gallery-image-container animation-fadeInQuick" style="min-height:20px;">
              <img src="/img/articles/<?= $articles->id_articles."/".$src ?>" alt="Image">
              <div class="gallery-image-options" data-src="<?= $articles->id_articles."/".$src ?>">
                <a href="/img/articles/<?= $articles->id_articles."/".$src ?>" class="gallery-link btn btn-primary" data-toggle="lightbox-image">
                  <i class="fa fa-search-plus"></i>
                </a>
                <button id='data-delete-photo' class="btn btn-effect-ripple btn-danger" title="Удалить"><i class="fa fa-times"></i></button>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

      </div>
    <?php endif; ?>
</div>
<div class="modal-footer">
    <!-- <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button> -->
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
