<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;
use yii\helpers\Html;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}



foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_articles'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="seo">
    <td>
      <?= $id ?>
    </td>

    <td>
      <?= $row['menu_title'] ?>
    </td>

    <td>
      <a href="/articles/<?= $row['menu_url'] ?>?visible=1">
        <?= $row['menu_url'] ?>
      </a>
    </td>

    <td>
      <?= $row['file_name'] ?>
    </td>

    <td class='text-muted'>
      <small rel="tooltip" title="Создан: <?= date_format(date_create($row['created_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['created_at']), "d.m.y") ?>
      </small> <br/>
      <small rel="tooltip" title="Изменен: <?= date_format(date_create($row['updated_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['updated_at']), "d.m.y") ?>
      </small>
    </td>

    <td>
      <label class='switch switch-success data-checkbox' id='visible'>
        <input type='checkbox' id='visible' <?= $row["visible"] ? "checked" : "" ?>>
        <span></span>
      </label>
    </td>

    <td>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>
      <div class="border-bottom" style="margin:5px 0;"></div>
      <button id='data-image' title='Добавить фото' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-image'></i></button>
      <button id='data-file' title='Добавить текст' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-file'></i></button>
      <!-- <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
