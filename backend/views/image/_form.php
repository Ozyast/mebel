<?php
// Имя формы
$table_name = "form-image";
// ID редактируемой записи
$id_table_name = 'id_items_image';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <?php if (!strlen($form['src'])): ?>
              <div class="form-group">
                <label class="col-md-3 control-label" for="src">Фото</label>
                <div class="col-md-8">
                  <div class="dropzone dz-clickable dropzone-in-form" id="src">
                    <div class="dz-default dz-message">
                      <span>Перенесите фото сюда для загрузки</span>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

            <div class="form-group">
              <label class="col-md-3 control-label" for="color">Цвет</label>
              <div class="col-md-8">
                <select id="color" name="color" class="select-chosen" data-placeholder="Выберите коллекцию">
                  <?php if (count($colors) < 1 or !is_array($colors)): ?>
                    <option value='null'>Ошибка загрузки</option>
                  <?php else: ?>
                    <option value='0' <?= $col["id_colors"] == $form['color'] ? "selected" : ""?>>Нет цвета</option>
                    <?php foreach ($colors as $col): ?>
                      <option value="<?= $col['id_colors'] ?>" <?= $col["id_colors"] == $form['color'] ? "selected" : ""?>><?= $col['id_colors'] ?>: <?= $col['name'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="id_items">Товар</label>
                <div class="col-md-8">
                    <input type="text" id="id_items" name="id_items" class="form-control"  placeholder="Товар" value="<?= $form['id_items'] ?>">
                </div>
            </div>

            <?php if (strlen($form['src'])): ?>
              <div class="form-group">
                <div class="col-md-7 col-md-offset-1">
                  <img src="/img/items/<?= str_replace("/", "/@", $form['src']) ?>" alt="Photo">
                </div>
              </div>
            <?php endif; ?>


        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
