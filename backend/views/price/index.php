<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Прайсы";
$file = mb_convert_encoding($file, 'utf-8', 'cp1251');
$file = explode("\n", $file);
?>
<script src="<?= Yii::$app->request->baseUrl ?>/js/page/price.js"></script>


<button id='data-save' type="button" class="btn btn-effect-ripple btn-primary">Сохранить</button>
<a href="<?= Url::to(['price/file']) ?>" type="button" class="btn btn-effect-ripple btn-info pull-right">Новый прайс</a><br/><br/>

<table class="table table-vcenter table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th></th>
      <?php $row = explode(";", $file[0]);
          foreach ($row as $key => $col) : ?>
        <th id="<?= ++$key ?>">
          <small><i class="gi gi-bin text-muted" id="data-delete-cols"></i></small>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($file as $key => $row) :
      $row = explode(";", $row); ?>
      <tr>
        <td>
          <i class="gi gi-bin text-muted" id="data-delete-rows"></i>
        </td>
        <?php foreach ($row as $key => $col) : ?>
          <td>
            <?= $col ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
