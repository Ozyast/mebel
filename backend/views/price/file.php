<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Новый прайс";
$file = mb_convert_encoding($file, 'utf-8', 'cp1251');
$file = explode("\n", $file);
?>
<script src="<?= Yii::$app->request->baseUrl ?>/js/page/price.js"></script>

<a href="<?= Url::to(["price/index"]) ?>" type="button" class="btn btn-effect-ripple btn-primary">Открыть файл</a><br/><br/>

<div class="block">
  <form action="/admin/price/file?update=1" method="POST" class="dropzone dz-clickable">
    <div class="dz-default dz-message">
      <span>Перенесите файл сюда для загрузки</span>
    </div>
  </form><br/>
</div>
