<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}

foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_cart_item'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="sale">
    <td class='text-center'>
      <?= $id ?>
    </td>

    <td>
      <?php if (strlen($row['image'])): ?>
        <img src="/img/items/<?= $row['image'] ?>" class="img-responsive">
      <?php else: ?>
        <i class="fa fa-file-image-o push-bit-top text-muted"></i>
      <?php endif; ?>
    </td>

    <td>
      <a href="<?= Url::to(["items/index", 's'=>$row['id_items'], 'row'=>'id_items', 't'=>'i']) ?>">
        <?= $row['title'] ?>
      </a>
    </td>

    <td>
      <?= $row['price'] ?>
      <br/>
      <span class="text-danger"><?= $row['price_sale'] ?><span>
    </td>

    <td>
      <?= $row['count'] ?>
    </td>

    <td>
      <a href="<?= Url::to(["cart/index", 's'=>$row['id_cart'], 'row'=>'id_cart', 't'=>'i']) ?>">
        <?= $row['id_cart'] ?>
      </a>
    </td>

    <td>
      <?php if (isset($row['colors'])): ?>
        <img src="/img/colors/<?= $row['colors']['image'] ?>" class="img-responsive img-circle" title="<?= $row['colors']['name'] ?>" rel="tooltip">
      <?php else: ?>
        <i class="fa fa-file-image-o push-bit-top text-muted"></i>
      <?php endif; ?>
    </td>

    <td class='text-muted'>
      <small rel="tooltip" title="Создан: <?= date_format(date_create($row['created_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['created_at']), "d.m.y") ?>
      </small> <br/>
      <small rel="tooltip" title="Изменен: <?= date_format(date_create($row['updated_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['updated_at']), "d.m.y") ?>
      </small>
    </td>

    <td class='text-center'>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>
      <!-- <div class="border-bottom" style="margin:5px 0;"></div>
      <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button>
      <button id='data-refresh' title='Проверить настройки' class='btn btn-effect-ripple btn-xs btn-info'><i class='fa fa-refresh'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
