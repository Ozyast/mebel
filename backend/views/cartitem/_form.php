<?php
// Имя формы
$table_name = "form-cart";
// ID редактируемой записи
$id_table_name = 'id_cart_item';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

          <div class="form-group">
            <label class="col-md-3 control-label" for="colors">Цвет</label>
            <div class="col-md-9">
              <select id="colors" name="colors" class="select-chosen" data-placeholder="Выберите цвета">
                <?php if (count($colors) < 1 or !is_array($colors)): ?>
                  <option value='null'>Ошибка загрузки</option>
                <?php else: ?>
                  <?php foreach ($colors as $col): ?>

                    <option value="<?= $col['id_colors'] ?>" <?= $col['id_colors']==$form['color'] ? "selected":"" ?>><?= $col['name'] ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label" for="price">Цена</label>
            <div class="col-md-3">
                <input type="text" id="price" name="price" class="form-control"  placeholder="Цена" value="<?= $form['price'] ?>">
            </div>
            <label class="col-md-3 control-label" for="price_sale">Скидка</label>
            <div class="col-md-3">
                <input type="text" id="price_sale" name="price_sale" class="form-control"  placeholder="Скидка" value="<?= $form['price_sale'] ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label" for="count">Кол-во</label>
            <div class="col-md-9">
                <input type="text" id="count" name="count" class="form-control"  placeholder="Кол-во" value="<?= $form['count'] ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label" for="id_items">Товар</label>
            <div class="col-md-3">
              <input type="text" id="id_items" name="id_items" class="form-control"  placeholder="Товар" value="<?= $form['id_items'] ?>">
            </div>
            <label class="col-md-2 control-label" for="id_cart">Корзина</label>
            <div class="col-md-4">
              <input type="text" id="id_cart" name="id_cart" class="form-control"  placeholder="Корзина" value="<?= $form['id_cart'] ?>">
            </div>
          </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
