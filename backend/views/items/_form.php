<?php
use app\models\Helper;

// Имя формы
$table_name = "form-items";
// ID редактируемой записи
$id_table_name = 'id_items';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form['id_items']) {
  // $form['title'] = "Шкаф-купе КП-222";
  // $form['price'] = "17 909";
  $form['sold'] = 1;
  $form['instock'] = 0;
  // Проверим последний добавленный товар и скопируем его данные
  if ($itemsLast) {
    $col_active = Helper::indexByKey($itemsLast->colors, "id_colors");
    $cat_active = Helper::indexByKey($itemsLast->category, "id_category");
    $form['id_manufacturer'] = $itemsLast['id_manufacturer'];
    $form['price'] = $itemsLast['price'];
    $form['instock'] = $itemsLast['instock'];
    $form['sold'] = $itemsLast['sold'];
    $form['instock_color'] = $itemsLast['instock_color'];
    $form['id_collections'] = $itemsLast['id_collections'];
  }
}


$form['image'] = explode(",", $form['image']);
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
              <label class="col-md-3 control-label" for="title">Заголовок</label>
              <div class="col-md-8">
                  <input type="text" id="title" name="title" class="form-control"  placeholder="Имя" value="<?= $form['title'] ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="price">Цена</label>
              <div class="col-md-3">
                  <input type="text" id="price" name="price" class="form-control"  placeholder="Цена" value="<?= $form['price'] ?>">
              </div>
              <label class="col-md-2 control-label" for="sale_price">Скидка</label>
              <div class="col-md-3">
                  <input type="text" id="sale_price" name="sale_price" class="form-control"  placeholder="Скидка" value="<?= $form['sale_price'] ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="instock">Колличество</label>
              <div class="col-md-2">
                  <input type="number" id="instock" name="instock" class="form-control" min="0" placeholder="Колличество" value="<?= $form['instock'] ?>" <?= !$form['sold'] && !$form['instock'] || $form['instock_color'] ? "disabled" : "" ?>>
              </div>
              <div class="col-md-3">
                <label class="control-label" for="sold_out">Бесконечное</label>
                <label class='csscheckbox csscheckbox-success' id="sold_out"><input id="sold_out" type='checkbox' <?= !$form['sold'] && !$form['instock'] && !$form['instock_color'] ? "checked" : "" ?> <?= $form['instock_color'] ? "disabled" : ""?>><span></span></label>
              </div>
              <div class="col-md-3">
                <label class="control-label" for="instock_color">По цветам</label>
                <label class='csscheckbox csscheckbox-success' id="instock_color"><input id="instock_color" type='checkbox' <?= $form['instock_color'] ? "checked" : "" ?> <?= !$form['sold'] && !$form['instock'] && !$form['instock_color'] ? "disabled" : ""?>><span></span></label>
              </div>

              <input type="hidden" id="sold" name="sold" value="<?= $form['sold'] ?>">
            </div>

            <?php if (isset($image) && is_array($image)): ?>
              <div class="form-group">
                <label class="col-md-3 control-label" for="image">Фото</label>
                <div class="col-md-8">
                  <select id="image" name="image" class="select-chosen" data-placeholder="Выберите главное фото" multiple max="2">
                      <?php foreach ($image as $img): ?>
                        <option value="<?= $img['src'] ?>" <?= in_array($img['src'], $form['image']) ? "selected":"" ?>><?= $img['src'] ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
              </div>
            <?php endif; ?>

            <div class="form-group">
              <label class="col-md-3 control-label" for="colors">Цвет</label>
              <div class="col-md-8">
                <select id="colors" name="colors" class="select-chosen" data-placeholder="Выберите цвета" multiple>
                  <?php if (count($colors) < 1 or !is_array($colors)): ?>
                    <option value='null'>Ошибка загрузки</option>
                  <?php else: ?>
                    <?php foreach ($colors as $col): ?>

                      <option value="<?= $col['id_colors'] ?>" <?= isset($col_active[$col['id_colors']]) ? "selected":"" ?>><?= $col['name'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="id_collections">Коллекция</label>
              <div class="col-md-8">
                <select id="id_collections" name="id_collections" class="select-chosen" data-placeholder="Выберите коллекцию">
                  <option value='0'>Нет коллекции</option>
                  <?php if (count($collections) >= 1 && is_array($collections)): ?>
                    <?php foreach ($collections as $col): ?>
                      <option value="<?= $col['id_collections'] ?>" <?= $col['id_collections']==$form['id_collections'] ? "selected":"" ?>><?= $col['name'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="category">Категория</label>
              <div class="col-md-8">
                <select id="category" name="category" class="select-chosen" data-placeholder="Выберите категории" multiple>
                  <?php if (count($category) < 1 or !is_array($category)): ?>
                    <option value='null'>Ошибка загрузки</option>
                  <?php else: ?>
                    <?php foreach ($category as $cat): ?>
                      <option value="<?= $cat['id_category'] ?>" <?= isset($cat_active[$cat['id_category']]) ? "selected":"" ?>><?= $cat['name_ru'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <?php if (Yii::$app->params['siteSubCategory']): ?>
              <div class="form-group">
                <label class="col-md-3 control-label" for="subcategory">Подкатегория</label>
                <div class="col-md-8">
                  <select id="subcategory" name="subcategory" class="select-chosen" data-placeholder="Выберите подкатегорию" multiple>
                    <?php if (count($subcategory) < 1 or !is_array($subcategory)): ?>
                      <option value='null'>Ошибка загрузки</option>
                    <?php else: ?>
                      <?php foreach ($subcategory as $sub): ?>
                        <option value="<?= $sub['id_subcategory'] ?>" <?= isset($sub_active[$int['id_subcategory']]) ? "selected":"" ?>><?= $sub['name_ru'] ?></option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select>
                </div>
              </div>
            <?php endif ?>

            <div class="form-group">
              <label class="col-md-3 control-label" for="id_manufacturer">Производитель</label>
              <div class="col-md-8">
                <select type="text" id="id_manufacturer" name="id_manufacturer" class="form-control" value="">
                  <?php if (count($manufacturer) < 1 or !is_array($manufacturer)): ?>
                    <option value='null'>Ошибка загрузки</option>
                  <?php else: ?>
                    <?php foreach ($manufacturer as $man): ?>
                      <option value="<?= $man['id_manufacturer'] ?>" <?= $man["id_manufacturer"] == $form['id_manufacturer'] ? "selected" : ""?>><?= $man['name'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="visible_catalog">Показывать в каталоге</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="visible_catalog"><input id="visible_catalog" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
              <label class="col-md-3 control-label" for="visible">Показывать</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>

            <?php if ($id_table): ?>
              <div id="description" class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="panel-title">
                      <i class="fa fa-angle-right"></i>
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#description" href="#desc" aria-expanded="true">
                        <strong>Добавить характеристики</strong>
                      </a>
                    </div>
                  </div>
                  <div id="desc" class="panel-collapse collapse" aria-expanded="true">
                    <div class="panel-body">
                      <div class="form-group">
                        <label class="col-md-3 control-label" for="description">Описание</label>
                        <div class="col-md-8">
                          <textarea rows="5" id="description" name="description" class="form-control"  placeholder="Описание"><?php
                              if (isset($form->description['description']))
                                $desc = json_decode(trim($form->description['description']));
                                if (is_object($desc))
                                  foreach ($desc as $key => $value) {
                                    echo $key.":".$value."\n";
                                  }
                              ?></textarea>
                              <input type="hidden" id="description_change" name="description_change" value="0">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
