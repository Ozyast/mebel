<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}

foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_items'];
  if (strlen($row['image']))
    $image = explode(",", $row['image']);
  ?>

  <tr data-tr-id='<?= $id ?>' id="items">
    <td class='text-center'>
      <a href="<?= Yii::$app->urlManagerFrontend->createUrl(["items/view", 'id'=>$id]); ?>">
        <?= $id ?>
      </a>
    </td>

    <td class="remove-padding">
      <?php if (!empty($image)): ?>
        <div id="info-carousel<?=$id?>" class="carousel slide remove-margin">
          <ol class="carousel-indicators">
            <?php foreach ($image as $key => $img): ?>
              <li data-target="#info-carousel<?=$id?>" data-slide-to="<?= $key ?>" class="<?= !$key ? 'active' : '' ?>"></li>
            <?php endforeach; ?>
          </ol>

          <div class="carousel-inner">
            <?php foreach ($image as $key => $img): ?>
              <div class="item <?= !$key ? 'active' : '' ?>">
                <img src="/img/items/<?= str_replace("/", "/@", $img) ?>" alt="image">
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      <?php else: ?>
        <i class="fa fa-file-image-o push-bit-top text-muted"></i>
      <?php endif; ?>
    </td>

    <td>
      <?= $row['title'] ?>
    </td>

    <td>
      <?php if ( $row['sale'] ): ?>
        <?= $row['sale_price'] ?> р.<br/>
        <del class="text-muted"><?= $row['price'] ?> р.</del>
      <?php else: ?>
        <?= $row['price'] ?> р.
      <?php endif; ?>
    </td>

    <td>
      <?php if ( $row['colors'] ): ?>
        <?php foreach ($row->colors as $key => $color): ?>
          <a href="<?= Url::to(["colors/index", 's'=>$color['id_colors'], 'row'=>'id_colors', 't'=>'i']) ?>">
            <img src="/img/colors/<?= $color->image ?>" width="20">
          </a>
        <?php endforeach; ?>
      <?php endif; ?>
    </td>

    <td>
      <?php if ( $row['category'] ): ?>
        <?php foreach ($row->category as $key => $cat): ?>
          <?= $key ? "," : "" ?>
          <a href="<?= Url::to(["category/index", 's'=>$cat['id_category'], 'row'=>'id_category', 't'=>'i']) ?>">
            <?= $cat['name_ru'] ?>
          </a>
        <?php endforeach; ?>
      <?php endif; ?>
    </td>

    <?php if (Yii::$app->params['siteSubCategory']): ?>
      <td>
        <?php if ( $row['subcategory'] ): ?>
          <?php foreach ($row['subcategory'] as $key => $int): ?>
            <?= $int['name_ru'] . ',' ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </td>
    <?php endif; ?>

    <td>
      <a href="<?= Url::to(["collections/index", 's'=>$row['id_collections'], 'row'=>'id_collections', 't'=>'i']) ?>">
        <?= $row->collections->name ?>
      </a><br/>
      Модули: <?= count($row->modules) ?>
    </td>

    <td>
      <a href="<?= Url::to(["manufacturer/index", 's'=>$row['id_manufacturer'], 'row'=>'id_manufacturer', 't'=>'i']) ?>">
        <?= $row['manufacturer']["name"] ?>
      </a>
    </td>

    <td class='text-center text-muted'>
      <small rel="tooltip" title="Создан: <?= date_format(date_create($row['created_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['created_at']), "d.m.y") ?>
      </small> <br/>
      <small rel="tooltip" title="Изменен: <?= date_format(date_create($row['updated_at']), "d.m.Y H:i:s") ?>">
        <?= date_format(date_create($row['updated_at']), "d.m.y") ?>
      </small>
    </td>

    <td class='text-center'>
      <label class='switch switch-success data-checkbox' id='visible'>
        <input type='checkbox' id='visible' <?= $row["visible"] ? "checked" : "" ?>>
        <span></span>
      </label>
    </td>

    <td class='text-center'>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>
      <!-- <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button> -->
      <button id='data-image' title='Добавить фото' class='btn btn-effect-ripple btn-xs btn-info'><i class='fa fa-image'></i></button>
      <div class="border-bottom" style="margin:5px 0;"></div>

      <div class="btn-group">
        <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="true">
          <i class="gi gi-cogwheels"></i> Еще <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right text-left">

          <li>
            <a href="<?= Url::to(['image/index', 's'=>$row['id_items'], 'row'=>'id_items', 't'=>'i']) ?>">
              <i class="fa fa-file-image-o pull-right"></i>
              Фото товара
            </a>
          </li>

          <li>
            <a href="<?= Url::to(['similar/index', 's'=>$row['id_items'], 'row'=>'id_items_master;id_items_slave', 't'=>'i']) ?>">
              <i class="gi gi-random pull-right"></i>
              Похожие товары
            </a>
          </li>

          <?php if ($row['instock_color']): ?>
            <li>
              <a href="<?= Url::to(['colorsrelation/index', 's'=>$row['id_items'], 'row'=>'id_items', 't'=>'i']) ?>">
                <i class="gi gi-brush pull-right"></i>
                Кол-во по цвету
              </a>
            </li>
          <?php endif; ?>

          <?php if ($row['id_collections']): ?>
            <li>
              <a href="<?= Url::to(['modulesrelation/index', 'id'=>$row['id_items']]) ?>">
                <i class="gi gi-brush pull-right"></i>
                Модули
              </a>
            </li>
          <?php endif; ?>

          <?php if ($row->manufacturer->name === 'Лером'): ?>
            <li>
              <a href="javascript:void(0)" id="data-price-recount">
                <i class="gi gi-roundabout pull-right"></i>
                Пересчитать
              </a>
            </li>
          <?php endif; ?>

          <!-- <li class="divider"></li> -->
        </ul>
      </div>
    </td>
  </tr>
<?php } ?>
