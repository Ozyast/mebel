<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body" data-tr-id="<?= $items->id_items ?>">
    <form action="/admin/items/imageupload?id=<?= $items->id_items ?>" class="dropzone dz-clickable">
      <div class="dz-default dz-message">
        <span>Перенесите фото сюда для загрузки</span>
      </div>
    </form>

    <div class="row gallery push-top">

      <?php
      echo $this->render("_image_item", [
        "items" => $items,
        "image" => $image,
      ]);
      ?>

    </div>
</div>
<div class="modal-footer">
    <!-- <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button> -->
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
