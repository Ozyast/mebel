<?php
$itemsImage = explode(",", $items->image);
?>

<?php if (count($image))
  foreach ($image as $img): ?>
  <div class="col-sm-3" data-id="<?= $img['id_items_image'] ?>" id="images">
    <div class="gallery-image-container animation-fadeInQuick" style="min-height:90px;">
      <img src="/img/items/<?= str_replace("/", "/@", $img['src']) ?>" alt="Image">

      <div class="gallery-image-options">
        <a href="/img/items/<?= str_replace("/", "/@", $img['src']) ?>" class="gallery-link btn btn-primary" data-toggle="lightbox-image" title="Image Info"><i class="fa fa-search-plus"></i></a>
        <?php if (in_array($img['src'], $itemsImage)): ?>
          <button id='data-image-main' class="btn btn-effect-ripple btn-success" title="Установить на главное фото"><i class="fa fa-check"></i></button>
        <?php else: ?>
          <button id='data-image-main' class="btn btn-effect-ripple btn-default" title="Убрать фото с главной"><i class="fa fa-check"></i></button>
        <?php endif; ?>
        <button id='data-delete-photo' class="btn btn-effect-ripple btn-danger" title="Удалить"><i class="fa fa-times"></i></button>
      </div>
    </div>
  </div>
<?php endforeach; ?>
