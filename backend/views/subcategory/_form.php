<?php
// Имя формы
$table_name = "form-subcategory";
// ID редактируемой записи
$id_table_name = 'id_subcategory';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
} else {
  $form['category'] = json_decode($form['category']);
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
                <label class="col-md-3 control-label" for="name_ru">Имя</label>
                <div class="col-md-8">
                    <input type="text" id="name_ru" name="name_ru" class="form-control"  placeholder="Имя" value="<?= $form['name_ru'] ?>">
                </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="category">Категории</label>
              <div class="col-md-8">
                <select id="category" name="category" class="select-chosen" data-placeholder="Выберите категорию" multiple>
                  <?php if (count($category) < 1 or !is_array($category)): ?>
                    <option value='null'>Ошибка загрузки</option>
                  <?php else: ?>
                    <?php foreach ($category as $cat): ?>
                      <option value="<?= $cat['id_category'] ?>" <?= is_array($form['category']) && in_array($cat['id_category'], $form['category']) ? "selected":"" ?>><?= $cat['name_ru'] ?></option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select>
              </div>
            </div>

            <?php if (!strlen($form['image'])): ?>
              <div class="form-group">
                <label class="col-md-3 control-label" for="image">Фото</label>
                <div class="col-md-8">
                  <div class="dropzone dz-clickable dropzone-in-form" id="image">
                    <div class="dz-default dz-message">
                      <span>Перенесите фото сюда для загрузки</span>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

            <div class="form-group">
              <label class="col-md-3 control-label" for="visible">Показывать</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
