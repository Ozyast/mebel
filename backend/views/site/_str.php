<?php
/*
 * Входные данные:
 * $pages - Массив с данными полученный из финкции Helper::pages();
 */
$length = $pages['length'];
$url = $pages['url'];
$str = $pages['str'];

/*
 * Настройка
 */
// Сколько страниц выводить
$count_str = 6;
?>

<div class='pagination'>
   <ul class='pagination pagination-lg hidden-xs'>
      <?php

      $back_disabled = $str <= 1 ? "disabled" : "";
      $next_disabled = $str >= $length ? "disabled" : "";
      $back = $str - 1;
      $next = $str + 1;

      echo "<li class='next $back_disabled'><a href='".str_replace("{{page}}", $back, $url)."' rel='tooltip' title='Страница: $back ($length)'><i class='icon-angle-left'></i> Назад</a></li>";

      //<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
      // Если страниц больше одной
      if ($length > 1) {

         // Найдем крайнюю левую и правую страницу страницу
         // Если активная 4 страница, а $count_str=4 то крайняя левая = 2 а правая = 6
         $str_left = $str - ceil($count_str / 2);
         $str_right = $str + ceil($count_str / 2);
         if ($str_left <= 1)
            $lim_str_left = $str;
         else
            $lim_str_left = $str_left;
         if ($str_right > $length)
            $lim_str_right = $length;
         else
            $lim_str_right = $str_right;


         // Если открыта страница дальше чем $count_str (7)
         if ($str >= $count_str) {
            // Выведем страницу 1
            $href = str_replace("{{page}}", 1, $url);
            echo "<li $active><a href='$href'>1</a></li>";
            if ($str_left != 2)
               echo "<li class='disabled'><a>..</a></li>";
         }

         //Если активная страница больше чем $count_str (7)
         if (($str >= $count_str) and ($length > $count_str)) {

            for ($i = $lim_str_left; $i <= $lim_str_right; $i++) {
               $active =  $i == $str ? "class='active'" : "";
               $href = str_replace("{{page}}", $i, $url);
               echo "<li $active><a href='$href'>$i</a></li>";
            }
            //Если активная страниц меньше чем $count_str (9)
         } else {
            for ($i = 1; $i <= $count_str and $i <= $length; $i++) {
               $active =  $i == $str ? "class='active'" : "";
               $href = str_replace("{{page}}", $i, $url);
               echo "<li $active><a href='$href'>$i</a></li>";
            }
         }

         // Если открыта страница меньше чем $count_str (7)
         if ($str_right < $length) {
            // Выведем последнюю страницу
            if ($str_right != $length - 1)
               echo "<li class='disabled'><a>..</a></li>";
            $href = str_replace("{{page}}", $length, $url);
            echo "<li><a href='$href'>$length</a></li>";
         }
      }
      // Если страница всего одна
      else {
         echo "<li class='active'><a " . str_replace("{{page}}", 1, $url) . ">1</a></li>";
      }
      ?>

      <li class='previous <?= $next_disabled ?>'><a href='<?= str_replace("{{page}}", $next, $url) ?>' rel='tooltip' title='Страница: <?= $next ?>(<?= $length ?>)'>Вперед <i class='icon-angle-right'></i></a></li>
   </ul>
</div>
