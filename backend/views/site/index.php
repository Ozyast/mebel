<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Главная";
?>
<script src="<?= Yii::$app->request->baseUrl ?>/js/page/index.js"></script>

<div class="row">
    <!-- Simple Stats Widgets -->
    <div class="col-sm-6 col-lg-3">
        <a href="<?= Url::to(["contact/index"]) ?>" class="widget">
            <div class="widget-content widget-content-mini text-right clearfix">
                <div class="widget-icon pull-left <?= $count['contact'] ? "themed-background" : "" ?>">
                    <i class="fa fa-comment text-light-op"></i>
                </div>
                <h2 class="widget-heading h3 <?= $count['contact'] ? "" : "text-muted" ?>">
                    <strong><?= $count['contact'] ?></strong>
                </h2>
                <span class="text-muted">Контакты</span>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-3">
        <a href="<?= Url::to(["cart/index"]) ?>" class="widget">
            <div class="widget-content widget-content-mini text-right clearfix">
                <div class="widget-icon pull-left <?= $count['cart_on_check'] ? "themed-background-success" : "" ?>">
                    <i class="gi gi-cart_in text-light-op"></i>
                </div>
                <h2 class="widget-heading h3 <?= $count['cart_on_check'] ? "text-success" : "text-muted" ?>">
                    <?php if ($count['cart_on_check'] ): ?>
                      <strong>+ <?= $count['cart_on_check'] ?></strong>
                    <?php else: ?>
                      <span>0<span>
                    <?php endif; ?>
                </h2>
                <span class="text-muted">Новые заказы</span>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-3">
        <a href="<?= Url::to(["cart/index"]) ?>" class="widget">
            <div class="widget-content widget-content-mini text-right clearfix">
                <div class="widget-icon pull-left <?= $count['cart'] ? "themed-background-warning" : "" ?>">
                    <i class="hi hi-shopping-cart text-light-op"></i>
                </div>
                <h2 class="widget-heading h3 <?= $count['contact'] ? "text-warning" : "text-muted" ?>">
                    <strong><?= $count['cart'] ?></strong>
                </h2>
                <span class="text-muted">Заказы</span>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-3">
        <a href="<?= Url::to(["items/index"]) ?>" class="widget">
            <div class="widget-content widget-content-mini text-right clearfix">
                <div class="widget-icon pull-left themed-background-danger">
                    <i class="fa fa-hotel text-light-op"></i>
                </div>
                <h2 class="widget-heading h3 text-danger">
                    <strong><?= $count['items'] ?></strong>
                </h2>
                <span class="text-muted">Товар</span>
            </div>
        </a>
    </div>
    <!-- END Simple Stats Widgets -->
</div>

<div class="row">
  <div class="col-sm-12 col-lg-12">
      <div class="widget">
          <div class="widget-content themed-background border-bottom text-light">
              Новые заказы
          </div>
          <div class="widget-content border-bottom themed-background-muted">

              <table id="main_table" class="table table-vcenter table-bordered table-striped table-hover">
                 <thead>
                      <tr>
                        <th class="text-center" style="width: 50px;" id="id_cart">ID</th>
                        <th id="name" class="text-center">Статус</th>
                        <th id="name" class="text-center">Цена</th>
                        <th id="name" class="text-center">Сумма</th>
                        <th id="name" class="text-center">Пользователь</th>
                        <th id="name" class="text-center">Комментарий</th>
                        <th id="visible" class="text-center" style="width: 60px;"><i class="fa fa-calendar"></i></th>
                      </tr>
                  </thead>
                  <tbody>

                      <?php
                      echo $this->render("@app/views/cart/_view_index", array("table_data" => $cart));
                      ?>

                  </tbody>
              </table>
          </div>

      </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-4 col-lg-2">
    <a href="javascript:void(0)" class="widget text-center" id="data-cahce-clear">
      <div class="widget-content themed-background-info text-light-op text-center">
        <i class="fa-3x gi gi-cleaning push-bit"></i><br>
        <strong>Очистить кэш</strong>
      </div>
    </a>
  </div>
</div>
