<?php
// Имя формы
$table_name = "form-colorsrelation";
// ID редактируемой записи
$id_table_name = 'id';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
                <label class="col-md-3 control-label" for="id_colors">Цвет</label>
                <div class="col-md-3">
                    <input type="text" id="id_colors" name="id_colors" class="form-control"  placeholder="Цвет" value="<?= $form['id_colors'] ?>" disabled>
                </div>
                <label class="col-md-2 control-label" for="id_items">Товар</label>
                <div class="col-md-3">
                  <input type="text" id="id_items" name="id_itemsid_items" class="form-control"  placeholder="Товар" value="<?= $form['id_items'] ?>" disabled>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="instock">Колличество</label>
                <div class="col-md-8">
                    <input type="number" id="instock" min="0" name="instock" class="form-control"  placeholder="Колличество" value="<?= $form['instock'] ?>" <?= !$form["visible"] ? "disabled" : "" ?>>
                </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="visible">Показывать</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
