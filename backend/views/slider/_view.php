<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}

foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_slider'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="furnitures">
    <td class='text-center'>
      <?= $id ?>
    </td>

    <td  width="100">
      <?php if (strlen($row['image'])): ?>
        <a href="/img/slider/<?= $row['image'] ?>" data-toggle="lightbox-image">
          <img src="/img/slider/<?= $row['image'] ?>" alt="Image">
        </a>
      <?php else: ?>
        <i class="fa fa-file-image-o push-bit-top text-muted"></i>
      <?php endif; ?>
    </td>

    <?php if (Yii::$app->params['siteSliderText']): ?>
      <td>
        <?= $row['title'] ?><br/>
        <?= $row['sub_title'] ?>
      </td>

      <td>
        <?= $row['text'] ?><br/>
      </td>

      <td>
        <?= $row['button_name'] ?><br/>
        <span class="text-muted"><i class="fa fa-link"></i> <?= $row['button_url'] ?></span>
      </td>
    <?php endif; ?>

    <td class='text-center'>
      <label class='switch switch-success data-checkbox' id='visible'>
        <input type='checkbox' id='visible' <?= $row["visible"] ? "checked" : "" ?>>
        <span></span>
      </label>
    </td>

    <td class='text-center'>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>
      <?php if (strlen($row['image'])): ?>
        <button id='data-delete-image' title='Удалить изображение' class='btn btn-effect-ripple btn-xs btn-warning'><i class='fa fa-file-image-o'></i></button>
      <?php endif; ?>
      <!-- <div class="border-bottom" style="margin:5px 0;"></div>
      <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button>
      <button id='data-refresh' title='Проверить настройки' class='btn btn-effect-ripple btn-xs btn-info'><i class='fa fa-refresh'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
