<?php
// Имя формы
$table_name = "form-slider";
// ID редактируемой записи
$id_table_name = 'id_slider';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

          <?php if (!strlen($form['image'])): ?>
            <div class="form-group">
              <label class="col-md-3 control-label" for="image">Фото</label>
              <div class="col-md-8">
                <div class="dropzone dz-clickable dropzone-in-form" id="image">
                  <div class="dz-default dz-message">
                    <span>Перенесите фото сюда для загрузки</span>
                  </div>
                </div>
              </div>
            </div>
          <?php else: ?>
            <div class="form-group">
              <label class="col-md-3 control-label" for="image">Фото</label>
              <div class="col-md-8">
                <input type="text" id="image" name="image" class="form-control"  placeholder="Фото" value="<?= $form['image'] ?>">
              </div>
            </div>
          <?php endif ?>

          <?php if (Yii::$app->params['siteSliderText']): ?>
            <div class="form-group">
                <label class="col-md-3 control-label" for="title">Заголовок</label>
                <div class="col-md-8">
                    <input type="text" id="title" name="title" class="form-control"  placeholder="Заголовок" value="<?= $form['title'] ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="sub_title">Подзаголовок</label>
                <div class="col-md-8">
                    <input type="text" id="sub_title" name="sub_title" class="form-control"  placeholder="Подзаголовок" value="<?= $form['sub_title'] ?>">
                </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="text">Текст</label>
              <div class="col-md-8">
                <textarea id="text" name="text" class="form-control"  placeholder="Текст"><?= $form['text'] ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="button_name">Кнопка</label>
              <div class="col-md-3">
                <input type="text" id="button_name" name="button_name" class="form-control"  placeholder="Имя кнопки" value="<?= $form['button_name'] ?>">
              </div>
              <label class="col-md-2 control-label" for="button_url">Адрес</label>
              <div class="col-md-3">
                <input type="text" id="button_url" name="button_url" class="form-control"  placeholder="Адрес кнопки" value="<?= $form['button_url'] ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="visible">Показывать</label>
              <div class="col-md-2">
                <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>
          <?php endif ?>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
