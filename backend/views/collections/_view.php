<?php
/* $table_data - данные для заполнения таблицы */

use yii\helpers\Url;

// Если нет данных
if (count($table_data) < 1) {
  echo "<tr>
           <td colspan='111' class='text-center'>Нет данных...</td>
         </tr>";
  return;
}



foreach ($table_data as $row) {
  /* Общие настройки */
  $id = $row['id_collections'];
  ?>

  <tr data-tr-id='<?= $id ?>' id="collections">
    <td>
      <?= $id ?>
    </td>

    <td>
      <?= $row['name'] ?>
    </td>

    <td>
      <a href="<?= Url::to(["modules/index", "s"=>$row['id_collections'], 'row'=>'id_collections', 't'=>'i']) ?>">
        <?= $row->getModules()->count() ?>
      </a>
    </td>

    <td>
      <a href="<?= Url::to(["items/index", "s"=>$row['id_collections'], 'row'=>'id_collections', 't'=>'i']) ?>">
        <?= $row->getItems()->count() ?>
      </a>
    </td>

    <td>
      <label class='switch switch-success data-checkbox' id='visible'>
        <input type='checkbox' id='visible' <?= $row["visible"] ? "checked" : "" ?>>
        <span></span>
      </label>
    </td>

    <td>
      <button id='data-edit' title='Редактировать' class='btn btn-effect-ripple btn-xs btn-success'><i class='fa fa-pencil'></i></button>
      <button id='data-delete' title='Удалить' class='btn btn-effect-ripple btn-xs btn-danger'><i class='fa fa-times'></i></button>

      <button id='data-get-module' title='загрузить модели' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-th'></i></button>
      <!-- <div class="border-bottom" style="margin:5px 0;"></div>-->
      <!-- <button id='data-backup' title='Бэкап прошивки' class='btn btn-effect-ripple btn-xs btn-default'><i class='gi gi-floppy_saved'></i></button> -->
      <!-- <button id='data-compare' title='Сравнить настройки' class='btn btn-effect-ripple btn-xs btn-default'>=</button> -->
      <!--<button id='data-video' title='Добавить видео' class='btn btn-effect-ripple btn-xs btn-default'><i class='fa fa-video-camera'></i></button>-->
    </td>
  </tr>

<?php } ?>
