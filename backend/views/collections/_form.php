<?php
// Имя формы
$table_name = "form-collections";
// ID редактируемой записи
$id_table_name = 'id_collections';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  // $form['name'] = "Шкаф-купе КП-222";
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

            <div class="form-group">
                <label class="col-md-3 control-label" for="name">Заголовок</label>
                <div class="col-md-8">
                    <input type="text" id="name" name="name" class="form-control"  placeholder="Имя" value="<?= $form['name'] ?>">
                </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="visible">Показывать</label>
              <div class="col-md-3">
                <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
              </div>
            </div>


        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
