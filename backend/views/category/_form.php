<?php
use app\models\Helper;

// Имя формы
$table_name = "form-category";
// ID редактируемой записи
$id_table_name = 'id_category';
$id_table = $form[$id_table_name];

// Значения формы по умолчанию
if (!$form[$id_table_name]) {
  $form['visible'] = 1;
}
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form id='<?= $table_name ?>' data-id="<?= $id_table ?>" data-action="edit" class="form-horizontal form-bordered">
        <fieldset>
          <input type="hidden" id="<?= $id_table_name ?>" name="<?= $id_table_name ?>" value="<?= $form[$id_table_name] ?>">

          <?php if (!strlen($form['image']) || $form['icon']): ?>
            <div class="form-group">
              <div id="image-field" class="<?= $form['icon'] ? 'hidden': '' ?>">
                <label class="col-md-3 control-label" for="image_src">Фото</label>
                <div class="col-md-5">
                  <div class="dropzone dz-clickable dropzone-in-form" id="image_src">
                    <div class="dz-default dz-message">
                      <span>Перенесите фото сюда для загрузки</span>
                    </div>
                  </div>
                </div>
              </div>

              <div id="icon-field" class="<?= !$form['icon'] ? 'hidden': '' ?>">
                <label class="col-md-3 control-label" for="icon_class">Класс иконки</label>
                <div class="col-md-5">
                  <input type="text" id="icon_class" name="icon_class" class="form-control"  placeholder="Класс иконки" value="<?= $form['image'] ?>">
                </div>
              </div>

              <div id="icon-ckeck">
                <label class="col-md-2 control-label" for="icon">Иконка</label>
                <div class="col-md-1">
                  <label class='csscheckbox csscheckbox-success' id="icon"><input id="icon" type='checkbox' <?= $form['icon'] ? "checked" : "" ?>><span></span></label>
                </div>
              </div>
            </div>
          <?php endif; ?>

          <div class="form-group">
            <label class="col-md-3 control-label" for="name_ru">Имя</label>
            <div class="col-md-8">
              <input type="text" id="name_ru" name="name_ru" class="form-control"  placeholder="Имя" value="<?= $form['name_ru'] ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label" for="name_en">Имя EN</label>
            <div class="col-md-8">
              <input type="text" id="name_en" name="name_en" class="form-control"  placeholder="Имя" value="<?= $form['name_en'] ?>">
              <span class="help-block">Используется для URL - адреса старницы категории (<?= Helper::ruEnTranslate(mb_strtolower($form['name_ru'])) ?>)</span>
            </div>
          </div>

          <?php if (Yii::$app->params['siteCategoryGroup']): ?>
            <div class="form-group">
              <label class="col-md-3 control-label" for="group">Группа</label>
              <div class="col-md-8">
                <input type="text" id="group" name="group" class="form-control"  placeholder="Группа" value="<?= $form['group'] ?>">
              </div>
            </div>
          <?php endif; ?>

          <div class="form-group">
            <label class="col-md-3 control-label" for="visible">Показывать</label>
            <div class="col-md-3">
              <label class='csscheckbox csscheckbox-success' id="visible"><input id="visible" type='checkbox' <?= $form['visible'] ? "checked" : "" ?>><span></span></label>
            </div>
          </div>

        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button id='data-save' form="<?= $table_name ?>" type="submit" class="btn btn-effect-ripple btn-primary">Готово</button>
    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Закрыть</button>
</div>
